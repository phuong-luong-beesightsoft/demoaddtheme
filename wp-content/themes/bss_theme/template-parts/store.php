<?php
/**
 * Template Name: Store
 */

$stores = storeDetail::all();
$google_api_key = 'AIzaSyDc86QPdj4CxdiYhoJgwjQbvvWbycgXbW4';
?>
<?php get_header(); ?>
<div class="content-wrapper">
    <div class="section section-stores" id="setHeightContent">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="list-stores inlineblock-top">
                        <div class="list-header">LIST STORES</div>
                        <div class="list-content">
                          <?php
                          foreach ($stores as $store) {
                            ?>
                              <div class="list-item display-table">
                                  <div class="icon-item" data-lat="<?= $store->latitude; ?>"
                                       data-lng="<?= $store->longtitude; ?>">
                                      <span class="icon-address"></span>
                                  </div>
                                  <div class="text-item"> <?= $store->street; ?> </div>
                              </div>
                            <?php
                          }
                          ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="map-stores">
                        <div id="map"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    locations = [
      <?php
      foreach($stores as $store){
      $address = $store->street;
      $location['lat'] = $store->latitude;
      $location['lng'] = $store->longtitude;
      ?>
        {
            lat: <?php echo $location['lat'];  ?>,
            lng: <?php echo $location['lng']; ?>,
            name: "<?php echo $address;?>"
        },
      <?php
      }
      ?>
    ];
</script>
<script src="<?php echo 'https://maps.googleapis.com/maps/api/js?key=' . $google_api_key . '&callback=myMap'; ?>"></script>