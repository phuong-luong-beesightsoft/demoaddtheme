<?php
/**
 * Template Name: Back Up Single Product
 */

if(isset($_GET)){
    $productID = $_GET['product-id'];
    $productDetails = productDetail::findByProductDetailId($productID);
    $addOns = productAddonDetail::all();
    $variants = json_decode($productDetails->variants);
    $addOns = productAddonDetail::all();
    if($productDetails){
        $productDetails->categories = json_decode($productDetails->categories);
    }
}
?>
<?php get_header(); ?>
<div id="primary" class="content-area">
    <div class="container">
        <div id="main" class="site-main">
            <?php
            if(!empty($productDetails)) {
                ?>
                <nav class="breadcrumb-product">
                    <a href="<?= get_home_url(); ?>">
                        Home
                    </a>
                    /
                    <a href="<?= get_home_url() . 'product-category/clothing/clothing' ?>">
                        <?= $productDetails->categories[0]->name; ?>
                    </a>
                    / <?= $productDetails->name; ?>
                </nav>
                <div id="product-details" class="product type-product">
                    <div class="row">
                        <div class="col-sm-6 ">
                            <div class="product-details-img">
                                <img src="<?php echo $productDetails->image; ?>"
                                     alt="Product Details Image">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="product-details-content">
                                <h1><?= $productDetails->name; ?></h1>
                                <div class="product-details-price">
                                    <h1><?=  number_format($productDetails->price, 0, ',', '.') . " VND"; ?></h1>
                                </div>
                                <div class="product-details-content-description">
                                    <p><?php echo $productDetails->description ?></p>
                                </div>
                                <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                                <div class="container" id="map-canvas" style="height:300px;"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="single-product-related">
                                <h1>Related Product</h1>
                            </div>
                        </div>
                        <?php
                        $relatedProducts = productList::find('all', array(
                            'conditions'  => array(
                                'categories_id = ' . $productDetails->categories[0]->id
                            )
                        ));
                        $cntRelatedProduct = 0;
                        foreach($relatedProducts as $key => $relatedProduct ) {
                            if($relatedProduct->product_id != $productDetails->id) {
                                $cntRelatedProduct++;
                                ?>
                                <div class="col-sm-3">
                                    <div class="related-product-item">
                                        <div class="related-product-item-img">
                                            <img src="<?php echo $relatedProduct->image ?>">
                                        </div>
                                        <div class="related-product-item-title">
                                            <a href="<?php echo get_site_url() . '/single-product/?product-id=' . $relatedProduct->product_id;?>">
                                                <?= $relatedProduct->name; ?>
                                            </a>
                                        </div>
                                        <div class="related-product-item-price">
                                            <h5>
                                                <?= number_format($relatedProduct->price, 0, ',', '.') . " VND"; ?>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                if($cntRelatedProduct == 4){
                                    break;
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
                <?php
            }
            else{
                echo "<h1>Please Choose A Product !!!</h1>";
            }
            ?>
        </div>
    </div>
    <!--    <script src="--><?php //echo 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCqmZ0AnE_9_bFGjq6gp-Pcig3gdJq9Frw&libraries=places'; ?><!--"></script>-->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
    <?php get_footer(); ?>
