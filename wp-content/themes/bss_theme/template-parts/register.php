<?php
/**
 * Template Name: Register
 */
?>
<?php

?>
<?php get_header(); ?>
<div class="section section-register">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="register-customer">
                    <div class="block-title">
                        REGISTER
                    </div>
                    <div class="register-customer-form">
                        <div class="col-sm-10 offset-sm-1">
                            <form class="form-register" novalidate method="POST">
                                <div class="register-info">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="title-item">First Name</label>
                                                <input type="text" name="first_name" id="first_name"
                                                       class="form-control first_name-item" maxlength="50"
                                                       placeholder="Enter your first name" value="" required>
                                                <div class="invalid-feedback">
                                                    Please provide first name.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="title-item">Last Name</label>
                                                <input type="text" name="last_name" id="last_name"
                                                       class="form-control last_name-item" maxlength="50"
                                                       placeholder="Enter your last name" value="" required>
                                                <div class="invalid-feedback">
                                                    Please provide last name.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="title-item">Email</label>
                                                <input type="email" name="username" id="username"
                                                       class="form-control username-item" maxlength="50"
                                                       placeholder="Enter your username" value="" required>
                                                <div class="invalid-feedback">
                                                    Please provide your email.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="title-item">Phone Number</label>
                                                <input type="text" name="phone" id="phone"
                                                       class="form-control phone-item" maxlength="15"
                                                       placeholder="Enter your phone number" value="" required>
                                                <div class="invalid-feedback">
                                                    Please provide your phone number
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="title-item">Password</label>
                                                <input type="password" name="password" id="password"
                                                       class="form-control password-item" maxlength="50"
                                                       placeholder="Enter your password" value="" required>
                                                <div class="invalid-feedback">
                                                    Please enter your password.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="title-item">Confirm Password</label>
                                                <input type="password" name="password_confirmation"
                                                       id="password_confirmation"
                                                       class="form-control password_confirmation-item" maxlength="50"
                                                       placeholder="Confrim password" value="" required>
                                                <div class="invalid-feedback">
                                                    Please Confirm your password.
                                                </div>
                                                <span id='message'></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="title-item">Gender</label>
                                                <input type="text" name="gender" id="gender"
                                                       class="form-control gender-item"
                                                       maxlength="50" placeholder="Gender" value="" required>
                                                <div class="invalid-feedback">
                                                    Please provide your gender.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="title-item">DOB</label>
                                                <input type="date" name="dob" id="dob" class="form-control dob-item"
                                                       maxlength="50" placeholder="DOB" value="" required>
                                                <div class="invalid-feedback">
                                                    Please provide your DOB.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label class="title-item">Address</label>
                                                <input type="text" name="address" id="address"
                                                       class="form-control address-item"
                                                       maxlength="50" placeholder="Enter your address" value=""
                                                       required>
                                                <div class="invalid-feedback">
                                                    Please provide your address.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <input type="hidden" name="brand_id" value="44">
                                        <button id="confirmRegister" class="btn btn-yellow">CONFIRM</button>
                                    </div>
                                </div>
                            </form>
                            <form class="form-activation" method="GET" style="display: none;">
                                <div class="activation-info">
                                    <div class="row">
                                        <div class="col-sm-8 offset-sm-2">
                                            <div class="form-group">
                                                <div class="alert alert-danger" role="alert" style="display: none;">
                                                    <strong>Error</strong>
                                                </div>
                                                <div class="alert alert-success" role="alert" style="display: none;">
                                                    <strong>Success</strong>
                                                </div>
                                                <label class="title-item activation-message"></label>
                                                <input type="text" id="otp" name="otp"
                                                       class="form-control activation-item" maxlength="6"
                                                       placeholder="Please Enter Your Activation Code " value=""
                                                       required>
                                                <div class="resend-otp">
                                                    <a href="#"><span class="fa fa-retweet"></span> Resend otp</a>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <input type="hidden" name="account" id="account"
                                                       value="pluong480@gmail.com">
                                                <button id="confirmActivation" class="btn btn-yellow">SUBMIT</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
