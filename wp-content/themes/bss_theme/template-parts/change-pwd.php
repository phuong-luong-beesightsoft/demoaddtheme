<?php
/**
 * Template Name: Change Password
 */
?>
<?php
get_header();
?>
<div class="section section-change-pwd">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 offset-sm-2">
                <div class="change-pwd">
                    <div class="block-title">
                        CHANGE PASSWORD
                    </div>
                    <div class="change-pwd-info">
                        <form method="POST" class="form-change-pwd" enctype="multipart/form-data">
                            <div class="my-profile-input">
                                <div class="ul-custom">
                                    <div>
                                        <div class="form-group">
                                            <div class="alert alert-danger" role="alert" style="display: none;">
                                                <strong>Error</strong>
                                            </div>
                                            <div class="alert alert-success" role="alert" style="display: none;">
                                                <strong>Success</strong>
                                            </div>
                                            <label class="title-item">Current Password</label>
                                            <input type="password" name="old_pwd" id="old_password"
                                                   class="form-control last-name-item" maxlength="50"
                                                   value="" required>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="form-group">
                                            <label class="title-item">New Password</label>
                                            <input type="password" name="new_pwd" id="password"
                                                   class="form-control phone-number-item" maxlength="50"
                                                   value="" required>
                                            <div id="messageError">
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="form-group">
                                            <label class="title-item">Confirm New Password</label>
                                            <input type="password" name="confirm_new_pwd" id="password_confirmation"
                                                   class="form-control email-profile-item" maxlength="50"
                                                   value="" required>
                                            <div id="message">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button id="actionChangePwd" class="btn btn-yellow">SAVE CHANGE</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
?>
