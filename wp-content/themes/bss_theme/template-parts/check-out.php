<?php
/**
 * Template Name: Checkout
 */
?>
<?php
if (!isset($_SESSION['cart-items']) || $_SESSION['cart-items'] == null) {
    header("Location: " . get_site_url());
} else {
    ?>
    <?php get_header(); ?>
    <div class="content-wrapper">
        <div class="phone-call mobile-item">
            <a href="tel:+0123456789" class="phone-call-icon d-flex justify-content-center align-items-center">
                <span class="icon-phone-speaker"></span>
            </a>
        </div>
        <form class="checkout-form" method="post">
            <div class="section section-cart" id="setHeightContent">
                <div class="container">
                    <div class="content-cart">
                        <div class="title-cart title">Your cart</div>
                        <div class="title-item title-item-mobile">Order Items</div>
                        <div class="list-items">
                            <?php
                            if (isset($_SESSION['cart-items'])) {
                                foreach ($_SESSION['cart-items'] as $key => $cartitem) {
                                    ?>
                                    <div class="order-item d-flex justify-content-between">
                                        <div class="d-flex">
                                            <div class="d-flex align-items-center">
                                                <a class="btn"><span class="icon-minus" data-id="1"></span></a>
                                                <div class="img-order">
                                                    <img src="<?= $cartitem['image'] ?>" alt="alt">
                                                    <div class="number text-center"><?= $cartitem['quantity']; ?></div>
                                                </div>
                                            </div>
                                            <div class="text-order">
                                                <div class="small-title"><?= $cartitem['name']; ?></div>
                                                <?php
                                                if (isset($cartitem['addOns'])) {
                                                    foreach ($cartitem['addOns'] as $item => $addOn) {
                                                        ?>
                                                        <div><?php echo $addOn['addons_type'] . ' : ' . $addOn['name'] ?></div>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="price-order">
                                            VND <?php echo number_format($cartitem['price']); ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="subtotal">
                            <div class="d-flex justify-content-between">
                                <div class="title-item">Subtotal</div>
                                <div class="price-subtotal">
                                    VND
                                    <?php
                                    $totalBill = 0;
                                    if (isset($_SESSION['cart-items'])) {
                                        foreach ($_SESSION['cart-items'] as $key => $cartitem) {
                                            $totalBill += $cartitem['price'];
                                        }
                                    }
                                    echo number_format($totalBill);
                                    ?>
                                </div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="title-item">Delivery Fee</div>
                                <div class="price-subtotal">-</div>
                            </div>
                        </div>
                        <div class="note-item">
                            <div class="title-item">Note</div>
                            <textarea class="note-content" name="note" rows="2">Note</textarea>
                        </div>
                        <div class="discout">
                            <div class="d-flex justify-content-between align-items-center">
                                <div>
                                    <span class="title-item">Discount</span>
                                    <input type="text" name="coupon-code" value="promotionCode"
                                           placeholder="DISCOUNT CODE">
                                    <input type="submit" name="coupn" value="APPLY" class="btn btn-yellowlight">
                                </div>
                                <div class="price-subtotal desktop-item">- VND 0</div>
                            </div>
                            <div class="price-subtotal text-right mobile-item">- VND 0</div>
                        </div>
                        <div class="total d-flex justify-content-between align-items-center">
                            <div>TOTAL</div>
                            <div class="price">VND <?= number_format($totalBill); ?></div>
                        </div>
                        <div class="deli-info">
                            <div class="title-item">Shipping Information</div>
                            <a class="btn" data-toggle="modal" data-target="#select-delivery-type"><span
                                        class="icon-edit"></span></a>
                            <div class="content-deli-info">
                                <div class="display-table">
                                    <div>Shipping Method</div>
                                    <div><?= $_SESSION['checkout']['type']; ?></div>
                                </div>
                                <?php
                                if ($_SESSION['checkout']['type'] === 'delivery') {
                                    ?>
                                    <div class="display-table">
                                        <div>Address</div>
                                        <div><?php
                                            $_SESSION['checkout']['address'];
                                            if (isset($_SESSION['profile']['address'])) {
                                                foreach ($_SESSION['profile']['address'] as $key => $address) {
                                                    if ($address->id == $_SESSION['checkout']['address']) {
                                                        echo $address->address1;
                                                    }
                                                }
                                            }
                                            ?>

                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <?php
                                if ($_SESSION['checkout']['type'] === 'pick-up') {
                                    ?>
                                    <div class="display-table">
                                        <div>Store</div>
                                        <div><?php
                                            $stores = storeDetail::all();
                                            if (!empty($stores)) {
                                                foreach ($stores as $key => $store) {
                                                    if ($store->enable == 1) {
                                                        if ($store->id == $_SESSION['checkout']['store']) {
                                                            echo $store->name;
                                                        }
                                                    }
                                                }
                                            }

                                            ?></div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="display-table">
                                    <div>Time</div>
                                    <div><?= $_SESSION['checkout']['time']; ?></div>
                                </div>
                                <div class="display-table">
                                    <div>Name</div>
                                    <div><?= $_SESSION['checkout']['name'] ?></div>
                                </div>
                                <div class="display-table">
                                    <div>Number</div>
                                    <div><?= $_SESSION['checkout']['phone'] ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="payment-method">
                            <div class="title-item">PAYMENT METHOD</div>
                            <div class="list-payment-method">
                                <label class="icheck">Cash
                                    <input type="radio" name="payment-method" checked="checked">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="text-right">
                            <button id="orderBtn" class="btn btn-yellowlight btn-your-cart">
                                <span>PLACE ORDER</span>
                                <span>VND <?= number_format($totalBill); ?></span>
                                <input type="hidden" name="total" id="cart-total" value="$total - $discountTotal">
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <input type="hidden" name="order_type"
                   value="<?= isset($_SESSION['checkout']['type']) ? $_SESSION['checkout']['type'] : null; ?>"/>
            <input type="hidden" name="is_guest"
                   value="<?= isset($_SESSION['checkout']['is_guest']) ? $_SESSION['checkout']['is_guest'] : 0; ?>"/>
            <input type="hidden" name="customer_id"
                   value="<?= isset($_SESSION['checkout']['customer_id']) ? $_SESSION['checkout']['customer_id'] : null; ?>"/>
            <input type="hidden" name="total_item_count"
                   value="<?= isset($_SESSION['cart-items']) ? count($_SESSION['cart-items']) : null; ?>"/>
            <?php
            if (isset($_SESSION['cart-items'])) {
                $total_qty_ordered = 0;
                $total_qty_ordered = (int)$total_qty_ordered;
                foreach ($_SESSION['cart-items'] as $key => $item) {
                    $total_qty_ordered += (int)$item['quantity'];
                }
            }
            ?>
            <input type="hidden" name="total_qty_ordered"
                   value="<?= isset($total_qty_ordered) ? $total_qty_ordered : null; ?>"/>
            <input type="hidden" name="order_currency_code" value="cod"/>
            <!--Shipping address -->
            <?php
            if ($_SESSION['checkout']['type'] == 'delivery' && $_SESSION['profile']['address'] && $_SESSION['checkout']['address']) {
                foreach ($_SESSION['profile']['address'] as $key => $address) {
                    if ($address->id == $_SESSION['checkout']['address']) {
                        ?>
                        <input type="hidden" name="name_shipping" value="<?= $address->name; ?>"/>
                        <input type="hidden" name="address1_shipping" value="<?= $address->address1; ?>"/>
                        <input type="hidden" name="country_shipping" value="<?= $address->country; ?>"/>
                        <input type="hidden" name="state_shipping" value="<?= $address->state; ?>"/>
                        <input type="hidden" name="city_shipping" value="<?= $address->city; ?>"/>
                        <input type="hidden" name="postcode_shipping" value="<?= $address->postcode; ?>"/>
                        <input type="hidden" name="phone_shipping" value="<?= $address->phone; ?>"/>
                        <input type="hidden" name="building_name_shipping" value="<?= $address->building_name; ?>"/>
                        <input type="hidden" name="address_nickname_shipping"
                               value="<?= $address->address_nickname; ?>"/>
                        <input type="hidden" name="notes_shipping" value="<?= $address->notes; ?>"/>
                        <input type="hidden" name="latitude_shipping" value="<?= $address->latitude; ?>"/>
                        <input type="hidden" name="longitude_shipping" value="<?= $address->longitude; ?>"/>
                        <input type="hidden" name="address_type_shipping" value="billing"/>
                        <?php
                    }
                }
            }
            ?>
            <!-- End Shipping Address -->
            <input type="hidden" name="payment_method_id" value="1"/>
            <input type="hidden" name="sub_total" value="<?= $totalBill; ?>"/>
            <input type="hidden" name="discount_amount" value=""/>
            <input type="hidden" name="brand_id" value="<?= $bss_options['BRAND_ID']; ?>"/>
            <?php
            if ($_SESSION['checkout']['type'] == 'pick-up') {
                ?>
                <input type="hidden" name="outlet_id"
                       value="<?= isset($_SESSION['checkout']['store']) ? $_SESSION['checkout']['store'] : null; ?>"/>
                <?php
            }
            ?>
            <input type="hidden" name="platform" value="web_cms"/>
            <!-- Promotions -->
            <input type="hidden" name="promotions_id" value=""/>
            <input type="hidden" name="promotions_name" value=""/>
            <input type="hidden" name="promotions_code" value=""/>
            <input type="hidden" name="promotions_discount_amount" value=""/>
            <input type="hidden" name="promotions_maximum_discount" value=""/>
            <!-- End Promotions -->
            <input type="hidden" name="promotion_code" value=""/>
            <input type="hidden" name="coupon_code" value=""/>
        </form>
    </div>
    <?php
    get_footer();
}
?>
