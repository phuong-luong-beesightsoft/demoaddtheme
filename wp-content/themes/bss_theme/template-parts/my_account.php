<?php
/**
 * Template Name: My Account
 */
?>
<?php
global $bss_options;
?>
<?php
if (isset($_SESSION['profile']['access_token'])) {
    ?>
    <?php
    get_header();
    ?>
    <div class="section section-my-account">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="my-account-info">
                        <div class="nav nav-tabs" id="tab_order_history" role="tablist">
                            <a class="nav-item nav-link" data-toggle="tab" href="#order_history" role="tab"
                               aria-controls="order_history" aria-selected="true" id="order-history">
                                ORDER HISTORY
                            </a>
                            <a class="nav-item nav-link active" data-toggle="tab" href="#my_profile" role="tab"
                               aria-controls="my_profile" aria-selected="true" id="my-profile">
                                MY PROFILE
                            </a>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade show" id="order_history" role="tabpanel"
                                 aria-labelledby="order_history-tab">
                                ABCDEF
                            </div>
                            <div class="tab-pane fade show active" id="my_profile" role="tabpanel"
                                 aria-labelledby="my_profile-tab">
                                <div class="col-sm-8 offset-sm-2">
                                    <div class="my-profile-info">
                                        <form method="POST" class="form-change-profile" enctype="multipart/form-data">
                                            <div class="avatar">
                                                <div class="upload-img">
                                                    <label for="file-image">
                                                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/icon-upload-img.png">
                                                    </label>
                                                    <input type="file" name="file_image" id="file-image"
                                                           style="display: none;">
                                                </div>
                                                <?php
                                                if (isset($_SESSION['profile']['avatar'])) {
                                                    ?>
                                                    <img src="<?php echo $bss_options['CXI_URL'] . $_SESSION['profile']['avatar']; ?>"
                                                         alt="" class="current-avatar">
                                                    <?php
                                                } else {
                                                    ?>
                                                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/user-login.png"
                                                         alt="" class="current-avatar">
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <div class="my-profile-input">
                                                <div class="ul-custom">
                                                    <div>
                                                        <div class="form-group">
                                                            <label class="title-item">First Name</label>
                                                            <input type="text" name="first_name"
                                                                   class="form-control first-name-item" maxlength="50"
                                                                   value="<?= $_SESSION['profile']['first_name'] ?>">
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="form-group">
                                                            <label class="title-item">Last Name</label>
                                                            <input type="text" name="last_name"
                                                                   class="form-control last-name-item" maxlength="50"
                                                                   value="<?= $_SESSION['profile']['last_name']; ?>">
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="form-group">
                                                            <label class="title-item">Phone Number</label>
                                                            <input type="text" name="phone_number"
                                                                   class="form-control phone-number-item" maxlength="50"
                                                                   value="<?= $_SESSION['profile']['phone'] ?>">
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="form-group">
                                                            <label class="title-item">Email</label>
                                                            <input type="email" name="email_profile"
                                                                   class="form-control email-profile-item"
                                                                   maxlength="50"
                                                                   value="<?= $_SESSION['profile']['email'] ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="text-center">
                                                    <button id="editProfile" class="btn btn-yellow">SAVE CHANGE</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    get_footer();
    ?>
    <?php
} else {
    header("Location: " . get_site_url());
}
?>
