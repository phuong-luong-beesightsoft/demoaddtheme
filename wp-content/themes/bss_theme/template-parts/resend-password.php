<?php
/**
 * Template Name: Reset Password
 */
?>
<?php
get_header();
?>
<div class="section section-reset-pwd">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="block-title">
                    RESET PASSWORD
                </div>
                <div class="col-sm-8 offset-sm-2">
                    <div class="alert alert-success successful-reset" style="display: none;">
                        successfully
                    </div>
                </div>
                <div class="register-customer-form">
                    <div class="col-sm-10 offset-sm-1">
                        <form class="form-reset-pwd" method="GET">
                            <div class="activation-info">
                                <div class="row">
                                    <div class="col-sm-8 offset-sm-2">
                                        <div class="form-group">
                                            <div class="alert alert-danger" role="alert" style="display: none;">
                                                <strong>Error</strong>
                                            </div>
                                            <div class="alert alert-success" role="alert" style="display: none;">
                                                <strong>Success</strong>
                                            </div>
                                            <label class="title-item activation-message"></label>
                                            <input type="text" name="username"
                                                   class="form-control activation-item" maxlength="50"
                                                   placeholder="Please Enter Your Username " value=""
                                                   required>
                                        </div>
                                        <div class="text-center">
                                            <button id="confirmActivation" class="btn btn-yellow">SUBMIT</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <form class="form-otp-reset-pwd" style="display: none;">
                            <div class="activation-info">
                                <div class="row">
                                    <div class="col-sm-8 offset-sm-2">
                                        <div class="form-group">
                                            <div class="alert alert-danger" role="alert" style="display: none;">
                                                <strong>Error</strong>
                                            </div>
                                            <div class="alert alert-success" role="alert" style="display: none;">
                                                <strong>Success</strong>
                                            </div>
                                            <label class="title-item activation-message">Please input the confirm code from your email or phone number to verify!</label>
                                            <input type="text" name="otp_reset_pwd"
                                                   class="form-control activation-item" maxlength="50"
                                                   placeholder="Please Enter Your OTP" value=""
                                                   required>
                                        </div>
                                        <div class="text-center">
                                            <input type="hidden" name="username_reset_pwd" value="" >
                                            <button id="confirmActivation" class="btn btn-yellow">SUBMIT</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <form class="form-new-pwd" style="display: none;" method="POST">
                            <div class="activation-info">
                                <div class="row">
                                    <div class="col-sm-8 offset-sm-2">
                                        <div class="form-group">
                                            <div class="alert alert-danger" role="alert" style="display: none;">
                                                <strong>Error</strong>
                                            </div>
                                            <div class="alert alert-success" role="alert" style="display: none;">
                                                <strong>Success</strong>
                                            </div>
                                            <label class="title-item newpwd">New Password</label>
                                            <input type="password" name="newpwd" class="form-control newpwd-item"
                                                   maxlength="50" placeholder="New Password" value="" required>
                                        </div>
                                        <div class="form-group">
                                            <label class="title-item confirm-new-pwd">Confirm New Password</label>
                                            <input type="password" name="confirm_new_pwd"
                                                   class="form-control confirm-new-pwd" maxlength="50"
                                                   placeholder="Confirm New Password" value="" required>
                                        </div>
                                        <div class="text-center">
                                            <input type="hidden" name="reminderCode" value="">
                                            <input type="hidden" name="userId" value="">
                                            <button id="confirmActivation" class="btn btn-yellow">RESET PASSWORD
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
?>
