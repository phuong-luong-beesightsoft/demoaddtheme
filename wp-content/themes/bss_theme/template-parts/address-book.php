<?php
/**
 * Template Name: Address Book
 */
?>
<?php
if (!isset($_SESSION['profile']['access_token'])) {
    header("Location: " . get_site_url());
} else {
    global $bss_options;
    $url = $bss_options['API_CXI_URL'] . '/users/addresses?page=1&per_page=5&order_by=created_at&order=desc';
    $args = array(
        'headers' => array(
            'X-APP-ID' => $bss_options['API_CXI_X_APP_ID'],
            'Authorization' => 'Bearer ' . $_SESSION['profile']['access_token']
        )
    );
    $response = wp_remote_get($url, $args);
    if (is_wp_error($response)) {
        return false;
    }
    $body = wp_remote_retrieve_body($response);
    $data = json_decode($body);
    ?>
    <?php
    get_header();
    ?>
    <div class="section section-address-book">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 offset-sm-2">
                    <div class="address-book-info">
                        <div class="block-title">
                            ADDRESS BOOK
                        </div>
                        <div class="add-new-address text-center">
                            <span>
                                + Add new address
                            </span>
                        </div>
                        <?php
                        if ($data->error == false) {
                            foreach ($data->data as $key => $infoAddress) {
                                ?>
                                <div class="d-flex align-items-center address-book">
                                    <div class="icon-location">
                                        <img src="<?php echo get_template_directory_uri() . "/assets/images/ico-location.png " ?>">
                                    </div>
                                    <div class="address-book-content">
                                        <div class="title">
                                            <?= $infoAddress->name; ?>
                                        </div>
                                        <div class="info-address">
                                            <?= $infoAddress->building_name; ?>, <?= $infoAddress->address1; ?> <br>
                                            <?= $infoAddress->state; ?> <?= $infoAddress->postcode ?>
                                            , <?= $infoAddress->country ?>
                                        </div>
                                        <div class="number-phone">
                                            <?= $infoAddress->phone; ?>
                                        </div>
                                    </div>
                                    <div class="ml-auto d-flex align-items-center">
                                        <div class="ml-auto icon-edit-address" data-id="<?= $infoAddress->id; ?>">
                                            <span class="fa fa-pencil"></span>
                                        </div>
                                        <div class="ml-auto icon-remove-address" data-id="<?= $infoAddress->id; ?>">
                                            <span class="fa fa-times"></span>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    View::render('modal/add-address', array(), true, false);
    View::render('modal/edit-address', array(), true, false);
    get_footer();
    ?>
    <?php
}
?>