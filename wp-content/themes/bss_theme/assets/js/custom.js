var namespace = "bss-theme";
var version = 1;
jQuery(document).ready( function ($) {

    $('input.input-check-enable').on('click',function(){
        if($(this).prop("checked") == true){
            var _this = $(this);
            var _productID = _this.attr('data-id');
            var _nametable = _this.attr('data-table');
            var ajaxUrlPDF = _domainUrl + '/wp-json' + '/' + namespace + '/v' + version + '/update-status';
            $.ajax({
                url: ajaxUrlPDF,
                type: 'POST',
                dataType: 'JSON',
                data: {
                    enable : 1,
                    product_id :  _productID,
                    name_table: _nametable
                },
                success: function (response) {
                    if(response.success){
                        alert('Enabled !!!');
                    }
                },
            });
        }
        else if($(this).prop("checked") == false){
            var _this = $(this);
            var _productID = _this.attr('data-id');
            var _nametable = _this.attr('data-table');
            var ajaxUrlPDF = _domainUrl + '/wp-json' + '/' + namespace + '/v' + version + '/update-status';
            $.ajax({
                url: ajaxUrlPDF,
                type: 'POST',
                dataType: 'JSON',
                data: {
                    enable : 0,
                    product_id :  _productID,
                    name_table: _nametable
                },
                success: function (response) {
                    if(response.success){
                        alert('Disabled !!!');
                    }
                },
            });
        }
    });

    $('.data-sync').on('click',function(){
        var ajaxUrlPDF = _domainUrl + '/wp-json' + '/' + namespace + '/v' + version + '/delete-db';
        var _confirm = confirm("Are you sure you will synchronize data?");
        if(_confirm == true){
            $.ajax({
                url: ajaxUrlPDF,
                type: 'POST',
                dataType: 'JSON',
                data: {
                    deletedb : 1
                },
                success: function (response) {
                    alert("synced data");
                },
            });
        }
    });

    $('.owl-carousel').owlCarousel({
        loop:false,
        margin:10,
        nav:false,
        items: 4
    });

    $('a.menu-list-item').on('click',function(){
        $('a.menu-list-item').removeClass('active-menu');
        $(this).addClass('active-menu');
    });

    var iScrollPos = 0;

    $(window).scroll(function(){
       var scroll = $(window).scrollTop();
       if(scroll > iScrollPos){
           $('header').removeClass('position-top');
           $('.section-product').removeClass('margin-top-104');
       }
       else{
           $('header').addClass('position-top');
           if ($('.free-delivery-mobile').css('display') != 'none') {
               if ($('.free-delivery-mobile').length > 0) {
                   $('.section-product').addClass('margin-top-104');
               }
           }
       }
        iScrollPos = scroll;
    });
});