jQuery(document).ready(function ($) {

    var _productData;
    var total = 0;
    var quantity = 0;
    var addOns = [];
    var selectProduct;
    var ChildIdProduct;
    var idProduct;
    var childEls = [];

    function getInputVal(form) {
        var action = form.attr('action'),
            type = form.attr('method'),
            data = {};
        // Make sure you use the 'name' field on the inputs you want to grab
        form.find('[name]').each(function (i, v) {
            var input = $(this),
                name = input.attr('name'),
                value = input.val();
            data[name] = value;
        });
        return data;
    };

    function getUrl(url) {
        var returnUrl = _domainUrl + '/wp-json' + '/' + namespace + '/v' + version + '/' + url;
        return returnUrl;
    }

    getCartdetails();
    $('.check-modal').on('click', function () {
        var _this = $(this);
        var _productId = $(this).data('id');
        total = 0;
        var ajaxUrlPDF = _domainUrl + '/wp-json' + '/' + namespace + '/v' + version + '/custom-product/' + _productId;
        $.ajax({
            type: 'GET',
            url: ajaxUrlPDF,
            success: function (response) {
                if (typeof (response) == 'string') {
                    response = JSON.parse(response);
                }
                if (response.success) {
                    $('#book-modal').modal('show');
                    appendModal(response.data, $('#book-modal'));
                    _productData = response.data;
                    idProduct = _productData.id;
                    selectProduct = _productData.name;
                }
            }
        });
    });

    $('header').on('click', '#checkoutOrder', function () {
        var checkEmpty = $('.custom-header-item.cart-item').find('.quantity-product').html();
        if (checkEmpty == "(0)") {
            document.location.href = _domainUrl;
        } else {
            var DeliType = $('.custom-header-item.cart-item').attr('data-type');
            if (typeof DeliType !== typeof undefined && DeliType !== false) {
                document.location.href = _domainUrl + '/checkout';
            } else {
                $('#select-delivery-type').modal('show');
            }
        }
    });

    $('.deli-info .icon-edit').on('click', function () {
        $('#select-delivery-type').modal('show');
    });

    $('.modal-body').on('change', 'select.selectpicker', function () {
        var optionSelected = $("option:selected", this);
        var indexSelected = optionSelected.attr('data-index');
        total = optionSelected.attr('data-price');
        let $extra = $('<div class="item extra-addon">' +
            '               <div class="small-title">EXTRA</div>' +
            '           </div>');

        _productData.variants[indexSelected].addon_types.forEach(function (item) {
            item.addons.forEach(function (addon) {
                _productData.addOns.forEach(function (infoAddon) {
                    if (infoAddon.product_add_on_id == addon.id) {
                        addon['image'] = infoAddon.image;
                    }
                })
            })
        });
        //append modal-body
        if ($('#book-modal').find('.modal-body .extra-addon')) {
            $('#book-modal').find('.modal-body .extra-addon').html('').remove();
        }
        let $items = $('<div class="item-addon">' + _productData.variants[indexSelected].addon_types.map(function (item) {
            return '<div class="value-add-on"><div class="value-add-on-desc">' + item.name + '</div><ul>' + '<form data-cntradio="0">' +
                item.addons.map(function (addon) {
                    return '<li>' +
                        '         <div class="option-item">' +
                        '              <div class="img-item">' +
                        '                   <img src="' + addon.image + '" alt=""/>' +
                        '                   <label class="icheck checkbox3 d-flex align-items-center">' +
                        '                        <input type="checkbox" data-addons-type="' + item.name + '"  data-name="' + addon.name + '" data-cntclickradio="0"  data-price="' + addon.price + '" name="add-on" value="' + addon.id + '"/>' +
                        '                        <span class="checkmark"></span>' +
                        '                   </label>' +
                        '               </div>' +
                        '               <div class="promotion-price value-add-on-name">' + addon.name + '</div>' +
                        '               <div class="promotion-price value-add-on-price value-add-on-name">' + formatCurrency(addon.price) + '</div>' +
                        '          </div>' +
                        '      </li>';
                }).join('')
                + '</form></ul></div>'
        }).join('')
        );
        $extra.find('.small-title').parent().append($items);
        $('#book-modal').find('.modal-body').append($extra);

        //append modal-footer

        $('.modal-footer').find('.total-item .total-price').html('').html(formatCurrency(total));

        quantity = 1;

        $('.modal-footer').find('.quantity-number').html(quantity);

        addOns = [];

        selectProduct = optionSelected.data('name');
        ChildIdProduct = optionSelected.val();

    });

    $('.modal-body').on('click', 'form input[type="checkbox"]', function () {
        var _this = $(this);
        total = parseInt(total);
        if (_this.is(':checked')) {
            total = (total * quantity) + (parseInt(_this.data('price')) * quantity);
            $('.modal-footer').find('.total-item .total-price').html('').html(formatCurrency(total));
            total = total / quantity;
            addOns.push({
                id: parseInt(_this.val()),
                name: _this.data('name'),
                quantity: quantity,
                addons_type: _this.data('addons-type'),
                price: _this.data('price')
            });
        } else {
            total = (total * quantity) - (parseInt(_this.data('price')) * quantity);
            $('.modal-footer').find('.total-item .total-price').html('').html(formatCurrency(total));
            total = total / quantity;
            for (var i = 0; i < addOns.length; i++) {
                if (addOns[i].id === parseInt(_this.val())) {
                    addOns.splice(i, 1);
                }
            }
        }
    });

    $('.modal-footer').on('click', '.quantity-plus', function () {
        if (quantity != 0) {
            quantity += 1;
            $('.modal-footer').find('.quantity-number').html(quantity);
            total = total * quantity;
            $('.modal-footer').find('.total-item .total-price').html('').html(formatCurrency(total));
            total = total / quantity;
            addOns.forEach(function (item) {
                item.quantity = quantity;
            });
        }
    });

    $('.modal-footer').on('click', '.quantity-minus', function () {
        if (quantity != 0) {
            quantity -= 1;
            if (quantity === 0) {
                quantity = 1;
            }
            $('.modal-footer').find('.quantity-number').html(quantity);
            total = total * quantity;
            $('.modal-footer').find('.total-item .total-price').html('').html(formatCurrency(total));
            total = total / quantity;
            addOns.forEach(function (item) {
                item.quantity = quantity;
            });
        }
    });

    $('.modal-footer').on('click', '.btn-yellow', function () {
        if (quantity > 0) {
            addToCart();
            setTimeout(function () {
                $('#book-modal').modal('toggle');
            }, 700);
        }
    });

    $('.custom-header-item').on('click', '.your-cart .icon-cancel', function () {
        let _this = $(this);
        removeItem(_this.data('key'));
    });

    $('.custom-header-item').on('click', '.your-cart .icon-minus', function () {
        let _this = $(this);
        var quantityofProduct = parseInt(_this.data('quantity')) - 1;
        updateCartData(_this.data('key'), quantityofProduct, _this.data('quantity'));
    });

    $('header').on('click', '.show-your-cart', function () {
        if (!$(this).parent().find('.your-cart').hasClass('show')) {
            $(this).parent().find('.your-cart').addClass('show');
            $(this).parent().find('.your-cart').removeClass('display-none');
        } else {
            $(this).parent().find('.your-cart').addClass('display-none');
            $(this).parent().find('.your-cart').removeClass('show');
        }
    });

    $('.custom-header-item').on('click', '.your-cart .icon-plus', function () {
        let _this = $(this);
        var quantityofProduct = parseInt(_this.data('quantity')) + 1;
        updateCartData(_this.data('key'), quantityofProduct, _this.data('quantity'));
    });

    function formatCurrency(value) {
        if (typeof (value) !== "string") {
            value = value.toString();
        }
        return 'VND ' + value.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function appendModal(data, el) {
        //append modal-body
        let $image = $('<div class="img-item"><img src="' + data.image + '" alt="Image"></div>');
        let $title = $('<h3 class="title">' + data.name + '</h3>');
        let $legendary = $('<div class="item">' +
            '                    <div class="small-title">' + 'LEGENDARY' + '</div>\n' +
            '                    <div class="product-info">' + data.legendary + '</div>\n' +
            '                 </div>');
        if (data.type == 'configurable') {
            let $size = $('<div class="item">' +
                '               <div class="small-title">' + 'SIZE' + '</div>' +
                '               <div class="dropdown-custom">' +
                '               </div>' +
                '           </div>');
            let $sizePicker = $('<select class="selectpicker">' +
                '<option disabled selected>Please Choose Size of pizza !!!</option>' +
                data.variants.map(function (item, index) {
                    return '<option data-id-product = "' + data.id + '" ' + 'data-name="' + item.name + '"' + 'data-price ="' + item.price + '"' + 'data-index ="' + index + '"' + ' value="' + item.id + '" ' + '>' + item.name + '     ' + formatCurrency(item.price) + '</option>';
                }) +
                '</select>');
            $size.find('.dropdown-custom').append($sizePicker);
            el.find('.modal-body').html('').append([$image, $title, $legendary, $size]);
        } else if (data.type == 'simple' || data.type == 'combo') {
            let $price = $('<div class="item">' +
                '               <div class="small-title">' + 'PRICE' + '</div>' +
                '               <div class="dropdown-custom">' +
                '               </div>' +
                '           </div>');
            let $priceProduct = $('<div class="price-product">' +
                formatCurrency(data.prices) +
                '</div>');
            $price.find('.dropdown-custom').append($priceProduct);
            total = data.prices;
            el.find('.modal-body').html('').append([$image, $title, $legendary, $price]);
            quantity = 1;
            $('.modal-footer').find('.quantity-number').html(quantity);
        }

        //append modal-footer
        let $total = $('<div class="total-item justify-content-between align-items-center">' +
            '               <div>' + 'Total' + '</div>' +
            '             </div>');
        let $totalValue = $('<div class="total-price" id="product-modal-price">' + formatCurrency(total) + '</div>');
        let $quantityTittle = $('<div class="quantity-title">' + 'Select a quantity' + '</div>');
        let $buttonBlock = $('<div class="d-flex justify-content-between align-items-center"></div>');
        let $quantityBlock = $('<div class="d-flex align-items-center"></div>');
        let $quantityMinus = $('<div class="quantity-icon d-flex align-items-center quantity-minus"><span>-</span></div>');
        let $quantity = $('<div class="quantity-number">' + quantity + '</div>');
        let $quantityPlus = $('<div class="quantity-icon active d-flex align-items-center quantity-plus"><span>+</span></div>');
        $quantityBlock.append([$quantityMinus, $quantity, $quantityPlus]);

        let $addButton = $('<div class="btn btn-yellow d-flex justify-content-center align-items-center"><span class="icon-add-card"></span>' + 'ADD TO CART' + '</div>');

        $buttonBlock.append([$quantityBlock, $addButton]);
        $total.append($totalValue);
        el.find('.modal-footer').html('').append([$total, $quantityTittle, $buttonBlock]);
    }

    function addToCart() {
        $.ajax({
            type: 'POST',
            url: getUrl('add_product_to_order'),
            data: {
                product: selectProduct,
                ChildIdProduct : ChildIdProduct,
                quantity: quantity,
                addOns: addOns,
                idProduct: idProduct,
                total: parseInt(total) * parseInt(quantity)
            },
            success: function (response) {
                if (typeof (response) == 'string') {
                    response = JSON.parse(response);
                }
                if (response.success) {
                    cartRender(response.data, $('.custom-header-item.cart-item'));
                }
            }
        });
    }

    function cartRender(data, els) {
        els.html('');
        let $cartLabel = $('<div class="btn my-sm-0 cart-item d-flex justify-content-center show-your-cart">' +
            '                     <span class="icon-cart click-your-cart"></span><a class="desktop-item click-your-cart"> ' + 'YOUR CART' + '</a>' +
            '                 </div>');
        let $cartQuantity = $('<div class="quantity-product available click-your-cart' +
            '">(' + Object.keys(data.products).length + ')</div>');
        $cartLabel.append($cartQuantity);
        let $cartTitle = $('<div class="your-cart display-none"><div class="your-cart-title">' + 'YOUR CART' + '</div></div>');
        let $cartItems = $('<div class="your-cart-list"></div>');
        let $discount = $('<div class="discount d-flex justify-content-between align-items-center">' +
            '                   <div>' + 'discount'.toUpperCase() + '</div>' +
            '                </div>');
        // let $promotionCode = $('<div class="promotion-code">' + promotionCode + '<b> - ' + this.formatCurrency(this.discountTotal) + '</b></div>');
        // $discount.append($promotionCode);
        let $cartTotalBlock = $('<div class="total-item d-flex justify-content-between align-items-center">' +
            '                   <div>' + 'TOTAL' + '</div>' +
            '                </div>');
        var totalBill = 0;
        $.each(data.products, function (index, item) {
            totalBill += parseInt(item.price);
        });
        let $cartTotal = $('<div class="total-price">' + formatCurrency(totalBill) + '</div>');
        $cartTotalBlock.append($cartTotal);
        let $checkoutButton = $('<a href="#" id="checkoutOrder" class="btn btn-yellow">' + 'CHECK OUT' + '</a>');
        $cartTitle.append([$cartItems, $discount, $cartTotalBlock, $checkoutButton]);
        els.append([$cartLabel, $cartTitle]);
        childEls['quantity'] = $cartQuantity;
        childEls['cartItems'] = $cartItems;
        childEls['cartTotal'] = $cartTotal;
        // childEls['promotionCode'] = $promotionCode;
        renderProductList(data, childEls);
    }

    function renderProductList(data) {
        let _this = this;
        childEls['quantity'].html('(' + Object.keys(data.products).length + ')');
        childEls['cartItems'].html('');
        var totallAll = 0;
        $.each(data.products, function (index, item) {
            totallAll += parseInt(item.price);
            let $cartItem = $('<div class="your-cart-item"></div>');
            let $productItem = $('<div class="d-flex justify-content-between"></div>');
            let totalItem = 0;
            let discountTotal = 0;
            let productPrice = getNumberValue(item.price);
            totalItem = productPrice;
            $productItem.append($('<div class="cart-item-product-info d-flex">' +
                '                   <div class="img-item">' +
                '                       <img src="' + item.image + '" alt="">' +
                '                   </div>' +
                '                   <div class="text-item">' +
                '                       <div>' + item.name +
                '                           </div>' +
                item.addOns.map(function (addOn) {
                    return '<p>' + addOn.addons_type + ' : ' + addOn.name + '</p>'
                }).join('') +
                '                       </div>' +
                '                   </div>'));
            let $iconRemove = $('<span data-key="' + index + '" class="icon-cancel icon-custom"></span>');
            $productItem.append($iconRemove);
            $cartItem.append($productItem);
            let $quantityBlock = $('<div class="d-flex justify-content-between">' +
                '                       <div class="quantity-item d-flex aligan-items-center"></div>' +
                '                   </div>');
            let $iconMinus = $('<span data-quantity="' + item.quantity + '" data-key="' + index + '" class="icon-minus icon-custom"></span>');
            let $quantity = $('<b>' + item.quantity + '</b>');
            let $iconPlus = $('<span data-quantity="' + item.quantity + '" data-key="' + index + '" class="icon-plus icon-custom available"></span>');
            $quantityBlock.find('.quantity-item').append([$iconMinus, $quantity, $iconPlus]);
            let $totalItem = $('<div class="price-item">' + formatCurrency(totalItem) + '</div>');
            $quantityBlock.append($totalItem);
            $cartItem.append([$productItem, $quantityBlock]);
            childEls['cartItems'].append($cartItem);
        });
        childEls['cartTotal'].html(formatCurrency(totallAll));
    }

    function getNumberValue(text) {
        if (typeof (text) === 'string') {
            text = parseInt(text.replace(/[^\w\s]/gi, ''));
        }
        if (isNaN(text)) {
            text = 0;
        }
        return text;
    }

    function removeItem(key) {
        let _this = this;
        var ajaxUrlPDF = _domainUrl + '/wp-json' + '/' + namespace + '/v' + version + '/remove_order_product';
        $.ajax({
            type: 'POST',
            url: ajaxUrlPDF,
            data: {id: key},
            success: function (response) {
                if (typeof (response) == 'string') {
                    response = JSON.parse(response);
                }
                if (response.success) {
                    renderProductList(response.data);
                }
            }
        });
    }

    function updateCartData(key, quantityProduct, oldQuatity) {
        let _this = this;
        var ajaxUrlPDF = _domainUrl + '/wp-json' + '/' + namespace + '/v' + version + '/update_quantity';
        $.ajax({
            type: 'POST',
            url: ajaxUrlPDF,
            data: {
                key: key,
                quantity: quantityProduct,
                oldQuantity: oldQuatity
            },
            success: function (response) {
                if (typeof (response) == 'string') {
                    response = JSON.parse(response);
                }
                if (response.success) {
                    renderProductList(response.data);
                }
            }
        })
    }

    function getCartdetails() {
        let _this = $(this);
        var ajaxUrlPDF = _domainUrl + '/wp-json' + '/' + namespace + '/v' + version + '/cart_details';
        $.ajax({
            type: "GET",
            url: ajaxUrlPDF,
            success: function (response) {
                if (typeof (response) == "string") {
                    response = JSON.parse(response);
                }
                if (response.success) {
                    cartRender(response.data, $('.custom-header-item.cart-item'));
                }
            }
        });
    }

    var map;

    function myMap(idMap) {
        var myLatLng = {lat: 10.794280, lng: 106.644940};
        map = new google.maps.Map(document.getElementById(idMap), {
            zoom: 17,
            center: myLatLng
        });
        if (locations.length > 0) {
            locations.forEach(function (item) {
                new google.maps.Marker({
                    position: {
                        lat: item.lat,
                        lng: item.lng
                    },
                    title: item.name,
                    map: map
                });
                map.setCenter({
                    lat: item.lat,
                    lng: item.lng
                });
            });
        }
    }

    if ($('#map').length > 0) {
        myMap('map');

        // click icon address to view location on map
        $('.section-stores .icon-item').on('click', function () {
            var lat = $(this).data('lat');
            var lng = $(this).data('lng');
            var center = new google.maps.LatLng(lat, lng);
            map.panTo(center);
        });
    }

    if ($('#map-without-account').length > 0) {
        myMap('map-without-account');

    }

    $('.modal-body').on('change', '#pickup-store', function () {
        var optionSelected = $("option:selected", this);
        var lat = optionSelected.attr('data-lat');
        var lng = optionSelected.attr('data-lng');
        var center = new google.maps.LatLng(lat, lng);
        map.panTo(center);
    });

    $('#order-without-account').on('click', function () {
        myMap('map-without-account');
    });

    $('#order-with-account').on('click', function () {
        myMap('map-with-account');
    });

    $(document).on('submit', 'form.user-form', function (e) {
        var data = getInputVal($(this));
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: getUrl('save_info_customer'),
            data: {
                infoCustomer: data
            },
            success: function (response) {
                if (typeof (response) == 'string') {
                    response = JSON.parse(response);
                }
                if (response.success) {
                    // window.location.href = _domainUrl + '/checkout';
                    console.log(response);
                }
            }
        });
    });

    $(document).on('submit', 'form.checkout-form', function (e) {
        var data = getInputVal($(this));
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: getUrl('order_product'),
            data: {
                infoOrder : data
            },
            success: function (response){
                if(typeof (response) == 'string'){
                    response = JSON.parse(response);
                }
                if (response.success){
                    console.log(response);
                }
            }
        });
    });

    //API Login

    $(document).on('submit', 'form.login-form', function (e) {
        var data = getInputVal($(this));
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: getUrl('login_form'),
            data: {
                username: data.username,
                password: data.pwd
            },
            success: function (response) {
                if (typeof (response) == 'string') {
                    response = JSON.parse(response);
                }
                if (response.success) {
                    if (response.error) {
                        alert(response.data.errors.errors[0].message);
                    } else {
                        $('#with_account_pickup .h-100').hide(300);
                        $('#with_account .h-100').hide(300);
                        $('#pickup-profile option').html('').append(response.data.profiles.name + " ------ " + response.data.profiles.phone);
                        $('#delivery-address-without-account option').html('').append(response.data.profiles.address);
                        $('#pickup-profile').parents('form').find("input[name~='name']").val(response.data.profiles.name);
                        $('#pickup-profile').parents('form').find("input[name~='phone']").val(response.data.profiles.phone);
                        $('#delivery-address-without-account').parents('form').find("input[name~='address']").val(response.data.profiles.address);
                        $('#delivery-address-without-account').parents('form').find("input[name~='name']").val(response.data.profiles.name);
                        $('#delivery-address-without-account').parents('form').find("input[name~='phone']").val(response.data.profiles.phone);
                        $('#with_account_pickup .user-form').show(300);
                        $('#with_account .user-form').show(300);
                    }
                }
            }
        });
    });

    $(document).on('submit', 'form.login-page-form', function (e) {
        var data = getInputVal($(this));
        e.preventDefault();
        var ajaxUrlPDF = getUrl('login_form');
        $.ajax({
            type: 'POST',
            url: ajaxUrlPDF,
            data: {
                username: data.username,
                password: data.pwd
            },
            success: function (response) {
                if (typeof (response) == 'string') {
                    response = JSON.parse(response);
                }
                if (response.success) {
                    if (response.error) {
                        alert(response.data.errors.errors[0].message);
                    } else {
                        console.log(response);
                        // document.location.href = _domainUrl;
                    }
                }
            }
        });
    });

    $(document).on('submit', 'form.form-reset-pwd', function (e) {
        var data = getInputVal($(this));
        e.preventDefault();
        var ajaxUrlPDF = getUrl('reset_pwd');
        $.ajax({
            type: 'GET',
            url: ajaxUrlPDF,
            data: {
                username: data.username
            },
            success: function (response) {
                if (typeof (response) == 'string') {
                    response = JSON.parse(response);
                }
                if (response.success) {
                    if (response.data.error) {
                        $('.form-reset-pwd').find('.alert-success').hide(300);
                        $('.form-reset-pwd').find('.alert-danger strong').html('').html(response.data.errors[0].message);
                        $('.form-reset-pwd').find('.alert-danger').show(300);
                    } else {
                        $('.form-reset-pwd').hide(300);
                        $('.form-otp-reset-pwd').find('.alert-success strong').html('').html(response.data.data[0].message);
                        $('.form-otp-reset-pwd').find("input[name~='username_reset_pwd']").val(response.username);
                        $('.form-otp-reset-pwd').find('.alert-success').show(300);
                        $('.form-otp-reset-pwd').show(300);
                    }
                }
            }
        });
    });

    $(document).on('submit', 'form.form-otp-reset-pwd', function (e) {
        var data = getInputVal($(this));
        e.preventDefault();
        var ajaxUrlPDF = _domainUrl + '/wp-json' + '/' + namespace + '/v' + version + '/otp_reset_pwd';
        var ajaxUrlPDF = getUrl('otp_reset_pwd');
        $.ajax({
            type: 'GET',
            url: ajaxUrlPDF,
            data: {
                otp_reset_pwd: data.otp_reset_pwd,
                username_reset_pwd: data.username_reset_pwd
            },
            success: function (response) {
                if (typeof (response) == 'string') {
                    response = JSON.parse(response);
                }
                if (response.success) {
                    if (response.data.error) {
                        $('.form-otp-reset-pwd').find('.alert-success').hide(300);
                        $('.form-otp-reset-pwd').find('.alert-danger strong').html('').html(response.data.errors[0].message);
                        $('.form-otp-reset-pwd').find('.alert-danger').show(300);
                    } else {
                        $('.form-otp-reset-pwd').hide(300);
                        $('.form-new-pwd').find("input[name~='reminderCode']").val(response.data.data.reminder_code);
                        $('.form-new-pwd').find("input[name~='userId']").val(response.data.data.userId);
                        $('.form-new-pwd').show(300);
                    }
                }
            }
        });
    });

    $(document).on('submit', 'form.form-new-pwd', function (e) {
        var data = getInputVal($(this));
        e.preventDefault();
        var ajaxUrlPDF = getUrl('new_pwd');
        $.ajax({
            type: 'POST',
            url: ajaxUrlPDF,
            data: {
                reminderCode: data.reminderCode,
                userId: data.userId,
                newpwd: data.newpwd,
                confirm_new_pwd: data.confirm_new_pwd
            },
            success: function (response) {
                if (response.success) {
                    if (response.data.error) {
                        $('.form-new-pwd').find('.alert-success').hide(300);
                        $('.form-new-pwd').find('.alert-danger strong').html('').html(response.data.errors[0].message);
                        $('.form-new-pwd').find('.alert-danger').show(300);
                    } else {
                        console.log(response);
                        $('.form-new-pwd').hide(300);
                        $('.successful-reset').html('').html(response.data.data[0].message).show(300);
                        setTimeout(function () {
                            document.location.href = _domainUrl;
                        }, 3000);
                    }
                }
            }
        });
    });

    $(document).on('submit', 'form.form-change-profile', function (e) {
        var data = getInputVal($(this));
        e.preventDefault();
        var ajaxUrlPDF = getUrl('change_profile');
        var file_data = $('#file-image').prop('files')[0];
        var formData = new FormData();
        formData.append('file', file_data);
        formData.append('first_name', data.first_name);
        formData.append('last_name', data.last_name);
        formData.append('phone_number', data.phone_number);
        formData.append('email_profile', data.email_profile);
        $.ajax({
            type: 'POST',
            url: ajaxUrlPDF,
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                if (response.success) {
                    if (response.error) {
                        alert(response.data.errors[0].message);
                    } else {
                        document.location.href = _domainUrl + '/my-account';
                    }
                }
            }
        });
    });

    $(document).on('submit', 'form.form-change-pwd', function (e) {
        var data = getInputVal($(this));
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: getUrl('change_pwd'),
            data: {
                'old_pwd': data.old_pwd,
                'new_pwd': data.new_pwd,
                'confirm_new_pwd': data.confirm_new_pwd
            },
            success: function (response) {
                if (response.success) {
                    if (response.data.error) {
                        $('.form-change-pwd').find('.alert-success').hide(300);
                        $('.form-change-pwd').find('.alert-danger strong').html('').html(response.data.errors[0].message);
                        $('.form-change-pwd').find('.alert-danger').show(300);
                    } else {
                        $('.form-change-pwd').find('.alert-danger').hide(300);
                        $('.form-change-pwd').find('.alert-success').html('').html(response.data.data).show(300);
                        setTimeout(function () {
                            document.location.href = _domainUrl + '/my-account';
                        }, 2000);
                    }
                }
            }
        });
    });

    $(document).on('submit', 'form.form-add-address', function (e) {
        var data = getInputVal($(this));
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: getUrl('add_new_address'),
            data: {
                'infoAddress': data
            },
            success: function (response) {
                if (response.success) {
                    if (response.data.error) {
                        console.log(response);
                    } else {
                        console.log(response);
                    }
                }
            }
        });
    });

    $(document).on('submit', 'form.form-edit-address', function (e) {
        var data = getInputVal($(this));
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: getUrl('edit_address'),
            data: {
                'infoAddress': data
            },
            success: function (response) {
                if (response.success) {
                    if (response.error) {
                        alert(response.data.errors[0].message)
                    } else {
                        document.location.href = _domainUrl + '/address-book';
                    }
                }
            }
        });
    });

    // API Register

    (function () {
        'use strict';
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('form-register');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();

    $(document).on('submit', 'form.form-activation', function (e) {
        var data = getInputVal($(this));
        e.preventDefault();
        console.log(data.account, data.otp);
        var ajaxUrlPDF = getUrl('activation_form');
        $.ajax({
            type: 'GET',
            url: ajaxUrlPDF,
            data: {
                username: data.account,
                otp: data.otp
            },
            success: function (response) {
                if (typeof (response) == 'string') {
                    response = JSON.parse(response);
                }
                if (response.success) {
                    if (response.data.error) {
                        $('.activation-info').find('.alert-success').hide(300);
                        $('.activation-info').find('.alert-danger strong').html('').html(response.data.errors[0].message);
                        $('.activation-info').find('.alert-danger').show(300);

                    } else {
                        $('.activation-info').find('.alert-success strong').html('').html(response.data.data[0].message);
                        $('.activation-info').find('.alert-danger').hide(300);
                        $('.activation-info').find('.alert-success').show(300);
                    }
                }
            }
        });
    });

    $('.resend-otp').on('click', function () {
        var _this = $(this);
        var username = _this.parents('form').find("input[name~='account']").val();
        var ajaxUrlPDF = getUrl('resend_otp');
        console.log(username);
        $.ajax({
            method: "GET",
            url: ajaxUrlPDF,
            data: {
                username: username
            },
            success: function (response) {
                if (typeof (response) == 'string') {
                    response = JSON.parse(response);
                }
                if (response.success) {
                    console.log(response);
                    if (response.data.error) {
                        $('.activation-info').find('.alert-success').hide(300);
                        $('.activation-info').find('.alert-danger strong').html(response.data.errors[0].message);
                        $('.activation-info').find('.alert-danger').show(300);
                    } else {
                        $('.activation-info').find('.alert-success strong').html(response.data.data[0].message);
                        $('.activation-info').find('.alert-danger').hide(300);
                        $('.activation-info').find('.alert-success').show(300);
                    }
                }
            }
        });

    });

    $(document).on('submit', 'form.form-register', function (e) {
        var data = getInputVal($(this));
        e.preventDefault();
        var ajaxUrlPDF = getUrl('register_form');
        $.ajax({
            type: 'POST',
            url: ajaxUrlPDF,
            data: {
                first_name: data.first_name,
                last_name: data.last_name,
                username: data.username,
                phone: data.phone,
                password: data.password,
                password_confirmation: data.password_confirmation,
                gender: data.gender,
                dob: data.dob,
                address: data.address,
                brand_id: data.brand_id
            },
            success: function (response) {
                if (typeof (response) == 'string') {
                    response = JSON.parse(response);
                }
                if (response.success) {
                    if (response.data.error) {
                        alert(response.data.errors[0].message);
                    } else {
                        $('.form-register').hide(300);
                        $('.activation-info label.activation-message').html('').html(response.data.data[0].message);
                        $('.activation-info').find("input[name~='username-activation']").val(response.username);
                        $('.form-activation').show(300);
                    }
                }
            }
        });
    });

    $('#password, #password_confirmation').on('keyup', function () {
        if ($('#password').val() == $('#password_confirmation').val()) {
            $('#message').html('Matching').css('color', 'green');
            $('#actionChangePwd').removeAttr("disabled");
        } else {
            $('#message').html('Not Matching').css('color', 'red');
            $('#actionChangePwd').attr('disabled', 'disabled');
        }
    });

    $('#password, #old_password').on('keyup', function () {
        if ($('#password').val() == $('#old_password').val()) {
            $('#messageError').html('The current password must be different from the new password\n ').css('color', 'red');
            $('#actionChangePwd').attr('disabled', 'disabled');
        } else {
            $('#messageError').html('');
            $('#actionChangePwd').removeAttr("disabled");
        }
    });

    // API Log out

    $('#log_out_action').on('click', function () {
        console.log('Okay Okay');
        $.ajax({
            type: 'POST',
            url: getUrl('log_out'),
            data: {
                log_out: 'log out now !!!'
            },
            success: function (response) {
                if (response.success) {
                    if (response.data.error) {
                        alert(response.data.errors[0].message);
                    } else {
                        console.log(response);
                    }
                }
            }
        });
    });

    //Modal Address

    $('.add-new-address span').on('click', function () {
        $('#add_address').modal('show');
    });

    $('.icon-edit-address').on('click', function () {
        $.ajax({
            type: 'GET',
            url: getUrl('get_details_address'),
            data: {
                idAddress: $(this).data('id')
            },
            success: function (response) {
                if (response.success) {
                    if (response.error) {
                        alert(response.data.errors[0].message);
                    } else {

                        $('form.form-edit-address').find("input[name~='name_address']").val(response.data.data.name);
                        $('form.form-edit-address').find("input[name~='phone_address']").val(response.data.data.phone);
                        $('form.form-edit-address').find("input[name~='building_name_address']").val(response.data.data.building_name);
                        $('form.form-edit-address').find("input[name~='address_nickname']").val(response.data.data.address_nickname);
                        $('form.form-edit-address').find("input[name~='address']").val(response.data.data.address1);
                        $('form.form-edit-address').find("input[name~='country_address']").val(response.data.data.country);
                        $('form.form-edit-address').find("input[name~='state_address']").val(response.data.data.state);
                        $('form.form-edit-address').find("input[name~='city_address']").val(response.data.data.city);
                        $('form.form-edit-address').find("input[name~='postcode_address']").val(response.data.data.postcode);
                        $('form.form-edit-address').find("input[name~='latitude']").val(response.data.data.latitude);
                        $('form.form-edit-address').find("input[name~='longitude']").val(response.data.data.longitude);
                        $('form.form-edit-address').find("input[name~='id_address']").val(response.data.data.id);
                        $('#edit_address').modal('show');
                    }
                }
            }
        });
    });

    $('.icon-remove-address').on('click', function () {
        if (confirm('Do you really want to delete it?')) {
            $.ajax({
                type: 'POST',
                url: getUrl('delete_address'),
                data: {
                    idAddress: $(this).data('id')
                },
                success: function (response) {
                    if (response.success) {
                        if (response.error) {
                            alert(response.data.errors[0].message);
                        } else {
                            document.location.href = _domainUrl + '/address-book';
                        }
                    }
                }
            });
        }
    });


});