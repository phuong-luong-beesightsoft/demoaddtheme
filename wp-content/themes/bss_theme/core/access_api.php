<?php

function access_cxi($option, $lastUpdateTime, $catelog = 'catalog', $productDetail = null)
{
    global $bss_options;
    if ($catelog != null) {
        $url = $bss_options['API_CXI_URL'] . '/admin/' . $catelog . '/' . $option .
            '?page=1
            &per_page=1000
            &filters={"match":"and","rules":[{
            "field": "updated_at",
            "operator": ">=",
            "value": "' . $lastUpdateTime . '"
            }]}
            &organization_id=' . $bss_options['ORGANIZATION_ID'] .
            '&brand_id=' . $bss_options['BRAND_ID'];
        if ($option == "products") {
            if (empty(productList::all())) {
                $url = $bss_options['API_CXI_URL'] . '/admin/' . $catelog . '/' . $option .
                    '?page=1
            &per_page=1000
            &filters={"match":"and","rules":[]}
            &organization_id=' . $bss_options['ORGANIZATION_ID'] .
                    '&brand_id=' . $bss_options['BRAND_ID'];
            }
        } else if ($option == "categories") {
            if (empty(productCategories::all())) {
                $url = $bss_options['API_CXI_URL'] . '/admin/' . $catelog . '/' . $option .
                    '?page=1
            &per_page=1000
            &filters={"match":"and","rules":[]}
            &organization_id=' . $bss_options['ORGANIZATION_ID'] .
                    '&brand_id=' . $bss_options['BRAND_ID'];
            }
        } else if ($option == "attributes") {
            if (empty(productAttribute::all())) {
                $url = $bss_options['API_CXI_URL'] . '/admin/' . $catelog . '/' . $option .
                    '?page=1
            &per_page=1000
            &filters={"match":"and","rules":[]}
            &organization_id=' . $bss_options['ORGANIZATION_ID'] .
                    '&brand_id=' . $bss_options['BRAND_ID'];
            }
        } else if ($option == "addons") {
            if (empty(productAddonDetail::all())) {
                $url = $bss_options['API_CXI_URL'] . '/admin/' . $catelog . '/' . $option .
                    '?page=1
            &per_page=1000
            &filters={"match":"and","rules":[]}
            &organization_id=' . $bss_options['ORGANIZATION_ID'] .
                    '&brand_id=' . $bss_options['BRAND_ID'];
            }
        } else if ($option == "addon-type") {
            if (empty(productAddonTypeDetail::all())) {
                $url = $bss_options['API_CXI_URL'] . '/admin/' . $catelog . '/' . $option .
                    '?page=1
            &per_page=1000
            &filters={"match":"and","rules":[]}
            &organization_id=' . $bss_options['ORGANIZATION_ID'] .
                    '&brand_id=' . $bss_options['BRAND_ID'];
            }
        } else if ($option == "promotions") {
            if (empty(productPromotionDetail::all())) {
                $url = $bss_options['API_CXI_URL'] . '/admin/' . $catelog . '/' . $option .
                    '?page=1
            &per_page=1000
            &filters={"match":"and","rules":[]}
            &organization_id=' . $bss_options['ORGANIZATION_ID'] .
                    '&brand_id=' . $bss_options['BRAND_ID'];
            }
        } else if ($option == "gifts") {
            if (empty(productGift::all())) {
                $url = $bss_options['API_CXI_URL'] . '/admin/' . $catelog . '/' . $option .
                    '?page=1
            &per_page=1000
            &filters={"match":"and","rules":[]}
            &organization_id=' . $bss_options['ORGANIZATION_ID'] .
                    '&brand_id=' . $bss_options['BRAND_ID'];
            }
        } else if ($option == "gift-categories") {
            if (empty(productGiftcategories::all())) {
                $url = $bss_options['API_CXI_URL'] . '/admin/' . $catelog . '/' . $option .
                    '?page=1
            &per_page=1000
            &filters={"match":"and","rules":[]}
            &organization_id=' . $bss_options['ORGANIZATION_ID'] .
                    '&brand_id=' . $bss_options['BRAND_ID'];
            }
        } else if ($option == 'stores') {
            $url = $bss_options['API_CXI_URL'] . '/admin/brands/' . $bss_options['BRAND_ID'] . '/outlets?organization_id=' . $bss_options['ORGANIZATION_ID'];
        }
    } else {
        $url = $bss_options['API_CXI_URL'] . '/admin/' . $option .
            '?page=1
            &per_page=1000
            &filters={"match":"and","rules":[{
            "field": "updated_at",
            "operator": ">=",
            "value": "' . $lastUpdateTime . '"
            }]}
            &organization_id=' . $bss_options['ORGANIZATION_ID'] .
            '&brand_id=' . $bss_options['BRAND_ID'];
        if ($option == "products") {
            if (empty(productList::all())) {
                $url = $bss_options['API_CXI_URL'] . '/admin/' . $option .
                    '?page=1
            &per_page=1000
            &filters={"match":"and","rules":[]}
            &organization_id=' . $bss_options['ORGANIZATION_ID'] .
                    '&brand_id=' . $bss_options['BRAND_ID'];
            }
        } else if ($option == "categories") {
            if (empty(productCategories::all())) {
                $url = $bss_options['API_CXI_URL'] . '/admin/' . $option .
                    '?page=1
            &per_page=1000
            &filters={"match":"and","rules":[]}
            &organization_id=' . $bss_options['ORGANIZATION_ID'] .
                    '&brand_id=' . $bss_options['BRAND_ID'];
            }
        } else if ($option == "attributes") {
            if (empty(productAttribute::all())) {
                $url = $bss_options['API_CXI_URL'] . '/admin/' . $option .
                    '?page=1
            &per_page=1000
            &filters={"match":"and","rules":[]}
            &organization_id=' . $bss_options['ORGANIZATION_ID'] .
                    '&brand_id=' . $bss_options['BRAND_ID'];
            }
        } else if ($option == "addons") {
            if (empty(productAddonDetail::all())) {
                $url = $bss_options['API_CXI_URL'] . '/admin/' . $option .
                    '?page=1
            &per_page=1000
            &filters={"match":"and","rules":[]}
            &organization_id=' . $bss_options['ORGANIZATION_ID'] .
                    '&brand_id=' . $bss_options['BRAND_ID'];
            }
        } else if ($option == "addon-type") {
            if (empty(productAddonTypeDetail::all())) {
                $url = $bss_options['API_CXI_URL'] . '/admin/' . $option .
                    '?page=1
            &per_page=1000
            &filters={"match":"and","rules":[]}
            &organization_id=' . $bss_options['ORGANIZATION_ID'] .
                    '&brand_id=' . $bss_options['BRAND_ID'];
            }
        } else if ($option == "promotions") {
            if (empty(productPromotionDetail::all())) {
                $url = $bss_options['API_CXI_URL'] . '/admin/' . $option .
                    '?page=1
            &per_page=1000
            &filters={"match":"and","rules":[]}
            &organization_id=' . $bss_options['ORGANIZATION_ID'] .
                    '&brand_id=' . $bss_options['BRAND_ID'];
            }
        } else if ($option == "gifts") {
            if (empty(productGift::all())) {
                $url = $bss_options['API_CXI_URL'] . '/admin/' . $option .
                    '?page=1
            &per_page=1000
            &filters={"match":"and","rules":[]}
            &organization_id=' . $bss_options['ORGANIZATION_ID'] .
                    '&brand_id=' . $bss_options['BRAND_ID'];
            }
        } else if ($option == "gift-categories") {
            if (empty(productGiftcategories::all())) {
                $url = $bss_options['API_CXI_URL'] . '/admin/' . $option .
                    '?page=1
            &per_page=1000
            &filters={"match":"and","rules":[]}
            &organization_id=' . $bss_options['ORGANIZATION_ID'] .
                    '&brand_id=' . $bss_options['BRAND_ID'];
            }
        } else if ($option == 'stores') {
            $url = $bss_options['API_CXI_URL'] . '/admin/brands/' . $bss_options['BRAND_ID'] . '/outlets?organization_id=' . $bss_options['ORGANIZATION_ID'];
        }
    }
    if ($productDetail != null) {
        $url = $bss_options['API_CXI_URL'] . '/admin/catalog/products/' . $productDetail . '?organization_id=' . $bss_options['ORGANIZATION_ID'];
    }
    $args = array(
        'headers' => array(
            'X-APP-ID' => $bss_options['API_CXI_X_APP_ID'],
            'Authorization' => $bss_options['API_CXI_TOKEN']
        )
    );
    $response = wp_remote_get($url, $args);
    if (is_wp_error($response)) {
        return false;
    }
    $body = wp_remote_retrieve_body($response);
    $data = json_decode($body);

    return $data;
}

function ListData($option)
{
    global $bss_options;
    if ($option == "products") {
        $cntData = productList::countall();
        lastUpdateTime::update(['product_time' => date("Y-m-d H:i:s", time())], ['id' => 1]);
    } else if ($option == 'categories') {
        $cntData = productCategories::countall();
        lastUpdateTime::update(['categories_time' => date("Y-m-d H:i:s", time())], ['id' => 1]);
    } else if ($option == 'attributes') {
        $cntData = productAttribute::countall();
        lastUpdateTime::update(['attribute_time' => date("Y-m-d H:i:s", time())], ['id' => 1]);
    } else if ($option == 'addons') {
        $cntData = productAddonDetail::countall();
        lastUpdateTime::update(['product_addon_time' => date("Y-m-d H:i:s", time())], ['id' => 1]);
    } else if ($option == 'addon-type') {
        $cntData = productAddonTypeDetail::countall();
        lastUpdateTime::update(['product_addon_type_time' => date("Y-m-d H:i:s", time())], ['id' => 1]);
    } else if ($option == 'promotions') {
        $cntData = productPromotionDetail::countall();
        lastUpdateTime::update(['promotion_time' => date("Y-m-d H:i:s", time())], ['id' => 1]);
    } else if ($option == 'gifts') {
        $cntData = productGift::countall();
        lastUpdateTime::update(['gift_last_update_time' => date("Y-m-d H:i:s", time())], ['id' => 1]);
    } else if ($option == 'gift-categories') {
        $cntData = productGiftcategories::countall();
        lastUpdateTime::update(['gift_categories_time' => date("Y-m-d H:i:s", time())], ['id' => 1]);
    } else if ($option == 'stores') {
        $cntData = storeDetail::countall();
        lastUpdateTime::update(['store_detail_time' => date("Y-m-d H:i:s", time())], ['id' => 1]);
    }
    $params = array(
        'conditions' => array(
            '1'
        )
    );
    $total = $cntData->total;
    $limit = 10;
    $page = (isset($_GET['paged']) && (int)$_GET['paged'] >= 1) ? $_GET['paged'] : 1;
    if ($page > ceil($total / $limit)) {
        $page = ceil($total / $limit);
    }
    $params['offset'] = $limit * ($page - 1);
    $params['limit'] = $limit;
    if ($option == "products") {
        $list_data = productList::find('all', $params);
    } else if ($option == 'categories') {
        $list_data = productCategories::find('all', $params);
    } else if ($option == 'attributes') {
        $list_data = productAttribute::find('all', $params);
    } else if ($option == 'addons') {
        $list_data = productAddonDetail::find('all', $params);
    } else if ($option == 'addon-type') {
        $list_data = productAddonTypeDetail::find('all', $params);
    } else if ($option == 'promotions') {
        $list_data = productPromotionDetail::find('all', $params);
    } else if ($option == 'gifts') {
        $list_data = productGift::find('all', $params);
    } else if ($option == 'gift-categories') {
        $list_data = productGiftcategories::find('all', $params);
    } else if ($option == 'stores') {
        $list_data = storeDetail::find('all', $params);
    }
    $fetch_data = array();
    $fetch_data['list_data'] = $list_data;
    $fetch_data['total'] = $total;
    $fetch_data['limit'] = $limit;
    $fetch_data['page'] = $page;
    return $fetch_data;
}

function get_data_from_db($option)
{
    global $bss_options;
    $updateLastTime = lastUpdateTime::all();
    if ($option == "products") {
        $data = access_cxi($option, $updateLastTime[0]->product_time);
        if (!empty($data->data)) {
            foreach ($data->data as $key => $value) {
                if (productList::findByProductId($value->id) == false) {
                    $productDetail = access_cxi($option, $updateLastTime[0]->product_time, 'catalog', $value->id);
                    $productSubmit = productList::create([
                        'product_id' => $value->id,
                        'name' => $value->name,
                        'image' => $bss_options['CXI_URL'] . $value->images[0]->original_image,
                        'description' => $value->description,
                        'price' => $value->price,
                        'enable' => 1,
                        'categories_id' => $productDetail->data->categories[0]->id
                    ]);
                }
                if (productDetail::findByProductDetailId($value->id) == false) {
                    $productDetail = access_cxi($option, $updateLastTime[0]->product_time, 'catalog', $value->id);
                    if ($productDetail != null) {
                        $productDetail = $productDetail->data;
                        $categoriesProduct = $productDetail->categories[0]->id;
                        $productDetail->categories = json_encode($productDetail->categories);
                        $productDetail->variants = json_encode($productDetail->variants);
                        if (isset($productDetail->super_attributes)) {
                            $productDetail->super_attributes = json_encode($productDetail->super_attributes);
                        } else {
                            $productDetail->super_attributes = null;
                        }
                        if (isset($productDetail->combo)) {
                            $productDetail->combo = json_encode($productDetail->combo);
                        } else {
                            $productDetail->combo = null;
                        }
                        $productSubmit = productDetail::create([
                            'product_id' => $productDetail->id,
                            'type' => $productDetail->type,
                            'name' => $productDetail->name,
                            'status' => $productDetail->status,
                            'price' => $productDetail->price,
                            'price_type' => $productDetail->price_type,
                            'formated_prices' => $productDetail->formated_prices,
                            'description' => $productDetail->description,
                            'sku' => $productDetail->sku,
                            'categories' => $productDetail->categories,
                            'image' => $bss_options['CXI_URL'] . $productDetail->images[0]->original_image,
                            'variants' => $productDetail->variants,
                            'super_attributes' => $productDetail->super_attributes,
                            'combo' => $productDetail->combo,
                            'categories_id' => $categoriesProduct,
                            'updated_at' => $productDetail->updated_at
                        ]);
                    }
                } else {
                    echo "OKAY OKAY OKAY";
                    $productDetail = access_cxi($option, $updateLastTime[0]->product_time, 'catalog', $value->id);
                    $productDetailFromDB = productDetail::findByProductDetailId($value->id);
                    if ($productDetail != null) {
                        $productDetail = $productDetail->data;
                        $categoriesProduct = $productDetail->categories[0]->id;
                        $productDetail->categories = json_encode($productDetail->categories);
                        $productDetail->variants = json_encode($productDetail->variants);
                        if (isset($productDetail->super_attributes)) {
                            $productDetail->super_attributes = json_encode($productDetail->super_attributes);
                        } else {
                            $productDetail->super_attributes = null;
                        }
                        if (isset($productDetail->combo)) {
                            $productDetail->combo = json_encode($productDetail->combo);
                        } else {
                            $productDetail->combo = null;
                        }
                        $updateProduct = productDetail::update(
                            [
                                'product_id' => $productDetail->id,
                                'type' => $productDetail->type,
                                'name' => $productDetail->name,
                                'status' => $productDetail->status,
                                'price' => $productDetail->price,
                                'price_type' => $productDetail->price_type,
                                'formated_prices' => $productDetail->formated_price,
                                'description' => $productDetail->description,
                                'sku' => $productDetail->sku,
                                'categories' => $productDetail->categories,
                                'image' => $bss_options['CXI_URL'] . $productDetail->images[0]->original_image,
                                'variants' => $productDetail->variants,
                                'super_attributes' => $productDetail->super_attributes,
                                'combo' => $productDetail->combo,
                                'categories_id' => $categoriesProduct,
                                'updated_at' => $productDetail->updated_at
                            ],
                            ['id' => $productDetailFromDB->id]
                        );
                    }
                }
            }
        }
        return ListData($option);
    } else if ($option == 'categories') {
        $data = access_cxi($option, $updateLastTime[0]->categories_time);
        if (!empty($data->data)) {
            foreach ($data->data as $key => $value) {
                if (productCategories::findByCategoriesId($value->id) == false) {
                    $value->children = json_encode($value->children);
                    $productSubmit = productCategories::create([
                        'categories_id' => $value->id,
                        'categories' => $value->name,
                        'image' => $bss_options['CXI_URL'] . $value->image,
                        'description' => $value->description,
                        'parent_id' => $value->parent_id,
                        'children' => $value->children,
                        'enable' => 1
                    ]);
                }
            }
        }
        return ListData($option);
    } else if ($option == 'attributes') {
        $data = access_cxi($option, $updateLastTime[0]->attribute_time);
        if (!empty($data->data)) {
            foreach ($data->data as $key => $value) {
                if (productAttribute::findByAttributeId($value->id) == false) {
                    $value->options = json_encode($value->options);
                    $productSubmit = productAttribute::create([
                        'attribute_id' => $value->id,
                        'attribute' => $value->admin_name,
                        'type' => $value->type,
                        'options' => $value->options,
                        'required' => $value->is_required,
                        'unique_attribute' => $value->is_unique,
                        'enable' => 1
                    ]);
                }
                $value->options = json_decode($value->options);
            }
        }
        return ListData($option);
    } else if ($option == 'addons') {
        $data = access_cxi($option, $updateLastTime[0]->product_addon_time);
        if (!empty($data->data)) {
            foreach ($data->data as $key => $value) {
                if (productAddonDetail::findByProductAddonId($value->id) == false) {
                    $productSubmit = productAddonDetail::create([
                        'product_add_on_id' => $value->id,
                        'product_add_on' => $value->name,
                        'image' => $bss_options['CXI_URL'] . $value->image,
                        'description' => $value->description,
                        'enable' => 1
                    ]);
                }
            }
        }
        return ListData($option);
    } else if ($option == 'addon-type') {
        $data = access_cxi($option, $updateLastTime[0]->product_addon_type_time);
        if (!empty($data->data)) {
            foreach ($data->data as $key => $value) {
                if (productAddonTypeDetail::findByProductAddonTypeId($value->id) == false) {
                    $productSubmit = productAddonTypeDetail::create([
                        'product_add_on_type_id' => $value->id,
                        'product_add_on_type' => $value->name,
                        'description' => $value->description,
                        'enable' => 1
                    ]);
                }
            }
        }
        return ListData($option);
    } else if ($option == 'promotions') {
        $data = access_cxi($option, $updateLastTime[0]->promotion_time, null);
        if (!empty($data->data)) {
            foreach ($data->data as $key => $value) {
                if (productPromotionDetail::findByProductPromotionId($value->id) == false) {
                    $productSubmit = productPromotionDetail::create([
                        'product_promotion_id' => $value->id,
                        'product_promotion' => $value->name,
                        'image' => $bss_options['CXI_URL'] . $value->image,
                        'description' => $value->description,
                        'starts_from' => $value->starts_from,
                        'ends_till' => $value->ends_till,
                        'enable' => 1
                    ]);
                }
            }
        }
        return ListData($option);
    } else if ($option == 'gifts') {
        $data = access_cxi($option, $updateLastTime[0]->gift_last_update_time, null);
        if (!empty($data->data)) {
            foreach ($data->data as $key => $value) {
                if (productGift::findByGiftId($value->id) == false) {
                    $productSubmit = productGift::create([
                        'gift_id' => $value->id,
                        'gift' => $value->name,
                        'image' => $bss_options['CXI_URL'] . $value->image->original_image,
                        'description' => $value->description,
                        'enable' => 1
                    ]);
                }
            }
        }
        return ListData($option);
    } else if ($option == 'gift-categories') {
        $data = access_cxi($option, $updateLastTime[0]->gift_categories_time, null);
        if (!empty($data->data)) {
            foreach ($data->data as $key => $value) {
                if (productGiftcategories::findByGiftcategoriesId($value->id) == false) {
                    $productSubmit = productGiftcategories::create([
                        'gift_categories_id' => $value->id,
                        'gift_categories' => $value->name,
                        'image' => $bss_options['CXI_URL'] . $value->image->original_image,
                        'description' => $value->description,
                        'enable' => 1
                    ]);
                }
            }
        }
        return ListData($option);
    } else if ($option == 'stores') {
        $data = access_cxi($option, $updateLastTime[0]->store_detail_time);
        if (!empty($data->data)) {
            foreach ($data->data as $key => $value) {
                if (storeDetail::findByStoreId($value->id) == false) {
                    $value->brand = json_encode($value->brand);
                    $value->urls = json_encode($value->urls);
                    $storeSubmit = storeDetail::create([
                        'store_id' => $value->id,
                        'name' => $value->name,
                        'description' => $value->description,
                        'contact_name' => $value->contact_name,
                        'contact_number' => $value->contact_number,
                        'contact_fax' => $value->contact_fax,
                        'is_activated' => $value->is_activated,
                        'country' => $value->country,
                        'state' => $value->state,
                        'city' => $value->city,
                        'street' => $value->street,
                        'postcode' => $value->postcode,
                        'latitude' => $value->latitude,
                        'longtitude' => $value->longitude,
                        'brand' => $value->brand,
                        'urls' => $value->urls,
                        'enable' => 1
                    ]);
                }
            }
        }
        return ListData($option);
    }
}

?>  