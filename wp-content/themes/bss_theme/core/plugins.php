<?php
function bss_theme_plugin_activation(){
    $plugins = array(
        array(
            'name' => 'Redux Framework',
            'slug' => 'redux-framework',
            'required' => true
        ),
//        array(
//            'name'               => 'Advanced Custom Fields', // The plugin name.
//            'slug'               => 'advanced-custom-fields', // The plugin slug (typically the folder name).
//            'source'             => get_template_directory_uri() . '/assets/plugins/advanced-custom-fields-pro.zip', // The plugin source.
//            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
//            'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
////            'force_activation'   => true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
////            'force_deactivation' => true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
//            'external_url'       => '', // If set, overrides default API URL and points to an external URL.
//        )
    );

    $configs = array(
      'menu' => 'bss_plugin_install',
      'has_notice' => true,
      'dismissable' => false,
      'is_automatic' => true
    );
    tgmpa($plugins, $configs);
}
add_action('tgmpa_register','bss_theme_plugin_activation');

?>