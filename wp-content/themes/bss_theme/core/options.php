<?php
if ( ! class_exists( 'Bss_Theme_Options' ) ) {

    class Bss_Theme_Options {
        public $args = array();
        public $sections = array();
        public $theme;
        public $ReduxFramework;
        /* Load Redux Framework */
        public function __construct() {

            if ( ! class_exists( 'ReduxFramework' ) ) {
                return;
            }

            // This is needed. Bah WordPress bugs.  ;)
            if ( true == Redux_Helpers::isTheme( __FILE__ ) ) {
                $this->initSettings();
            } else {
                add_action( 'plugins_loaded', array( $this, 'initSettings' ), 10 );
            }

        }
        public function initSettings()
        {

            // Set the default arguments
            $this->setArguments();

            // Set a few help tabs so you can see how it's done
            $this->setHelpTabs();

            // Create the sections and fields
            $this->setSections();

            if (!isset($this->args['opt_name'])) { // No errors please
                return;
            }

            $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
        }
        public function setArguments()
        {
            $theme = wp_get_theme();
            $this->args = array(
                'opt_name' => 'bss_options',
                'display_name' => $theme->get('Name'),
                'menu_type' => 'menu',
                'allow_sub_menu' => true,
                'menu_title' => __('Bss Theme Options', 'bss_theme'),
                'page_title' => __('Bss Theme Options', 'bss_theme'),
                'dev_mode' => false,
                'customizer' => true,
                'menu_icon' => '',
                'google_api_key' => 'AIzaSyAs0iVWrG4E_1bG244-z4HRKJSkg7JVrVQ',
                'hints' => array(
                    'icon' => 'icon-question-sign',
                    'icon_position' => 'right',
                    'icon_color' => 'lightgray',
                    'icon_size' => 'normal',
                    'tip_style' => array(
                        'color' => 'light',
                        'shadow' => true,
                        'rounded' => false,
                        'style' => '',
                    ),
                    'tip_position' => array(
                        'my' => 'top left',
                        'at' => 'bottom right',
                    ),
                    'tip_effect' => array(
                        'show' => array(
                            'effect' => 'slide',
                            'duration' => '500',
                            'event' => 'mouseover',
                        ),
                        'hide' => array(
                            'effect' => 'slide',
                            'duration' => '500',
                            'event' => 'click mouseleave',
                        ),
                    ),
                )
            );
        }
        public function setHelpTabs() {

            // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
            $this->args['help_tabs'][] = array(
                'id'      => 'redux-help-tab-1',
                'title'   => __( 'Theme Information 1', 'bss_theme' ),
                'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'bss_theme' )
            );

            $this->args['help_tabs'][] = array(
                'id'      => 'redux-help-tab-2',
                'title'   => __( 'Theme Information 2', 'bss_theme' ),
                'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'bss_theme' )
            );

            // Set the help sidebar
            $this->args['help_sidebar'] = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'bss_theme' );
        }

        public function setSections() {

            // Header Section
            $this->sections[] = array(
                'title'  => 'Header',
                'desc'   => 'All of settings for header on this theme.',
                'icon'   => 'el-icon-home',
                'fields' => array(
                    array(
                        'id'       => 'logo-on',
                        'type'     => 'switch',
                        'title'    => 'Enable Image Logo',
                        'compiler' => 'bool',
                        'desc'     => 'Do you want to use image as a logo?',
                        'on' => 'Enabled',
                        'off' => 'Disabled'
                    ),

                    array(
                        'id'       => 'logo-image',
                        'type'     => 'media',
                        'title'    => 'Logo Image',
                        'desc'     => 'Image that you want to use as logo',
                    ),

                    array(
                        'id'       => 'logo-on-mobile',
                        'type'     => 'switch',
                        'title'    => 'Enable Image Logo Mobile',
                        'compiler' => 'bool',
                        'desc'     => 'Do you want to use image as a logo?',
                        'on' => 'Enabled',
                        'off' => 'Disabled'
                    ),

                    array(
                        'id'       => 'logo-image-mobile',
                        'type'     => 'media',
                        'title'    => 'Logo Image Mobile',
                        'desc'     => 'Image that you want to use as logo',
                    ),
                )
            ); // end section
            // Footer Section
            $this->sections[] = array(
                'title'  => 'Footer',
                'desc'   => 'All of settings for footer on this theme.',
                'icon'   => 'el-icon-home',
                'fields' => array(
                    array(
                        'id'       => 'logo-on-footer',
                        'type'     => 'switch',
                        'title'    => 'Enable Image Logo',
                        'compiler' => 'bool',
                        'desc'     => 'Do you want to use image as a logo?',
                        'on' => 'Enabled',
                        'off' => 'Disabled'
                    ),

                    array(
                        'id'       => 'logo-image-footer',
                        'type'     => 'media',
                        'title'    => 'Logo Image',
                        'desc'     => 'Image that you want to use as logo',
                    ),

                    array(
                        'id'       => 'logo-on-footer-mobile',
                        'type'     => 'switch',
                        'title'    => 'Enable Image Logo Mobile',
                        'compiler' => 'bool',
                        'desc'     => 'Do you want to use image as a logo?',
                        'on' => 'Enabled',
                        'off' => 'Disabled'
                    ),

                    array(
                        'id'       => 'copyright',
                        'type'     => 'editor',
                        'title'    => '© Copyright',
                        'desc'     => 'Image that you want to use as logo',
                    ),
                )
            ); // end section
            // Configuration Section
            $this->sections[] = array(
                'title'  => 'Configuration',
                'desc'   => 'All of settings for options page on this theme.',
                'icon'   => 'el-icon-wrench',
                'fields' => array(

                    array(
                        'id'       => 'API_CXI_URL',
                        'type'     => 'text',
                        'title'    => 'API CXI URL',
                    ),

                    array(
                        'id'       => 'CXI_URL',
                        'type'     => 'text',
                        'title'    => 'CXI URL',
                    ),

                    array(
                        'id'       => 'API_CXI_USER_NAME',
                        'type'     => 'text',
                        'title'    => 'API CXI USER NAME',
                    ),

                    array(
                        'id'       => 'API_CXI_PWD',
                        'type'     => 'password',
                        'title'    => 'API CXI PWD',
                    ),

                    array(
                        'id'       => 'API_CXI_TOKEN',
                        'type'     => 'text',
                        'title'    => 'API CXI TOKEN',
                    ),

                    array(
                        'id'       => 'API_CXI_X_APP_ID',
                        'type'     => 'text',
                        'title'    => 'API CXI X APP ID',
                    ),

                    array(
                        'id'       => 'BRAND_ID',
                        'type'     => 'text',
                        'title'    => 'BRAND ID',
                    ),

                    array(
                        'id'       => 'ORGANIZATION_ID',
                        'type'     => 'text',
                        'title'    => 'ORGANIZATION ID',
                    ),

                    array(
                        'id'       => 'TIME_ZONE',
                        'type'     => 'text',
                        'title'    => 'TIME ZONE',
                    ),
                )
            ); // end section
          // SYNC Section
          $this->sections[] = array(
            'title'  => 'Sync / Update',
            'desc'   => 'All of settings for sync.',
            'icon'   => 'el-icon-download-alt',
            'fields' => array(

              array (
                'id'            => 'sync-js-button',
                'type'          => 'js_button',
                'title'         => '<button type="button" class="btn btn-primary data-sync btn-primary" > <i class="el el-refresh"></i> SYNC</button>',
                'subtitle'      => 'Synchronize data from the beginning.'
              ),

            )
          ); // end section
        }
    }
    global $reduxConfig;
    $reduxConfig = new Bss_Theme_Options();
}