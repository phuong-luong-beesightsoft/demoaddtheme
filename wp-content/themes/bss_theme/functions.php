<?php
define( 'THEME_URL', get_stylesheet_directory() );
define( 'CORE', THEME_URL . '/core' );
require_once get_template_directory().'/class-wp-bootstrap-navwalker.php';
require_once( CORE . '/init.php' );
require get_template_directory() . '/inc/inc.php';

$language_folder = THEME_URL . '/languages';

load_theme_textdomain( 'bss_theme', $language_folder );

add_theme_support( 'automatic-feed-links' );

add_theme_support( 'post-formats',
    array(
        'image',
        'video',
        'gallery',
        'quote',
        'link'
    )
);

add_action('admin_head', 'my_custom_variable');
function my_custom_variable(){
    echo '<script> var _templateUrl = "'.get_template_directory_uri() .'" ;</script>';
    echo '<script> var _domainUrl = "'.get_site_url() .'" ;</script>';
}

function Bss_Theme_enqueue(){
    wp_enqueue_style('font-awesome-css', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style('font-awesome', "//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
    wp_enqueue_style('owlcarouselcss', get_template_directory_uri() . '/assets/css/owl.carousel.min.css');
    wp_enqueue_style('owlcarouseltheme', get_template_directory_uri() . '/assets/css/owl.theme.default.min.css');
    wp_enqueue_style('style', get_template_directory_uri().'/style.css');
    wp_enqueue_style('customcss', get_template_directory_uri() . '/assets/css/custom.css');
    wp_enqueue_script('jqueryjs', get_template_directory_uri().'/assets/js/jquery.min.js', array('jquery'),'',true);
    wp_enqueue_script('bootstrapjs', get_template_directory_uri().'/assets/js/bootstrap.min.js');
    wp_enqueue_script('owlcarouseljs', get_template_directory_uri().'/assets/js/owl.carousel.min.js');
    wp_enqueue_script('actionjs', get_template_directory_uri().'/assets/js/action.js');
    wp_enqueue_script('customjs', get_template_directory_uri().'/assets/js/custom.js');
}
add_action('wp_enqueue_scripts','Bss_Theme_enqueue');

add_theme_support('title-tag');
add_theme_support('post_thumbnails');
register_nav_menus(array('header' => 'Custom Primary Menu'));

add_action('admin_head', 'my_custom_media');

function my_custom_media()
{
    echo '<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" media="all" />';
    echo '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">';
    echo '<link rel="stylesheet" href="'.get_template_directory_uri().' /assets/css/custom-admin.css" type="text/css" media="all" />';
    echo '<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>';
    echo '<script src='.get_template_directory_uri().'/assets/js/jquery.min.js  crossorigin="anonymous"></script>';
    echo '<script src='.get_template_directory_uri().'/assets/js/custom.js crossorigin="anonymous"></script>';
}

add_action( 'init', 'add_excerpts_to_pages' );
function add_excerpts_to_pages() {
    add_post_type_support( 'page', 'excerpt' );
}

add_action( 'admin_init', 'mytheme_admin_init' );
function mytheme_admin_init() {
    if ( ! get_option( 'bss_theme_installed' ) ) {
        $new_page_id = wp_insert_post( array(
            'post_title'     => 'List Product',
            'post_type'      => 'page',
            'post_name'      => 'List Product',
            'post_status'    => 'publish',
            'post_author'    => get_user_by( 'id', 1 )->user_id,
            'menu_order'     => 0
        ) );

        if ( $new_page_id && ! is_wp_error( $new_page_id ) ){
            update_post_meta( $new_page_id, '_wp_page_template', 'list_product.php' );
        }

        $new_page_id = wp_insert_post( array(
            'post_title'     => 'Single Product',
            'post_type'      => 'page',
            'post_name'      => 'single Product',
            'post_status'    => 'publish',
            'post_author'    => get_user_by( 'id', 1 )->user_id,
            'menu_order'     => 0
        ) );

        if ( $new_page_id && ! is_wp_error( $new_page_id ) ){
            update_post_meta( $new_page_id, '_wp_page_template', 'single-product.php' );
        }

        update_option( 'bss_theme_installed', true );
    }
}

function start_session() {
  if(!session_id()) {
    session_start();
  }
}

add_action('init', 'start_session', 1);

function end_session(){
  if(session_id()){
    session_destroy();
  }
}

//add_action('init', 'end_session', 1);
?>
