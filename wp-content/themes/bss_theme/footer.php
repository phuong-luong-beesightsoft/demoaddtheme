<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-5 mobile-item">
                <div class="form-register-email">
                    <h3 class="title">Register Email to Receive Latest Promotions</h3>
                </div>
            </div>
            <div class="col-md-7">
                <ul>
                    <li>
                        <h3 class="title">Get Started</h3>
                        <div class="menu-list">
                            <div class="menu-first-footer-menu-container">
                                <ul id="menu-first-footer-menu" class="navbar-nav mr-auto">
                                    <li id="menu-item-723"
                                        class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-723 nav-item">
                                        <a href="http://www.yellowcabpizza.vn/en"
                                           class="nav-link d-flex align-items-center">Home</a></li>
                                    <li id="menu-item-724"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-724 nav-item">
                                        <a href="https://www.yellowcabpizza.vn/en/promotion/"
                                           class="nav-link d-flex align-items-center">Promotions</a></li>
                                    <li id="menu-item-728"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-728 nav-item">
                                        <a href="http://www.yellowcabpizza.vn/en/menu/pizza"
                                           class="nav-link d-flex align-items-center">Menu</a></li>
                                    <li id="menu-item-725"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-725 nav-item">
                                        <a href="https://www.yellowcabpizza.vn/en/stores/"
                                           class="nav-link d-flex align-items-center">Stores</a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li>
                        <h3 class="title">Talk to Us</h3>
                        <div class="menu-list">
                            <div class="menu-second-footer-menu-container">
                                <ul id="menu-second-footer-menu" class="navbar-nav mr-auto">
                                    <li id="menu-item-731"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-731 nav-item">
                                        <a href="https://www.yellowcabpizza.vn/en/contact-feedback/"
                                           class="nav-link d-flex align-items-center">Feedback</a></li>
                                    <li id="menu-item-729"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-729 nav-item">
                                        <a href="https://www.yellowcabpizza.vn/en/contact-career/"
                                           class="nav-link d-flex align-items-center">Careers</a></li>
                                    <li id="menu-item-730"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-730 nav-item">
                                        <a href="https://www.yellowcabpizza.vn/en/contact-location/"
                                           class="nav-link d-flex align-items-center">Location offer</a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="footer-social">
                        <h3 class="title">Connect To Us</h3>
                        <div class="desc">Follow us for the latest trends and updates about Yellow Cab Pizza Co.</div>
                        <p>
                            <a href="http://online.gov.vn/HomePage/CustomWebsiteDisplay.aspx?DocId=45501"
                               target="_blank" class="social-item">
                                <img src="https://www.yellowcabpizza.vn/wp-content/uploads/2019/03/20150827110756-dathongbao-e1553063945312.png"
                                     alt="">
                            </a>
                        </p>
                    </li>
                </ul>
                <div class="footer-privacy-policy">© 2017 Blue Star Pizza JSC, 424A Nguyen Thi Minh Khai , Ward 5
                    District 3 , HCMC <span class="desktop-item">All Rights Reserved.</span>
                    <p class="mobile-item">All Rights Reserved.</p>
                </div>
            </div>
            <div class="col-md-5 desktop-item">
                <div class="form-register-email">
                    <h3 class="title">Register Email to Receive Latest Promotions</h3>
                    <script>(function () {
                            if (!window.mc4wp) {
                                window.mc4wp = {
                                    listeners: [],
                                    forms: {
                                        on: function (event, callback) {
                                            window.mc4wp.listeners.push({
                                                event: event,
                                                callback: callback
                                            });
                                        }
                                    }
                                }
                            }
                        })();
                    </script><!-- MailChimp for WordPress v4.3.3 - https://wordpress.org/plugins/mailchimp-for-wp/ -->
                    <form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-1421" method="post" data-id="1421"
                          data-name="Register">
                        <div class="mc4wp-form-fields">
                            <div class="form-group">
                                <input type="text" name="FNAME" class="form-control" placeholder="Enter Your Name">
                            </div>
                            <div class="form-group">
                                <input type="email" name="EMAIL" class="form-control" placeholder="Enter Your Email">
                            </div>
                            <input value="SUBMIT" type="submit" id="emailRegister" class="w-100 btn btn-yellow">
                        </div>
                        <label style="display: none !important;">Leave this field empty if you're human: <input
                                    type="text" name="_mc4wp_honeypot" value="" tabindex="-1"
                                    autocomplete="off"></label><input type="hidden" name="_mc4wp_timestamp"
                                                                      value="1583379854"><input type="hidden"
                                                                                                name="_mc4wp_form_id"
                                                                                                value="1421"><input
                                type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-1">
                        <div class="mc4wp-response"></div>
                    </form><!-- / MailChimp for WordPress Plugin -->        </div>
            </div>
        </div>
    </div>
</footer>
<?php
View::render('modal/product', array(), true, false);
View::render('modal/pick-up', array(), true, false);
View::render('modal/delivery', array(), true, false);
?>

<?php wp_footer(); ?>
</body>
</html>