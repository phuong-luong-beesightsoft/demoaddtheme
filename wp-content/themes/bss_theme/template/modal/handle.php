<?php
function getTime(){
    global $bss_options;
    $tz = $bss_options['TIME_ZONE'];
    if ((int)$tz >= 0) {
        $tz = '+' . $tz;
    }
    $beginTime = new DateTime("now", new DateTimeZone($tz));
    $beginTime->setTime(8, 0, 0);
    $endTime = new DateTime("now", new DateTimeZone($tz));
    $endTime->setTime(22, 0, 0);

    $dt = new DateTime("now", new DateTimeZone($tz));
//$dt->add(new DateInterval('PT1H'));
    if ($beginTime > $dt) {
        $dt = $beginTime;
    }
    $times = array();
    while ($dt < $endTime) {

        $dt->setTime(
            $dt->format('H'),
            ceil($dt->format('i') / 15) * 15,
            0
        );
        $times[] = $dt->format('H:i');
        $dt->add(new DateInterval('PT15M'));
    }
    return $times;
}
?>