<div class="modal modal-normal fade" id="edit_address" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content setHeightContentTopBottom100">
            <div class="modal-header">
                <button type="button" class="close btn-close-modal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="icon-cancel-2 icon-close-modal"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="section section-add-address">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="add-address">
                                    <div class="block-title">
                                        EDIT ADDRESS
                                    </div>
                                    <form class="form-edit-address" method="POST">
                                        <div class="add-address-content">
                                            <div class="ul-custom">
                                                <div>
                                                    <div class="form-group">
                                                        <div class="alert alert-danger" role="alert" style="display: none;">
                                                            <strong>Error</strong>
                                                        </div>
                                                        <div class="alert alert-success" role="alert" style="display: none;">
                                                            <strong>Success</strong>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="form-group">
                                                        <label class="title-item">Name</label>
                                                        <input type="text" name="name_address" id="name_address"
                                                               class="form-control name-address-item" maxlength="50" value="" required>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="form-group">
                                                        <label class="title-item">Phone Number</label>
                                                        <input type="text" name="phone_address" id="phone_address"
                                                               class="form-control phone-address-item" maxlength="50" value="" required>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="form-group">
                                                        <label class="title-item">Building Name</label>
                                                        <input type="text" name="building_name_address" id="building_name_address"
                                                               class="form-control building-name-address" maxlength="50" value=""
                                                               placeholder="Republic Plaza"
                                                        >
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="form-group">
                                                        <label class="title-item">Address</label>
                                                        <input type="text" name="address" class="form-control address-address"
                                                               maxlength="50" placeholder="18E Cong Hoa p4 Quan Tan Binh Tp HCM"
                                                               value="" required>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="form-group">
                                                        <label class="title-item">Country</label>
                                                        <input type="text" name="country_address" class="form-control country-address"
                                                               maxlength="50" placeholder="Viet Nam" value="" required>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="form-group">
                                                        <label class="title-item">State</label>
                                                        <input type="text" name="state_address" class="form-control state_address"
                                                               maxlength="50" placeholder="Viet Nam" value="" required>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="form-group">
                                                        <label class="title-item">City</label>
                                                        <input type="text" name="city_address" class="form-control city_address"
                                                               maxlength="50" placeholder="Ho Chi Minh" value="" required>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="form-group">
                                                        <label class="title-item">Post Code</label>
                                                        <input type="text" name="postcode_address" class="form-control postcode_address"
                                                               maxlength="50" placeholder="70000" value="">
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="form-group">
                                                        <label class="title-item">Address Nickname</label>
                                                        <input type="text" name="address_nickname" class="form-control address_nickname"
                                                               maxlength="50" placeholder="NickName 01" value="" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <input type="hidden" name="latitude" >
                                                <input type="hidden" name="longitude" >
                                                <input type="hidden" name="id_address" >
                                                <button id="actionAddAddress" class="btn btn-yellow">Confirm</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>