<?php
require_once('handle.php');
$times = getTime();
?>
<div class="modal modal-normal modal-delivery-pickup fade" id="delivery" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content setHeightContentTopBottom100">
            <div class="modal-header">
                <button type="button" class="close btn-close-modal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="icon-cancel-2 icon-close-modal"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <h2 class="block-title"> DELIVERY</h2>
                </div>
                <div class="nav nav-tabs ul-custom-nav-tabs" id="tab_delivery" role="tablist">
                    <a class="nav-item nav-link active" id="without_account_tab" data-toggle="tab"
                       href="#without_account" role="tab" aria-controls="without_account" aria-selected="true">
                        Order without Account
                    </a>
                    <a class="nav-item nav-link" id="without_account_tab" data-toggle="tab"
                       href="#with_account" role="tab" aria-controls="with_account" aria-selected="true">
                        Order with Account
                    </a>
                </div>
                <div class="tab-content" id="tab_delivery_content">
                    <div class="tab-pane fade show active" id="without_account" role="tabpanel"
                         aria-labelledby="without_account_tab">
                        <form method="POST" class="user-form">
                            <div class="delivery-time">
                                <div class="delivery-time">Delivery Time</div>
                                <div class="delivery-selectpicker">
                                    <select class="selectpicker" id="delivery-time" name="time">
                                        <option value="now">Now</option>
                                        <?php
                                        foreach ($times as $time) {
                                            ?>
                                            <option value="<?php echo $time ?>"> <?= $time ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="delivery-info">
                                <div class="delivery-title">Delivery Information</div>
                                <div class="ul-custom">
                                    <div>
                                        <div class="form-group">
                                            <label class="title-item">Name</label>
                                            <input type="text" name="name" id="delivery-time"
                                                   class="form-control name-item" maxlength="50"
                                                   placeholder="Enter your name" value="currentName" required>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="form-group">
                                            <label class="title-item">Phone Number</label>
                                            <input type="text" name="phone" id="delivery-phone"
                                                   class="form-control phone-item" pattern="^[0-9]{8,14}$"
                                                   placeholder="Enter your phone number" value="0123456798">
                                        </div>
                                    </div>
                                    <div>
                                        <div class="form-group">
                                            <label class="title-item">City</label>
                                            <div class="delivery-selectpicker delivery-selectpicker-min">
                                                <select name="city" class="selectpicker" id="delivery-city" required>
                                                    <option value="City">Ho Chi Minh</option>
                                                    <option value="City">Ha Noi</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="form-group">
                                            <label class="title-item">District</label>
                                            <div class="delivery-selectpicker delivery-selectpicker-min">
                                                <select name="district" class="selectpicker" id="delivery-district"
                                                        required>
                                                    <option value="">District</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="form-group">
                                            <label class="title-item">Ward</label>
                                            <div class="delivery-selectpicker delivery-selectpicker-min">
                                                <select name="ward" class="selectpicker" id="delivery-ward" required>
                                                    <option value="">Ward</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="form-group">
                                            <label class="title-item">Address Detail</label>
                                            <input type="text" name="address" id="delivery-address"
                                                   class="form-control phone-item"
                                                   maxlength="100" placeholder="Enter your address" value="" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <input type="hidden" name="type" value="delivery">
                                <button id="confirmDelivery" class="btn btn-yellow">CONFIRM</button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade show" id="with_account" role="tabpanel"
                         aria-labelledby="with_account_tab">
                        <?php
                        if (!isset($_SESSION['profile']['access_token'])) {
                            ?>
                            <div class="h-100">
                                <div class="d-flex justify-content-center h-100">
                                    <div class="user_card">
                                        <div class="d-flex justify-content-center">
                                            <div class="brand_logo_container">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/img-logo.png"
                                                     class="brand_logo" alt="Logo">
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-center form_container">
                                            <form method="POST" class="login-form">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="fa fa-user"
                                                                                          aria-hidden="true"></i></span>
                                                    </div>
                                                    <input type="text" name="username"
                                                           class="form-control input_user" value=""
                                                           placeholder="username" required>
                                                </div>
                                                <div class="input-group mb-2">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="fa fa-key"
                                                                                          aria-hidden="true"></i></span>
                                                    </div>
                                                    <input type="password" name="pwd"
                                                           class="form-control input_pass" value=""
                                                           placeholder="password" required>
                                                </div>
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input"
                                                               id="customControlInline">
                                                        <label class="custom-control-label"
                                                               for="customControlInline">Remember me</label>
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-center mt-3 login_container">
                                                    <button type="submit" class="btn login_btn">Login</button>
                                                </div>
                                            </form>
                                        </div>

                                        <div class="mt-4">
                                            <div class="d-flex justify-content-center links">
                                                Don't have an account? <a href="#" class="ml-2">Sign Up</a>
                                            </div>
                                            <div class="d-flex justify-content-center links">
                                                <a href="#">Forgot your password?</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <form method="POST" class="user-form"
                            <?php
                            if (!isset($_SESSION['profile']['access_token'])) {

                                ?>
                                style="display: none;"
                                <?php
                            }
                            ?>
                        >
                            <div class="delivery-time">
                                <div class="delivery-time">Delivery Time</div>
                                <div class="delivery-selectpicker">
                                    <select class="selectpicker" id="delivery-time" name="time">
                                        <option value="now">Now</option>
                                        <?php
                                        foreach ($times as $time) {
                                            ?>
                                            <option value="<?php echo $time ?>"> <?= $time ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="delivery-info">
                                <div class="delivery-title">Delivery Information</div>
                                <div class="delivery-time">
                                    <div class="delivery-selectpicker">
                                        <select name="time" class="selectpicker" id="pickup-profile">
                                            <option value="now">
                                                <?php
                                                if (isset($_SESSION['profile']['name'])) {
                                                    echo 'Name : ' . $_SESSION['profile']['name'] . " ------ " . "Phone Number : " . $_SESSION['profile']['phone'];
                                                }
                                                ?>
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="delivery-time">
                                    <div class="delivery-selectpicker">
                                        <select name="address" class="selectpicker" id="delivery-address-without-account">
                                            <?php
                                            if (isset($_SESSION['profile']['address'])) {
                                                foreach ($_SESSION['profile']['address'] as $key => $address) {
                                                    ?>
                                                    <option value="<?= $address->id; ?>">
                                                        <?= $address->building_name; ?>
                                                        <?= $address->address1; ?>
                                                        <?= $address->state; ?>
                                                        <?= $address->postcode; ?>,
                                                        <?= $address->country; ?>
                                                    </option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <input type="hidden" name="is_guest" value="1" />
                                <input type="hidden" name="type" value="delivery" />
                                <input type="hidden" name="customer_id" value="<?= isset($_SESSION['profile']['id']) ? $_SESSION['profile']['id'] : null; ?>" />
                                <input type="hidden" name="name"
                                       value="<?php echo isset($_SESSION['profile']['name']) ? $_SESSION['profile']['name'] : ''; ?>">
                                <input type="hidden" name="phone"
                                       value="<?php echo isset($_SESSION['profile']['phone']) ? $_SESSION['profile']['phone'] : ''; ?>">
                                <button id="confirmDelivery" class="btn btn-yellow">CONFIRM</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>