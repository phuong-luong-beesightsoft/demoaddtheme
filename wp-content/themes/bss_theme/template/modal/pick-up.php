<?php
$google_api_key = 'AIzaSyDc86QPdj4CxdiYhoJgwjQbvvWbycgXbW4';
?>
<?php
require_once('handle.php');
?>
<div class="modal modal-normal modal-delivery-pickup fade" id="pickup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content setHeightContentTopBottom100">
            <div>
                <div class="modal-header">
                    <button type="button" class="close btn-close-modal" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="icon-cancel-2 icon-close-modal"></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <h2 class="block-title">Pick up</h2>
                    </div>
                    <div class="nav nav-tabs" id="tab_pickup" role="tablist">
                        <a class="nav-item nav-link active" data-toggle="tab" href="#without_account_pickup" role="tab"
                           aria-controls="without_account" aria-selected="true" id="order-without-account">
                            Order without Account
                        </a>
                        <a class="nav-item nav-link" data-toggle="tab" href="#with_account_pickup" role="tab"
                           aria-controls="with_account" aria-selected="true" id="order-with-account">
                            Order with Account
                        </a>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="without_account_pickup" role="tabpanel"
                             aria-labelledby="without_account-tab">
                            <form method="POST" class="user-form">
                                <div class="delivery-time">
                                    <div class="delivery-title">Delivery Time</div>
                                    <div class="delivery-selectpicker">
                                        <select name="time" class="selectpicker" id="pickup-time">
                                            <option value="now">Now</option>
                                            <?php
                                            $times = getTime();
                                            foreach ($times as $time) {
                                                ?>
                                                <option value="<?php echo $time ?>"> <?= $time ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="delivery-time">
                                    <div class="delivery-title">Select Store</div>
                                    <div class="delivery-selectpicker delivery-selectpicker-min">
                                        <select name="store" class="" id="pickup-store">
                                            <?php
                                            $stores = storeDetail::all();
                                            if (!empty($stores)) {
                                                foreach ($stores as $key => $store) {
                                                    if ($store->enable == 1) {
                                                        ?>
                                                        <option value="<?= $store->id; ?>"
                                                                data-lat="<?= $store->latitude; ?>"
                                                                data-lng="<?= $store->longtitude; ?>"><?= $store->name ?></option>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="map-stores">
                                        <div id="map-without-account"></div>
                                    </div>
                                </div>
                                <div class="delivery-info pickup-info">
                                    <div class="delivery-title">Information</div>
                                    <div class="ul-custom">
                                        <div>
                                            <div class="form-group">
                                                <label class="title-item"> Name</label>
                                                <input name="name" type="text" id="pickup-name"
                                                       class="form-control name-item" required max-length="50"
                                                       placeholder="Enter your name" value="Pickup Name">
                                            </div>
                                        </div>
                                        <div>
                                            <div class="form-group">
                                                <label class="title-item">Phone Number</label>
                                                <input name="phone" type="text" id="pickup-phone"
                                                       class="form-control phone-item" required
                                                       placeholder="Enter your phone number" value="0368497518"
                                                       pattern="^[0-9]{8,14}$">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <input type="hidden" name="type" value="pick-up">
                                    <input type="hidden" name="is_guest" value="1" />
                                    <button type="submit" class="btn btn-yellow btn-confirm-modal">
                                        CONFIRM
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade show" id="with_account_pickup" role="tab" aria-controls="with_account"
                             aria-selected="true">
                            <?php
                            if (!isset($_SESSION['profile']['access_token'])) {
                                ?>
                                <div class="h-100">
                                    <div class="d-flex justify-content-center h-100">
                                        <div class="user_card">
                                            <div class="d-flex justify-content-center">
                                                <div class="brand_logo_container">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/img-logo.png"
                                                         class="brand_logo" alt="Logo">
                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-center form_container">
                                                <form method="POST" class="login-form">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"><i class="fa fa-user"
                                                                                              aria-hidden="true"></i></span>
                                                        </div>
                                                        <input type="text" name="username"
                                                               class="form-control input_user" value=""
                                                               placeholder="username" required>
                                                    </div>
                                                    <div class="input-group mb-2">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"><i class="fa fa-key"
                                                                                              aria-hidden="true"></i></span>
                                                        </div>
                                                        <input type="password" name="pwd"
                                                               class="form-control input_pass" value=""
                                                               placeholder="password" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input"
                                                                   id="customControlInline">
                                                            <label class="custom-control-label"
                                                                   for="customControlInline">Remember me</label>
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-center mt-3 login_container">
                                                        <button type="submit" class="btn login_btn">Login</button>
                                                    </div>
                                                </form>
                                            </div>

                                            <div class="mt-4">
                                                <div class="d-flex justify-content-center links">
                                                    Don't have an account? <a
                                                            href="<?php echo get_site_url() . '/register'; ?>"
                                                            class="ml-2">Sign Up</a>
                                                </div>
                                                <div class="d-flex justify-content-center links">
                                                    <a href="#">Forgot your password?</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            <form method="POST" class="user-form"
                                <?php
                                if (!isset($_SESSION['profile']['access_token'])) {
                                    ?>
                                    style="display: none;"
                                    <?php
                                }
                                ?>
                            >
                                <div class="delivery-time">
                                    <div class="delivery-title">Infomation</div>
                                    <div class="delivery-selectpicker">
                                        <select name="time" class="selectpicker" id="pickup-profile">
                                            <option value="now">
                                                <?php
                                                if (isset($_SESSION['profile']['name'])) {
                                                    echo 'Name : ' . $_SESSION['profile']['name'] . " ------ " . "Phone Number : " . $_SESSION['profile']['phone'];
                                                }
                                                ?>
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="delivery-time">
                                    <div class="delivery-title">Delivery Time</div>
                                    <div class="delivery-selectpicker">
                                        <select name="time" class="selectpicker" id="pickup-time">
                                            <option value="now">Now</option>
                                            <?php
                                            foreach ($times as $time) {
                                                ?>
                                                <option value="<?php echo $time ?>"> <?= $time ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="delivery-time">
                                    <div class="delivery-title">Select Store</div>
                                    <div class="delivery-selectpicker delivery-selectpicker-min">
                                        <select name="store" class="" id="pickup-store">
                                            <?php
                                            $stores = storeDetail::all();
                                            if (!empty($stores)) {
                                                foreach ($stores as $key => $store) {
                                                    if ($store->enable == 1) {
                                                        ?>
                                                        <option value="<?= $store->id ?>"
                                                                data-lat="<?= $store->latitude; ?>"
                                                                data-lng="<?= $store->longtitude; ?>"><?= $store->name ?></option>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="map-stores">
                                        <div id="map-with-account"></div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <input type="hidden" name="type" value="pick-up">
                                    <input type="hidden" name="is_guest" value="0" />
                                    <input type="hidden" name="customer_id" value="<?= isset($_SESSION['profile']['id']) ? $_SESSION['profile']['id'] : null; ?>" />
                                    <input type="hidden" name="name"
                                           value="<?php echo isset($_SESSION['profile']['name']) ? $_SESSION['profile']['name'] : ''; ?>" />
                                    <input type="hidden" name="phone"
                                           value="<?php echo isset($_SESSION['profile']['phone']) ? $_SESSION['profile']['phone'] : ''; ?>" />
                                    <button
                                            class="btn btn-yellow btn-confirm-modal">CONFIRM
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal modal-normal modal-select-order-type fade" id="select-delivery-type" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content setHeightContentTopBottom100">
            <div class="modal-header">
                <button type="button" class="close btn-close-modal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="icon-cancel-2 icon-close-modal"></span>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="title text-center">SELECT AN ORDER TYPE</h2>
                <div class="order-type-list">
                    <div class="order-type-item text-center check-modal" data-dismiss="modal" data-toggle="modal"
                         data-target="#delivery">
                        <span class="icon-delivery"></span>
                        <div class="order-type-name">DELIVERY</div>
                        <div class="order-type-desc">Have your order delivered directly to you</div>
                    </div>
                    <div class="order-type-item text-center check-modal" data-dismiss="modal" data-toggle="modal"
                         data-target="#pickup">
                        <span class="icon-pickup"></span>
                        <div class="order-type-name">PICKUP</div>
                        <div class="order-type-desc">Pick up your order at a Yellow Cab Pizza's store</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    locations = [
        <?php
        foreach($stores as $store){
        $address = $store->street;
        $location['lat'] = $store->latitude;
        $location['lng'] = $store->longtitude;
        ?>
        {
            lat: <?php echo $location['lat'];  ?>,
            lng: <?php echo $location['lng']; ?>,
            name: "<?php echo $address;?>"
        },
        <?php
        }
        ?>
    ];
</script>
<script src="<?php echo 'https://maps.googleapis.com/maps/api/js?key=' . $google_api_key . '&callback=myMap'; ?>"></script>