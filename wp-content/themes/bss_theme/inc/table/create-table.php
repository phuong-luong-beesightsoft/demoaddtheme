<?php
global $wpdb;
$charset_collate = $wpdb->get_charset_collate();

$sql = "CREATE TABLE `{$wpdb->base_prefix}bss_product` (
        id bigint(20) AUTO_INCREMENT,
        product_id bigint(20),
        name varchar(255),
        image varchar(255),
        description varchar(255),
        price varchar(255),
        enable int(1),
        categories_id int(11),
        PRIMARY KEY (id)
    ) $charset_collate;";

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
dbDelta($sql);

global $wpdb;
$charset_collate = $wpdb->get_charset_collate();

$sql = "CREATE TABLE `{$wpdb->base_prefix}bss_product_categories` (
        id bigint(20) AUTO_INCREMENT,
        categories_id bigint(20),
        categories varchar(255),
        image varchar(255),
        description varchar(255),
        parent_id int(11),
        children longtext,
        enable int(1),
        PRIMARY KEY (id)
    ) $charset_collate;";

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
dbDelta($sql);

$sql = "CREATE TABLE `{$wpdb->base_prefix}bss_product_attribute` (
        id bigint(20) AUTO_INCREMENT,
        attribute_id bigint(20),
        attribute varchar (255),
        type varchar (255),
        options longtext,
        required int(1),
        unique_attribute int(1),
        enable int(1),
        PRIMARY KEY (id)
    ) $charset_collate;";

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
dbDelta($sql);

$sql = "CREATE TABLE `{$wpdb->base_prefix}bss_product_add_on` (
        id bigint(20) AUTO_INCREMENT,
        product_add_on_id bigint(20),
        product_add_on varchar(255),
        image varchar(255),
        description varchar(255),
        enable int(1),
        PRIMARY KEY (id)
    ) $charset_collate;";

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
dbDelta($sql);

$sql = "CREATE TABLE `{$wpdb->base_prefix}bss_product_add_on_type` (
        id bigint(20) AUTO_INCREMENT,
        product_add_on_type_id bigint(20),
        product_add_on_type varchar(255),
        description varchar(255),
        enable int(1),
        PRIMARY KEY (id)
    ) $charset_collate;";

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
dbDelta($sql);

$sql = "CREATE TABLE `{$wpdb->base_prefix}bss_product_promotion` (
        id bigint(20) AUTO_INCREMENT,
        product_promotion_id bigint(20),
        product_promotion varchar(255),
        image varchar (255),
        description varchar (255),
        starts_from varchar (255),
        ends_till varchar (255),
        enable int(1),
        PRIMARY KEY (id)
    ) $charset_collate;";

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
dbDelta($sql);

$sql = "CREATE TABLE `{$wpdb->base_prefix}bss_product_gift` (
        id bigint(20) AUTO_INCREMENT,
        gift_id bigint(20),
        gift varchar(255),
        image varchar(255),
        description varchar(255),
        enable int(1),
        PRIMARY KEY (id)
    ) $charset_collate;";

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
dbDelta($sql);

$sql = "CREATE TABLE `{$wpdb->base_prefix}bss_product_gift_categories` (
        id bigint(20) AUTO_INCREMENT,
        gift_categories_id bigint(20),
        gift_categories varchar(255),
        image varchar(255),
        description varchar(255),
        enable int(1),
        PRIMARY KEY (id)
    ) $charset_collate;";

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
dbDelta($sql);

$sql = "CREATE TABLE `{$wpdb->base_prefix}bss_product_last_update_time` (
        id bigint(20) AUTO_INCREMENT,
        product_time varchar (255),
        categories_time varchar(255),
        attribute_time varchar (255),
        product_addon_time varchar (255),
        product_addon_type_time varchar (255),
        promotion_time varchar (255),
        gift_last_update_time varchar (255),
        gift_categories_time varchar (255),
        store_detail_time varchar (255),
        PRIMARY KEY (id)
    ) $charset_collate;";

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
dbDelta($sql);

$sql = "CREATE TABLE `{$wpdb->base_prefix}bss_product_detail` (
        id bigint(20) AUTO_INCREMENT,
        product_id int(11),
        type varchar (255),
        name varchar (255),
        status varchar (255),
        price int(11),
        price_type varchar (255),
        formated_prices varchar (255),
        description longtext,
        sku varchar (255),
        categories longtext,
        image varchar (255),
        variants longtext,
        super_attributes longtext,
        combo longtext,
        categories_id int(11),
        updated_at varchar (255),
        PRIMARY KEY (id)
    ) $charset_collate;";

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
dbDelta($sql);

$sql = "CREATE TABLE `{$wpdb->base_prefix}bss_store_detail` (
        id bigint(20) AUTO_INCREMENT,
        store_id int(11),
        name varchar (255),
        description varchar (255),
        contact_name varchar (255),
        contact_email varchar (255),
        contact_number varchar (255),
        contact_fax varchar (255),
        is_activated varchar (255),
        country varchar (255),
        state varchar (255),
        city varchar (255),
        street varchar (255),
        postcode varchar (255),
        latitude varchar (255),
        longtitude varchar (255),
        brand longtext,
        urls longtext,
        enable int(1),
        PRIMARY KEY (id)
        ) $charset_collate;";

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
dbDelta($sql);
?>