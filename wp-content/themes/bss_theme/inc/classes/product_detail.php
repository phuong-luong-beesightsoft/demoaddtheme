<?php
// make sure to include the abstract class first before extending the Model Class
class productDetail extends Model{
  static $table_name = 'bss_product_detail';

  public static function findByProductDetailId($product_id)
  {
    global $wpdb;

    $sql = "SELECT * FROM " . self::table() . " WHERE product_id = '" . $product_id . "'";

    return $wpdb->get_row($sql);
  }

  public static function findProductDetailByCategoriesId($categories_id)
  {
    global $wpdb;

    $sql = "SELECT * FROM " . self::table() . " WHERE categories_id = '" . $categories_id . "'";

    return $wpdb->get_row($sql);
  }
}
?>