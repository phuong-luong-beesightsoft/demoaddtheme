<?php
// make sure to include the abstract class first before extending the Model Class
class productGiftcategories extends Model{
    static $table_name = 'bss_product_gift_categories';

    public static function findByGiftcategoriesId($product_id)
    {
        global $wpdb;

        $sql = "SELECT * FROM " . self::table() . " WHERE gift_categories_id = '" . $product_id . "'";

        return $wpdb->get_row($sql);
    }
}
?>