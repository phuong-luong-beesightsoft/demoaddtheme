<?php
// make sure to include the abstract class first before extending the Model Class
class productCategories extends Model{
    static $table_name = 'bss_product_categories';

    public static function findByCategoriesId($product_id)
    {
        global $wpdb;

        $sql = "SELECT * FROM " . self::table() . " WHERE categories_id = '" . $product_id . "'";

        return $wpdb->get_row($sql);
    }
}
?>