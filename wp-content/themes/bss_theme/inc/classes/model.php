<?php

abstract class Model
{
  static $table_name = '';

  public static function table()
  {
    global $wpdb;

    return $wpdb->prefix . static::$table_name;
  }

  public static function all($conditions = '')
  {
    if (!empty(self::table())) {
      global $wpdb;
      $sql = "SELECT * FROM " . self::table() . $conditions;
      return $wpdb->get_results($sql);
    }

    return false;
  }

  public function row($conditions = '')
  {

    if (!empty(self::table())) {
      global $wpdb;

      $sql = "SELECT * FROM " . self::table() . $conditions;

      return $wpdb->get_row($sql);
    }

    return false;
  }

  public static function findById($id)
  {
    global $wpdb;

    $sql = "SELECT * FROM " . self::table() . " WHERE id = '" . $id . "'";

    return $wpdb->get_row($sql);
  }

  public static function findByTitle($title)
  {
    global $wpdb;

    $sql = "SELECT * FROM " . self::table() . " WHERE name = '" . $title . "'";

    return $wpdb->get_row($sql);
  }

  public static function find($type, $parameters)
  {
    if (empty($type)) {
      $type = 'all';
    }

    $conditions = isset($parameters['conditions']) ? (' WHERE ' . join(' AND ', $parameters['conditions'])) : null;

    if (isset($parameters['orderBy'])) {
      $conditions .= ' ORDER BY ' . $parameters['orderBy'];

      if (isset($parameters['order'])) {
        $conditions .= ' ' . $parameters['order'];
      }
    }

    if (isset($parameters['limit'])) {
      $conditions .= ' LIMIT ' . (int)$parameters['limit'];
    }

    if (isset($parameters['offset'])) {
      $conditions .= ' OFFSET ' . (int)$parameters['offset'];
    }

    if ($type = 'all') {
      return self::all($conditions);
    } else if ($type == 'row') {
      return self::row($conditions);
    }

    return false;

  }

  public static function count($parameters)
  {
    if (!empty(self::table())) {
      global $wpdb;

      $conditions = isset($parameters['conditions']) ? (' WHERE ' . join(' AND ', $parameters['conditions'])) : null;

      $count = $wpdb->get_var("SELECT COUNT(*) FROM " . self::table() . conditions);

      return $count;
    }
    return 0;

  }

  public static function countall($conditions = '')
  {
    if (!empty(self::table())) {
      global $wpdb;

      $sql = "SELECT count(*) as total FROM " . self::table() . $conditions;

      return $wpdb->get_row($sql);
    }

    return false;
  }

  public static function create($data)
  {
    global $wpdb;

    if ($wpdb->insert(self::table(), $data)) {
      $id = $wpdb->insert_id;

      $data = self::findById($id);

      return $data;
    }

    return false;
  }

  public static function update($data, $where, $format = null, $where_format = null)
  {
    global $wpdb;

    return $wpdb->update(self::table(), $data, $where, $format, $where_format);
  }

  public static function delete($data)
  {
    global $wpdb;

    return $wpdb->delete(self::table(), $data);
  }

  public static function droptable()
  {
    global $wpdb;

    $sql = "DROP TABLE IF EXISTS " . self::table();

    return $wpdb->query($sql);
  }
}

?>