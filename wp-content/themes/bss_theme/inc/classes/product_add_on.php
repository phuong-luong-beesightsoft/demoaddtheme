<?php
// make sure to include the abstract class first before extending the Model Class
class productAddonDetail extends Model{
    static $table_name = 'bss_product_add_on';

    public static function findByProductAddonId($product_id)
    {
        global $wpdb;

        $sql = "SELECT * FROM " . self::table() . " WHERE product_add_on_id = '" . $product_id . "'";

        return $wpdb->get_row($sql);
    }
}
?>