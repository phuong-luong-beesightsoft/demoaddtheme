<?php
// make sure to include the abstract class first before extending the Model Class
class productAddonTypeDetail extends Model{
    static $table_name = 'bss_product_add_on_type';

    public static function findByProductAddonTypeId($product_id)
    {
        global $wpdb;

        $sql = "SELECT * FROM " . self::table() . " WHERE product_add_on_type_id = '" . $product_id . "'";

        return $wpdb->get_row($sql);
    }
}
?>