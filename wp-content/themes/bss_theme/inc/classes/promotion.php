<?php
// make sure to include the abstract class first before extending the Model Class
class productPromotionDetail extends Model{
    static $table_name = 'bss_product_promotion';

    public static function findByProductPromotionId($product_id)
    {
        global $wpdb;

        $sql = "SELECT * FROM " . self::table() . " WHERE product_promotion_id = '" . $product_id . "'";

        return $wpdb->get_row($sql);
    }
}
?>