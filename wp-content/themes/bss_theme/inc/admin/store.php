<?php
function my_admin_store_menu()
{
  add_menu_page(
    'Stores',
    'Stores',
    'manage_options',
    'store-page',
    'store_admin_page_contents');
}

add_action('admin_menu', 'my_admin_store_menu');

function store_admin_page_contents()
{
  $option = 'stores';
  $fetch_data = get_data_from_db($option);
  $list_data = $fetch_data['list_data'];
  $total = $fetch_data['total'];
  $limit = $fetch_data['limit'];
  $page = $fetch_data['page'];
  ?>
    <h1>Stores</h1>
    <div class="wrap">
        <div class="row">
            <div class="col-md-3 col-sm-4 offset-md-9 offset-sm-8">
                <form>
                    <input type="hidden" name="page" value="product-page"/>
                    <div class="text-right">
                        <div class="tablenav-pages">
                            <span class="displaying-num"><?php _e($total) ?> items</span>
                            <span class="pagination-links">
                <a class="first-page" href="#"
                   onclick="jQuery('#current-page-selector').val(1); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">«</span>
                </a>
                <a class="previous-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e($page - 1) ?>); jQuery('form').submit();">
                <span class="tablenav-pages-navspan" aria-hidden="true">‹</span>
                </a>
                <span class="paging-input">
                  <label for="current-page-selector" class="screen-reader-text">Current Page</label>
                  <input class="current-page" id="current-page-selector" type="text" name="paged"
                         value="<?php echo $page ?>" size="1" aria-describedby="table-paging">
                  <span class="tablenav-paging-text"> of <span class="total-pages"><?php _e(ceil($total / $limit)) ?>
                      </span></span>
                </span>
                <a class="next-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e($page + 1) ?>); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">›</span>
                </a>
                <a class="last-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e(ceil($total / $limit)) ?>); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">»</span>
                </a>
              </span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">NAME</th>
                        <th scope="col">DESCRIPTION</th>
                        <th scope="col">CONTACT NAME</th>
                        <th scope="col">CONTACT NUMBER</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody class="update-product">
                    <?php
                    foreach ($list_data as $key => $store) {
                      ?>
                        <tr>
                            <td>
                                <span style="padding-left: 20px;"><?= $store->name; ?></span>
                            </td>
                            <td>
                              <?= $store->description; ?>
                            </td>
                            <td>
                              <?= $store->contact_name; ?>
                            </td>
                            <td>
                              <?= $store->contact_number; ?>
                            </td>
                          <td>
                            <?php
                            if ($store->enable == 1) {
                              ?>
                              <label class="switch">
                                <input type="checkbox" class="input-check-enable" checked
                                       data-id="<?= $store->id; ?>" data-table="storeDetail">
                                <span class="slider round"></span>
                              </label>
                              <?php
                            } else {
                              ?>
                              <label class="switch">
                                <input type="checkbox" class="input-check-enable" data-id="<?= $store->id; ?>"
                                       data-table="storeDetail">
                                <span class="slider round"></span>
                              </label>
                              <?php
                            }
                            ?>
                          </td>
                        </tr>
                      <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
  <?php
}

?>