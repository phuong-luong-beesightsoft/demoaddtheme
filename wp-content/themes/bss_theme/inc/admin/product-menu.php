<?php

function my_admin_product_menu()
{
    add_menu_page(
        'Products',
        'Products',
        'manage_options',
        'product-page',
        'product_admin_page_contents',
        'dashicons-schedule',
        14
    );

    add_submenu_page(
        'product-page',
        'Categories',
        'Categories',
        'manage_options',
        'product_categories_admin_page_contents',
        'product_categories_admin_page_contents');

    add_submenu_page(
        'product-page',
        'Attribute',
        'Attribute',
        'manage_options',
        'product_attribute_admin_page_contents',
        'product_attribute_admin_page_contents');

    add_submenu_page(
        'product-page',
        'Product add-on',
        'Product add-on',
        'manage_options',
        'product_add_on_admin_page_contents',
        'product_add_on_admin_page_contents');

    add_submenu_page(
        'product-page',
        'Product add-on type',
        'Product add-on type',
        'manage_options',
        'product_add_on_type_admin_page_contents',
        'product_add_on_type_admin_page_contents');

    add_submenu_page(
        'product-page',
        'Promotion',
        'Promotion',
        'manage_options',
        'product_promotion_admin_page_contents',
        'product_promotion_admin_page_contents');

    add_submenu_page(
        'product-page',
        'Gift',
        'Gift',
        'manage_options',
        'product_gift_admin_page_contents',
        'product_gift_admin_page_contents');

    add_submenu_page(
        'product-page',
        ' Gift-categories',
        'Gift-categories',
        'manage_options',
        'product_gift_categories_admin_page_contents',
        'product_gift_categories_admin_page_contents');

}

add_action('admin_menu', 'my_admin_product_menu');

function product_admin_page_contents()
{
    global $bss_options;
    $option = 'products';
    $fetch_data = get_data_from_db($option);
    $list_data = $fetch_data['list_data'];
    $total = $fetch_data['total'];
    $limit = $fetch_data['limit'];
    $page = $fetch_data['page'];
    ?>
    <h1>Products</h1>
    <div class="wrap">
        <div class="row">
            <div class="col-md-3 col-sm-4 offset-md-9 offset-sm-8">
                <form>
                    <input type="hidden" name="page" value="product-page"/>
                    <div class="text-right">
                        <div class="tablenav-pages">
                            <span class="displaying-num"><?php _e($total) ?> items</span>
                            <span class="pagination-links">
                <a class="first-page" href="#"
                   onclick="jQuery('#current-page-selector').val(1); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">«</span>
                </a>
                <a class="previous-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e($page - 1) ?>); jQuery('form').submit();">
                <span class="tablenav-pages-navspan" aria-hidden="true">‹</span>
                </a>
                <span class="paging-input">
                  <label for="current-page-selector" class="screen-reader-text">Current Page</label>
                  <input class="current-page" id="current-page-selector" type="text" name="paged"
                         value="<?php echo $page ?>" size="1" aria-describedby="table-paging">
                  <span class="tablenav-paging-text"> of <span class="total-pages"><?php _e(ceil($total / $limit)) ?>
                      </span></span>
                </span>
                <a class="next-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e($page + 1) ?>); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">›</span>
                </a>
                <a class="last-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e(ceil($total / $limit)) ?>); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">»</span>
                </a>
              </span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">NAME</th>
                        <th scope="col">DESCRIPTION</th>
                        <th scope="col">PRICE</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody class="update-product">
                    <?php
                    foreach ($list_data as $key => $value) {
                        ?>
                        <tr>
                            <td>
                                <img src="<?= $value->image; ?>" style="height: 42px; width: 42px;">
                                <span style="padding-left: 20px;"><?php echo $value->name; ?></span>
                            </td>
                            <td>
                                <?php
                                echo $value->description;
                                ?>
                            </td>
                            <td>
                                <?php
                                $price = number_format($value->price, 0, ',', '.');
                                echo $price . " VND";
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($value->enable == 1) {
                                    ?>
                                    <label class="switch">
                                        <input type="checkbox" class="input-check-enable" checked
                                               data-id="<?= $value->id; ?>" data-table="productList">
                                        <span class="slider round"></span>
                                    </label>
                                    <?php
                                } else {
                                    ?>
                                    <label class="switch">
                                        <input type="checkbox" class="input-check-enable" data-id="<?= $value->id; ?>"
                                               data-table="productList">
                                        <span class="slider round"></span>
                                    </label>
                                    <?php
                                }
                                ?>

                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <?php
}

function product_categories_admin_page_contents()
{
    global $bss_options;
    $option = 'categories';
    $fetch_data = get_data_from_db($option);
    $list_data = $fetch_data['list_data'];
    $total = $fetch_data['total'];
    $limit = $fetch_data['limit'];
    $page = $fetch_data['page'];
    ?>
    <h1>Categories</h1>
    <div class="wrap">
        <div class="row">
            <div class="col-md-3 col-sm-4 offset-md-9 offset-sm-8">
                <form>
                    <input type="hidden" name="page" value="product_categories_admin_page_contents"/>
                    <div class="text-right">
                        <div class="tablenav-pages">
                            <span class="displaying-num"><?php _e($total) ?> items</span>
                            <span class="pagination-links">
                <a class="first-page" href="#"
                   onclick="jQuery('#current-page-selector').val(1); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">«</span>
                </a>
                <a class="previous-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e($page - 1) ?>); jQuery('form').submit();">
                <span class="tablenav-pages-navspan" aria-hidden="true">‹</span>
                </a>
                <span class="paging-input">
                  <label for="current-page-selector" class="screen-reader-text">Current Page</label>
                  <input class="current-page" id="current-page-selector" type="text" name="paged"
                         value="<?php echo $page ?>" size="1" aria-describedby="table-paging">
                  <span class="tablenav-paging-text"> of <span class="total-pages"><?php _e(ceil($total / $limit)) ?>
                      </span></span>
                </span>
                <a class="next-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e($page + 1) ?>); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">›</span>
                </a>
                <a class="last-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e(ceil($total / $limit)) ?>); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">»</span>
                </a>
              </span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">NAME</th>
                        <th scope="col">DESCRIPTION</th>
                        <th scope="col">PARENT ID</th>
                        <th scope="col">CHILDREN</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody class="update-product">
                    <?php
                    foreach ($list_data as $key => $value) {
                        ?>
                        <tr>
                            <td>
                                <img src="<?= $value->image; ?>" style="height: 42px; width: 42px;">
                                <span style="padding-left: 20px;"><?php echo $value->categories; ?></span>
                            </td>
                            <td>
                                <?php
                                echo $value->description;
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $value->parent_id;
                                ?>
                            </td>
                            <td>
                                <ul>
                                    <?php
                                    $value->children = json_decode($value->children);
                                    foreach ($value->children as $children) {

                                        ?>
                                        <li style="    font-size: 1rem;
                                                        line-height: 1.42857143rem;
                                                        font-weight: 400;
                                                        border: .07142857rem solid #cfd1d6;
                                                        padding: .14285714rem .57142857rem;
                                                        display: inline-block;
                                                        border-radius: .85714286rem;
                                                        flex: 0 0 50%;">
                                            <?php echo $children->name; ?>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </td>
                            <td>
                                <?php
                                if ($value->enable == 1) {
                                    ?>
                                    <label class="switch">
                                        <input type="checkbox" class="input-check-enable" checked
                                               data-id="<?= $value->id; ?>" data-table="productCategories">
                                        <span class="slider round"></span>
                                    </label>
                                    <?php
                                } else {
                                    ?>
                                    <label class="switch">
                                        <input type="checkbox" class="input-check-enable" data-id="<?= $value->id; ?>"
                                               data-table="productCategories">
                                        <span class="slider round"></span>
                                    </label>
                                    <?php
                                }
                                ?>

                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php
}

function product_attribute_admin_page_contents()
{
    global $bss_options;
    $option = 'attributes';
    $fetch_data = get_data_from_db($option);
    $list_data = $fetch_data['list_data'];
    $total = $fetch_data['total'];
    $limit = $fetch_data['limit'];
    $page = $fetch_data['page'];
    ?>
    <h1>Attribute</h1>
    <div class="wrap">
        <div class="row">
            <div class="col-md-3 col-sm-4 offset-md-9 offset-sm-8">
                <form>
                    <input type="hidden" name="page" value="product_attribute_admin_page_contents"/>
                    <div class="text-right">
                        <div class="tablenav-pages">
                            <span class="displaying-num"><?php _e($total) ?> items</span>
                            <span class="pagination-links">
                <a class="first-page" href="#"
                   onclick="jQuery('#current-page-selector').val(1); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">«</span>
                </a>
                <a class="previous-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e($page - 1) ?>); jQuery('form').submit();">
                <span class="tablenav-pages-navspan" aria-hidden="true">‹</span>
                </a>
                <span class="paging-input">
                  <label for="current-page-selector" class="screen-reader-text">Current Page</label>
                  <input class="current-page" id="current-page-selector" type="text" name="paged"
                         value="<?php echo $page ?>" size="1" aria-describedby="table-paging">
                  <span class="tablenav-paging-text"> of <span class="total-pages"><?php _e(ceil($total / $limit)) ?>
                      </span></span>
                </span>
                <a class="next-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e($page + 1) ?>); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">›</span>
                </a>
                <a class="last-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e(ceil($total / $limit)) ?>); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">»</span>
                </a>
              </span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">NAME</th>
                        <th scope="col">TYPE</th>
                        <th scope="col">OPTIONS</th>
                        <th scope="col">REQUIRED</th>
                        <th scope="col">UNIQUE</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody class="update-product">
                    <?php
                    foreach ($list_data as $key => $value) {
                        ?>
                        <tr>
                            <td>
                                <span><?php echo $value->attribute; ?></span>
                            </td>
                            <td>
                                <?php
                                echo $value->type;
                                ?>
                            </td>
                            <td>
                                <ul>
                                    <?php
                                    $value->options = json_decode($value->options);
                                    foreach ($value->options as $option) {

                                        ?>
                                        <li style="    font-size: 1rem;
                                                        line-height: 1.42857143rem;
                                                        font-weight: 400;
                                                        border: .07142857rem solid #cfd1d6;
                                                        padding: .14285714rem .57142857rem;
                                                        display: inline-block;
                                                        border-radius: .85714286rem;
                                                        flex: 0 0 50%;">
                                            <?php echo $option->admin_name; ?>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </td>
                            <td>
                                <?php
                                if ($value->required == 0) {
                                    echo "False";
                                } else echo "True";
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($value->unique_attribute == 0) {
                                    echo "False";
                                } else echo "True";
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($value->enable == 1) {
                                    ?>
                                    <label class="switch">
                                        <input type="checkbox" class="input-check-enable" checked
                                               data-id="<?= $value->id; ?>" data-table="productAttribute">
                                        <span class="slider round"></span>
                                    </label>
                                    <?php
                                } else {
                                    ?>
                                    <label class="switch">
                                        <input type="checkbox" class="input-check-enable" data-id="<?= $value->id; ?>"
                                               data-table="productAttribute">
                                        <span class="slider round"></span>
                                    </label>
                                    <?php
                                }
                                ?>

                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php
}

function product_add_on_admin_page_contents()
{
    global $bss_options;
    $option = 'addons';
    $fetch_data = get_data_from_db($option);
    $list_data = $fetch_data['list_data'];
    $total = $fetch_data['total'];
    $limit = $fetch_data['limit'];
    $page = $fetch_data['page'];
    ?>
    <h1>Product add-on</h1>
    <div class="wrap">
        <div class="row">
            <div class="col-md-3 col-sm-4 offset-md-9 offset-sm-8">
                <form>
                    <input type="hidden" name="page" value="product_add_on_admin_page_contents"/>
                    <div class="text-right">
                        <div class="tablenav-pages">
                            <span class="displaying-num"><?php _e($total) ?> items</span>
                            <span class="pagination-links">
                <a class="first-page" href="#"
                   onclick="jQuery('#current-page-selector').val(1); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">«</span>
                </a>
                <a class="previous-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e($page - 1) ?>); jQuery('form').submit();">
                <span class="tablenav-pages-navspan" aria-hidden="true">‹</span>
                </a>
                <span class="paging-input">
                  <label for="current-page-selector" class="screen-reader-text">Current Page</label>
                  <input class="current-page" id="current-page-selector" type="text" name="paged"
                         value="<?php echo $page ?>" size="1" aria-describedby="table-paging">
                  <span class="tablenav-paging-text"> of <span class="total-pages"><?php _e(ceil($total / $limit)) ?>
                      </span></span>
                </span>
                <a class="next-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e($page + 1) ?>); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">›</span>
                </a>
                <a class="last-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e(ceil($total / $limit)) ?>); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">»</span>
                </a>
              </span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">NAME</th>
                        <th scope="col">DESCRIPTION</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody class="update-product">
                    <?php
                    foreach ($list_data as $key => $value) {
                        ?>
                        <tr>
                            <td>
                                <img src="<?= $value->image; ?>" style="height: 42px; width: 42px;">
                                <span style="padding-left: 20px;"><?php echo $value->product_add_on; ?></span>
                            </td>
                            <td>
                                <?php
                                echo $value->description;
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($value->enable == 1) {
                                    ?>
                                    <label class="switch">
                                        <input type="checkbox" class="input-check-enable" checked
                                               data-id="<?= $value->id; ?>" data-table="productAddonDetail">
                                        <span class="slider round"></span>
                                    </label>
                                    <?php
                                } else {
                                    ?>
                                    <label class="switch">
                                        <input type="checkbox" class="input-check-enable" data-id="<?= $value->id; ?>"
                                               data-table="productAddonDetail">
                                        <span class="slider round"></span>
                                    </label>
                                    <?php
                                }
                                ?>

                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php
}

function product_add_on_type_admin_page_contents()
{
    global $bss_options;
    $option = 'addon-type';
    $fetch_data = get_data_from_db($option);
    $list_data = $fetch_data['list_data'];
    $total = $fetch_data['total'];
    $limit = $fetch_data['limit'];
    $page = $fetch_data['page'];
    ?>
    <h1>Product add-on type</h1>
    <div class="wrap">
        <div class="row">
            <div class="col-md-3 col-sm-4 offset-md-9 offset-sm-8">
                <form>
                    <input type="hidden" name="page" value="product_add_on_type_admin_page_contents"/>
                    <div class="text-right">
                        <div class="tablenav-pages">
                            <span class="displaying-num"><?php _e($total) ?> items</span>
                            <span class="pagination-links">
                <a class="first-page" href="#"
                   onclick="jQuery('#current-page-selector').val(1); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">«</span>
                </a>
                <a class="previous-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e($page - 1) ?>); jQuery('form').submit();">
                <span class="tablenav-pages-navspan" aria-hidden="true">‹</span>
                </a>
                <span class="paging-input">
                  <label for="current-page-selector" class="screen-reader-text">Current Page</label>
                  <input class="current-page" id="current-page-selector" type="text" name="paged"
                         value="<?php echo $page ?>" size="1" aria-describedby="table-paging">
                  <span class="tablenav-paging-text"> of <span class="total-pages"><?php _e(ceil($total / $limit)) ?>
                      </span></span>
                </span>
                <a class="next-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e($page + 1) ?>); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">›</span>
                </a>
                <a class="last-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e(ceil($total / $limit)) ?>); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">»</span>
                </a>
              </span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">NAME</th>
                        <th scope="col">DESCRIPTION</th>
                        <th scope="col"></i></th>
                    </tr>
                    </thead>
                    <tbody class="update-product">
                    <?php
                    $listProducts = productList::all();
                    $countProduct = 0;
                    foreach ($list_data as $key => $value) {
                        ?>
                        <tr>
                            <td>
                                <span><?php echo $value->product_add_on_type; ?></span>
                            </td>
                            <td>
                                <?php
                                echo $value->description;
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($value->enable == 1) {
                                    ?>
                                    <label class="switch">
                                        <input type="checkbox" class="input-check-enable" checked
                                               data-id="<?= $value->id; ?>" data-table="productAddonTypeDetail">
                                        <span class="slider round"></span>
                                    </label>
                                    <?php
                                } else {
                                    ?>
                                    <label class="switch">
                                        <input type="checkbox" class="input-check-enable" data-id="<?= $value->id; ?>"
                                               data-table="productAddonTypeDetail">
                                        <span class="slider round"></span>
                                    </label>
                                    <?php
                                }
                                ?>

                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php
}

function product_promotion_admin_page_contents()
{
    global $bss_options;
    $option = 'promotions';
    $fetch_data = get_data_from_db($option);
    $list_data = $fetch_data['list_data'];
    $total = $fetch_data['total'];
    $limit = $fetch_data['limit'];
    $page = $fetch_data['page'];
    ?>
    <h1>Promotion</h1>
    <div class="wrap">
        <div class="row">
            <div class="col-md-3 col-sm-4 offset-md-9 offset-sm-8">
                <form>
                    <input type="hidden" name="page" value="product_promotion_admin_page_contents"/>
                    <div class="text-right">
                        <div class="tablenav-pages">
                            <span class="displaying-num"><?php _e($total) ?> items</span>
                            <span class="pagination-links">
                <a class="first-page" href="#"
                   onclick="jQuery('#current-page-selector').val(1); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">«</span>
                </a>
                <a class="previous-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e($page - 1) ?>); jQuery('form').submit();">
                <span class="tablenav-pages-navspan" aria-hidden="true">‹</span>
                </a>
                <span class="paging-input">
                  <label for="current-page-selector" class="screen-reader-text">Current Page</label>
                  <input class="current-page" id="current-page-selector" type="text" name="paged"
                         value="<?php echo $page ?>" size="1" aria-describedby="table-paging">
                  <span class="tablenav-paging-text"> of <span class="total-pages"><?php _e(ceil($total / $limit)) ?>
                      </span></span>
                </span>
                <a class="next-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e($page + 1) ?>); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">›</span>
                </a>
                <a class="last-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e(ceil($total / $limit)) ?>); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">»</span>
                </a>
              </span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">NAME</th>
                        <th scope="col">DESCRIPTION</i></th>
                        <th scope="col">START DATE</th>
                        <th scope="col">END DATE</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody class="update-product">
                    <?php
                    foreach ($list_data as $key => $value) {
                        ?>
                        <tr>
                            <td>
                                <img src="<?= $value->image; ?>" style="height: 42px; width: 42px;">
                                <span style="padding-left: 20px;"><?php echo $value->product_promotion; ?></span>
                            </td>
                            <td>
                                <?php
                                echo $value->description;
                                ?>
                            </td>
                            <td>
                                <?php
                                echo date('d/m/y', strtotime($value->starts_from));
                                ?>
                            </td>
                            <td>
                                <?php
                                echo date('d/m/y', strtotime($value->ends_till));
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($value->enable == 1) {
                                    ?>
                                    <label class="switch">
                                        <input type="checkbox" class="input-check-enable" checked
                                               data-id="<?= $value->id; ?>" data-table="productPromotionDetail">
                                        <span class="slider round"></span>
                                    </label>
                                    <?php
                                } else {
                                    ?>
                                    <label class="switch">
                                        <input type="checkbox" class="input-check-enable" data-id="<?= $value->id; ?>"
                                               data-table="productPromotionDetail">
                                        <span class="slider round"></span>
                                    </label>
                                    <?php
                                }
                                ?>

                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php
}

function product_gift_admin_page_contents()
{
    global $bss_options;
    $option = 'gifts';
    $fetch_data = get_data_from_db($option);
    $list_data = $fetch_data['list_data'];
    $total = $fetch_data['total'];
    $limit = $fetch_data['limit'];
    $page = $fetch_data['page'];
    ?>
    <h1>Gift</h1>
    <div class="wrap">
        <div class="row">
            <div class="col-md-3 col-sm-4 offset-md-9 offset-sm-8">
                <form>
                    <input type="hidden" name="page" value="product_gift_admin_page_contents"/>
                    <div class="text-right">
                        <div class="tablenav-pages">
                            <span class="displaying-num"><?php _e($total) ?> items</span>
                            <span class="pagination-links">
                <a class="first-page" href="#"
                   onclick="jQuery('#current-page-selector').val(1); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">«</span>
                </a>
                <a class="previous-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e($page - 1) ?>); jQuery('form').submit();">
                <span class="tablenav-pages-navspan" aria-hidden="true">‹</span>
                </a>
                <span class="paging-input">
                  <label for="current-page-selector" class="screen-reader-text">Current Page</label>
                  <input class="current-page" id="current-page-selector" type="text" name="paged"
                         value="<?php echo $page ?>" size="1" aria-describedby="table-paging">
                  <span class="tablenav-paging-text"> of <span class="total-pages"><?php _e(ceil($total / $limit)) ?>
                      </span></span>
                </span>
                <a class="next-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e($page + 1) ?>); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">›</span>
                </a>
                <a class="last-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e(ceil($total / $limit)) ?>); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">»</span>
                </a>
              </span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">NAME</th>
                        <th scope="col">DESCRIPTION</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody class="update-product">
                    <?php
                    foreach ($list_data as $key => $value) {
                        ?>
                        <tr>
                            <td>
                                <img src="<?= $value->image; ?>" style="height: 42px; width: 42px;">
                                <span style="padding-left: 20px;"><?php echo $value->gift; ?></span>
                            </td>
                            <td>
                                <?php
                                echo $value->description;
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($value->enable == 1) {
                                    ?>
                                    <label class="switch">
                                        <input type="checkbox" class="input-check-enable" checked
                                               data-id="<?= $value->id; ?>" data-table="productGift">
                                        <span class="slider round"></span>
                                    </label>
                                    <?php
                                } else {
                                    ?>
                                    <label class="switch">
                                        <input type="checkbox" class="input-check-enable" data-id="<?= $value->id; ?>"
                                               data-table="productGift">
                                        <span class="slider round"></span>
                                    </label>
                                    <?php
                                }
                                ?>

                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php
}

function product_gift_categories_admin_page_contents()
{
    global $bss_options;
    $option = 'gift-categories';
    $fetch_data = get_data_from_db($option);
    $list_data = $fetch_data['list_data'];
    $total = $fetch_data['total'];
    $limit = $fetch_data['limit'];
    $page = $fetch_data['page'];
    ?>
    <h1>Gift</h1>
    <div class="wrap">
        <div class="row">
            <div class="col-md-3 col-sm-4 offset-md-9 offset-sm-8">
                <form>
                    <input type="hidden" name="page" value="product_gift_categories_admin_page_contents"/>
                    <div class="text-right">
                        <div class="tablenav-pages">
                            <span class="displaying-num"><?php _e($total) ?> items</span>
                            <span class="pagination-links">
                <a class="first-page" href="#"
                   onclick="jQuery('#current-page-selector').val(1); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">«</span>
                </a>
                <a class="previous-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e($page - 1) ?>); jQuery('form').submit();">
                <span class="tablenav-pages-navspan" aria-hidden="true">‹</span>
                </a>
                <span class="paging-input">
                  <label for="current-page-selector" class="screen-reader-text">Current Page</label>
                  <input class="current-page" id="current-page-selector" type="text" name="paged"
                         value="<?php echo $page ?>" size="1" aria-describedby="table-paging">
                  <span class="tablenav-paging-text"> of <span class="total-pages"><?php _e(ceil($total / $limit)) ?>
                      </span></span>
                </span>
                <a class="next-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e($page + 1) ?>); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">›</span>
                </a>
                <a class="last-page" href="#"
                   onclick="jQuery('#current-page-selector').val(<?php _e(ceil($total / $limit)) ?>); jQuery('form').submit();">
                  <span class="tablenav-pages-navspan" aria-hidden="true">»</span>
                </a>
              </span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">NAME</th>
                        <th scope="col">DESCRIPTION</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody class="update-product">
                    <?php
                    foreach ($list_data as $key => $value) {
                        ?>
                        <tr>
                            <td>
                                <img src="<?= $value->image; ?>" style="height: 42px; width: 42px;">
                                <span style="padding-left: 20px;"><?php echo $value->gift_categories; ?></span>
                            </td>
                            <td>
                                <?php
                                echo $value->description;
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($value->enable == 1) {
                                    ?>
                                    <label class="switch">
                                        <input type="checkbox" class="input-check-enable" checked
                                               data-id="<?= $value->id; ?>" data-table="productGiftcategories">
                                        <span class="slider round"></span>
                                    </label>
                                    <?php
                                } else {
                                    ?>
                                    <label class="switch">
                                        <input type="checkbox" class="input-check-enable" data-id="<?= $value->id; ?>"
                                               data-table="productGiftcategories">
                                        <span class="slider round"></span>
                                    </label>
                                    <?php
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php
}

function update_status()
{
    global $bss_options;
    $returnData = array(
        'success' => false
    );
    if (isset($_POST['enable'])) {
        $_POST['name_table']::update(['enable' => $_POST['enable']], ['id' => $_POST['product_id']]);
        $returnData = array(
            'success' => true
        );
    }
    wp_send_json($returnData);
}

function delete_db()
{
    productList::droptable();
    productAddonDetail::droptable();
    productAddonTypeDetail::droptable();
    productAttribute::droptable();
    productCategories::droptable();
    productDetail::droptable();
    productGift::droptable();
    productGiftcategories::droptable();
    productPromotionDetail::droptable();
    lastUpdateTime::droptable();
    $retunData = array(
        'success' => true
    );
    wp_send_json($retunData);
}

?>