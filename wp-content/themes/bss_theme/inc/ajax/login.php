<?php
function saveInfoCustomer($id , $first_name, $last_name, $name, $phone, $address, $avatar, $email, $listAddress)
{
    $_SESSION['profile']['id'] = $id;
    $_SESSION['profile']['first_name'] = $first_name;
    $_SESSION['profile']['last_name'] = $last_name;
    $_SESSION['profile']['name'] = $name;
    $_SESSION['profile']['name'] = $name;
    $_SESSION['profile']['phone'] = $phone;
    $_SESSION['profile']['address'] = $address;
    $_SESSION['profile']['avatar'] = $avatar;
    $_SESSION['profile']['email'] = $email;
    $_SESSION['profile']['address'] = $listAddress;
    return true;
}

function changeDataprofile($key, $data)
{
    $_SESSION['profile'][$key] = $data;
    return true;
}

function login_form()
{
    global $bss_options;
    $returnData = array(
        'success' => false
    );
    $username = isset($_POST['username']) ? $_POST['username'] : null;
    $password = isset($_POST['password']) ? $_POST['password'] : null;
    $bodyAPI = array(
        'username' => array(
            'value' => $username,
            'brand_id' => '44'
        ),
        'password' => $password,
        'provider' => 'users'
    );
    $bodyAPI = json_encode($bodyAPI);
    $url = $bss_options['API_CXI_URL'] . '/oauth/sign-in';
    $args = array(
        'blocking' => true,
        'headers' => array(
            'X-APP-ID' => $bss_options['API_CXI_X_APP_ID'],
            'Content-Type' => 'application/json'
        ),
        'body' => $bodyAPI
    );
    $response = wp_remote_post($url, $args);
    if (is_wp_error($response)) {
        return false;
    }
    $body = wp_remote_retrieve_body($response);
    $data = json_decode($body);

    if ($username && $password) {
        if ($data->error) {
            $returnData = array(
                'success' => true,
                'error' => true,
                'data' => array(
                    'errors' => $data
                )
            );
        } else {
            $access_token = isset($data->access_token) ? $data->access_token : null;
            if (isset($access_token)) {
                $_SESSION['profile']['access_token'] = $access_token;
            }
            $urlProfiles = $bss_options['API_CXI_URL'] . '/users/profiles';
            $args = array(
                'headers' => array(
                    'X-APP-ID' => $bss_options['API_CXI_X_APP_ID'],
                    'Authorization' => $data->token_type . " " . $access_token
                )
            );
            $response = wp_remote_get($urlProfiles, $args);
            if (is_wp_error($response)) {
                return false;
            }
            $body = wp_remote_retrieve_body($response);
            $dataProfiles = json_decode($body);
            $profiles = $dataProfiles->data;
            $first_name = isset($profiles->first_name) ? $profiles->first_name : null;
            $last_name = isset($profiles->last_name) ? $profiles->last_name : null;
            $name = $first_name . " " . $last_name;
            $phone = isset($profiles->phone) ? $profiles->phone : null;
            $address = isset($profiles->address) ? $profiles->address : null;
            $avatar = isset($profiles->avatar) ? $profiles->avatar : null;
            $email = isset($profiles->email) ? $profiles->email : null;
            $idProfile = isset($profiles->id) ? $profiles->id : null;
            $url = $bss_options['API_CXI_URL'] . '/users/addresses?page=1&per_page=5&order_by=created_at&order=desc';
            $response = wp_remote_get($url, $args);
            if (is_wp_error($response)) {
                return false;
            }
            $body = wp_remote_retrieve_body($response);
            $addressBook = json_decode($body);
            if(!$addressBook->error){
                $listAddress = $addressBook->data;
            }
            $listAddress = isset($listAddress) ? $listAddress : null;

            saveInfoCustomer($idProfile, $first_name, $last_name, $name, $phone, $address, $avatar, $email, $listAddress);
            $returnData = array(
                'success' => true,
                'data' => array(
                    'access_token' => $_SESSION['profile']['access_token'],
                    'profiles' => array(
                        'data-profile' => $dataProfiles,
                        'address' => $listAddress,
                        'abbcaasd' => $_SESSION['profile']
                    )
                )
            );
        }

    }
    wp_send_json($returnData);
}

function reset_pwd()
{
    global $bss_options;
    $returnData = array(
        'success' => false
    );

    $username = isset($_GET['username']) ? $_GET['username'] : null;

    if ($username) {
        $url = $bss_options['API_CXI_URL'] . '/password/reset' . '?username=' . $username . '&provider=user';
        $args = array(
            'headers' => array(
                'X-APP-ID' => $bss_options['API_CXI_X_APP_ID']
            )
        );
        $response = wp_remote_get($url, $args);
        if (is_wp_error($response)) {
            return false;
        }
        $body = wp_remote_retrieve_body($response);
        $data = json_decode($body);
        $returnData = array(
            'success' => true,
            'data' => $data,
            'username' => $username
        );
    }
    wp_send_json($returnData);
}

function otp_reset_pwd()
{
    global $bss_options;
    $returnData = array(
        'success' => false
    );

    $otp_reset_pwd = isset($_GET['otp_reset_pwd']) ? $_GET['otp_reset_pwd'] : null;
    $username = isset($_GET['username_reset_pwd']) ? $_GET['username_reset_pwd'] : null;

    if ($otp_reset_pwd) {
        $url = $bss_options['API_CXI_URL'] . '/password/reset/' . $otp_reset_pwd . '?username=' . $username . '&provider=user';
        $args = array(
            'headers' => array(
                'X-APP-ID' => $bss_options['API_CXI_X_APP_ID'],
            )
        );
        $response = wp_remote_get($url, $args);
        if (is_wp_error($response)) {
            return false;
        }
        $body = wp_remote_retrieve_body($response);
        $data = json_decode($body);
        $returnData = array(
            'success' => true,
            'data' => $data
        );
    }
    wp_send_json($returnData);
}

function new_pwd()
{
    global $bss_options;
    $returnData = array(
        'success' => false
    );
    $reminderCode = isset($_POST['reminderCode']) ? $_POST['reminderCode'] : null;
    $userId = isset($_POST['userId']) ? $_POST['userId'] : null;
    $newpwd = isset($_POST['newpwd']) ? $_POST['newpwd'] : null;
    $confirm_new_pwd = isset($_POST['confirm_new_pwd']) ? $_POST['confirm_new_pwd'] : null;

    if ($reminderCode && $userId && $newpwd && $confirm_new_pwd) {
        $bodyAPI = array(
            'new_password' => $newpwd,
            'new_password_confirmation' => $confirm_new_pwd
        );
        $url = $bss_options['API_CXI_URL'] . '/password/reset/' . $userId . '/' . $reminderCode . '?provider=user';
        $args = array(
            'headers' => array(
                'X-APP-ID' => $bss_options['API_CXI_X_APP_ID'],
                'Content-Type' => 'application/json'
            ),
            'body' => json_encode($bodyAPI)
        );
        $response = wp_remote_post($url, $args);
        if (is_wp_error($response)) {
            return false;
        }
        $body = wp_remote_retrieve_body($response);
        $data = json_decode($body);
        $returnData = array(
            'success' => true,
            'data' => $data
        );
    }

    wp_send_json($returnData);

}

function change_profile()
{
    global $bss_options;
    $returnData = array(
        'success' => false
    );
    $first_name = isset($_POST['first_name']) ? $_POST['first_name'] : null;
    $last_name = isset($_POST['last_name']) ? $_POST['last_name'] : null;
    $phone_number = isset($_POST['phone_number']) ? $_POST['phone_number'] : null;
    $email_profile = isset($_POST['email_profile']) ? $_POST['email_profile'] : null;

    if ($first_name && $last_name) {
        $bodyAPI = array(
            'first_name' => $first_name,
            'last_name' => $last_name
        );
        $args = array(
            'method' => 'PUT',
            'headers' => array(
                'X-APP-ID' => $bss_options['API_CXI_X_APP_ID'],
                'Authorization' => "Bearer " . $_SESSION['profile']['access_token'],
                'Content-Type' => 'application/json'
            ),
            'body' => json_encode($bodyAPI)
        );

        $url = $bss_options['API_CXI_URL'] . '/users/profiles';
        $response = wp_remote_post($url, $args);
        if (is_wp_error($response)) {
            return false;
        }
        $body = wp_remote_retrieve_body($response);
        $data = json_decode($body);
        if (!$data->error) {
            $_SESSION['profile']['first_name'] = $data->first_name;
            $_SESSION['profile']['last_name'] = $data->last_name;
        }
        $returnData = array(
            'success' => true,
            'data' => $data
        );
    }

//    if ($_FILES['file']) {
//
//        $fileImage = file_get_contents(realpath($_FILES['file']['tmp_name']));
//        $encoded_image = base64_encode($fileImage);
//
////        $file = @fopen( $_FILES['file']['name'], 'r' );
////        $file_size = filesize( $_FILES['file']['name'] );
////        $file_data = fread( $fileImage, $_FILES['file']['size'] );
//
//        $postData = array(
//            'file' => $encoded_image
//        );
//        $url = $bss_options['API_CXI_URL'] . '/users/upload-avatar';
//        $args = array(
//            'headers' => array(
//                'X-APP-ID' => $bss_options['API_CXI_X_APP_ID'],
//                'Authorization' => "Bearer " . $_SESSION['profile']['access_token'],
//                'accept'        => 'application/json',
//                'Content-Type' => 'application/binary',
//                'Content-Disposition' => 'attachment; filename=' . $_FILES['file']['name']
//            ),
//            'body'        => $postData
//        );
//
//        $response = wp_remote_post($url, $args);
//        if (is_wp_error($response)) {
//            return false;
//        }
//        $body = wp_remote_retrieve_body($response);
//        $data['update-image'] = json_decode($body);
//        if(!$data['update-image']->error){
//            changeDataprofile('avatar', $data['update-image']->data->avatar );
//        }
//        $returnData = array(
//            'success' => true,
//            'data' => $data,
//            'test' => $postData
//        );
//    }

    wp_send_json($returnData);

}

function change_pwd()
{
    global $bss_options;
    $returnData = array(
        'success' => false
    );
    $old_pwd = isset($_POST['old_pwd']) ? $_POST['old_pwd'] : null;
    $new_pwd = isset($_POST['new_pwd']) ? $_POST['new_pwd'] : null;
    $confirm_new_pwd = isset($_POST['confirm_new_pwd']) ? $_POST['confirm_new_pwd'] : null;

    if ($old_pwd && $new_pwd && $confirm_new_pwd) {
        $bodyAPI = array(
            'current_password' => $old_pwd,
            'password' => $new_pwd,
            'password_confirmation' => $confirm_new_pwd
        );
        $url = $bss_options['API_CXI_URL'] . '/users/profiles/change-password';
        $args = array(
            'headers' => array(
                'X-APP-ID' => $bss_options['API_CXI_X_APP_ID'],
                'Authorization' => "Bearer " . $_SESSION['profile']['access_token'],
                'Content-Type' => 'application/json'
            ),
            'body' => json_encode($bodyAPI)
        );
        $response = wp_remote_post($url, $args);
        if (is_wp_error($response)) {
            return false;
        }
        $body = wp_remote_retrieve_body($response);
        $data = json_decode($body);
        $returnData = array(
            'success' => true,
            'data' => $data
        );
    }
    wp_send_json($returnData);
}

function log_out()
{
    global $bss_options;
    $returnData = array(
        'success' => false
    );
    $log_out = isset($_POST['log_out']) ? $_POST['log_out'] : null;
    if ($log_out) {
        $url = $bss_options['API_CXI_URL'] . '/oauth/logout';
        $args = array(
            'header' => array(
                'X-APP-ID' => $bss_options['API_CXI_X_APP_ID'],
                'Authorization' => "Bearer " . $_SESSION['profile']['access_token'],
                'Content-Type' => 'application/json'
            )
        );
        $response = wp_remote_post($url, $args);
        if (is_wp_error($response)) {
            return false;
        }
        $body = wp_remote_retrieve_body($response);
        $data = json_decode($body);
//        if ($data->error == false){
//            unset($_SESSION['profile']);
//        }
        $returnData = array(
            'success' => true,
            'data' => $response
        );
    }
    wp_send_json($returnData);
}

function add_new_address()
{
    global $bss_options;
    $returnData = array(
        'success' => false
    );
    $infoAddress = isset($_POST['infoAddress']) ? $_POST['infoAddress'] : null;
    if ($infoAddress) {
        $name = isset($infoAddress['name_address']) ? $infoAddress['name_address'] : null;
        $phone = isset($infoAddress['phone_address']) ? $infoAddress['phone_address'] : null;
        $building_name = isset($infoAddress['building_name_address']) ? $infoAddress['building_name_address'] : null;
        $address1 = isset($infoAddress['address']) ? $infoAddress['address'] : null;
        $country = isset($infoAddress['state_address']) ? $infoAddress['state_address'] : null;
        $state = isset($infoAddress['country_address']) ? $infoAddress['country_address'] : null;
        $city = isset($infoAddress['city_address']) ? $infoAddress['city_address'] : null;
        $postcode = isset($infoAddress['postcode_address']) ? $infoAddress['postcode_address'] : null;
        $latitude = isset($infoAddress['latitude_address']) ? $infoAddress['latitude_address'] : 0;
        $longitude = isset($infoAddress['longitude_address']) ? $infoAddress['longitude_address'] : 0;
        $address_nickname = isset($infoAddress['address_nickname']) ? $infoAddress['address_nickname'] : null;
        $bodyAPI = array(
            "address_nickname" => $address_nickname,
            "name" => $name,
            "phone" => $phone,
            "building_name" => $building_name,
            "address1" => $address1,
            "country" => $country,
            "state" => $state,
            "city" => $city,
            "postcode" => $postcode,
            "latitude" => $latitude,
            "longitude" => $longitude
        );
        $url = $bss_options['API_CXI_URL'] . '/users/addresses';
        $args = array(
            'headers' => array(
                'X-APP-ID' => $bss_options['API_CXI_X_APP_ID'],
                'Authorization' => "Bearer " . $_SESSION['profile']['access_token'],
                'Content-Type' => 'application/json'
            ),
            'body' => json_encode($bodyAPI)
        );
        $response = wp_remote_post($url, $args);
        if (is_wp_error($response)) {
            return false;
        }
        $body = wp_remote_retrieve_body($response);
        $data = json_decode($body);
        $returnData = array(
            'success' => true,
            'data' => $data
        );
    }

    wp_send_json($returnData);
}

function get_details_address()
{
    global $bss_options;
    $returnData = array(
        'success' => false
    );

    $idAddress = isset($_GET['idAddress']) ? $_GET['idAddress'] : null;

    if ($idAddress) {
        $url = $bss_options['API_CXI_URL'] . '/users/addresses/' . $idAddress;
        $args = array(
            'headers' => array(
                'X-APP-ID' => $bss_options['API_CXI_X_APP_ID'],
                'Authorization' => "Bearer " . $_SESSION['profile']['access_token']
            )
        );
        $response = wp_remote_get($url, $args);
        if (is_wp_error($response)) {
            return false;
        }
        $body = wp_remote_retrieve_body($response);
        $data = json_decode($body);
        $returnData = array(
            'success' => true,
            'data' => $data
        );
    }

    wp_send_json($returnData);
}

function edit_address()
{
    global $bss_options;
    $returnData = array(
        'success' => false
    );
    $infoAddress = isset($_POST['infoAddress']) ? $_POST['infoAddress'] : null;
    if ($infoAddress) {
        $name = isset($infoAddress['name_address']) ? $infoAddress['name_address'] : null;
        $phone = isset($infoAddress['phone_address']) ? $infoAddress['phone_address'] : null;
        $building_name = isset($infoAddress['building_name_address']) ? $infoAddress['building_name_address'] : null;
        $address1 = isset($infoAddress['address']) ? $infoAddress['address'] : null;
        $country = isset($infoAddress['state_address']) ? $infoAddress['state_address'] : null;
        $state = isset($infoAddress['country_address']) ? $infoAddress['country_address'] : null;
        $city = isset($infoAddress['city_address']) ? $infoAddress['city_address'] : null;
        $postcode = isset($infoAddress['postcode_address']) ? $infoAddress['postcode_address'] : null;
        $latitude = isset($infoAddress['latitude']) ? $infoAddress['latitude'] : 0;
        $longitude = isset($infoAddress['longitude']) ? $infoAddress['longitude'] : 0;
        $address_nickname = isset($infoAddress['address_nickname']) ? $infoAddress['address_nickname'] : null;
        $idAddress = isset($infoAddress['id_address']) ? $infoAddress['id_address'] : null;
        $bodyAPI = array(
            "address_nickname" => $address_nickname,
            "name" => $name,
            "phone" => $phone,
            "building_name" => $building_name,
            "address1" => $address1,
            "country" => $country,
            "state" => $state,
            "city" => $city,
            "postcode" => $postcode,
            "latitude" => $latitude,
            "longitude" => $longitude
        );
        $url = $bss_options['API_CXI_URL'] . '/users/addresses/' . $idAddress;
        $args = array(
            'method' => 'PUT',
            'headers' => array(
                'X-APP-ID' => $bss_options['API_CXI_X_APP_ID'],
                'Authorization' => "Bearer " . $_SESSION['profile']['access_token'],
                'Content-Type' => 'application/json'
            ),
            'body' => json_encode($bodyAPI)
        );
        $response = wp_remote_post($url, $args);
        if (is_wp_error($response)) {
            return false;
        }
        $body = wp_remote_retrieve_body($response);
        $data = json_decode($body);
        $returnData = array(
            'success' => true,
            'data' => $data
        );
    }

    wp_send_json($returnData);
}

function delete_address()
{
    global $bss_options;
    $returnData = array(
        'success' => false
    );

    $idAddress = isset($_POST['idAddress']) ? $_POST['idAddress'] : null;

    if ($idAddress) {
        $url = $bss_options['API_CXI_URL'] . '/users/addresses/' . $idAddress;
        $args = array(
            'method' => 'DELETE',
            'headers' => array(
                'X-APP-ID' => $bss_options['API_CXI_X_APP_ID'],
                'Authorization' => "Bearer " . $_SESSION['profile']['access_token']
            )
        );
        $response = wp_remote_get($url, $args);
        if (is_wp_error($response)) {
            return false;
        }
        $body = wp_remote_retrieve_body($response);
        $data = json_decode($body);
        $returnData = array(
            'success' => true,
            'data' => $data
        );
    }

    wp_send_json($returnData);
}

?>