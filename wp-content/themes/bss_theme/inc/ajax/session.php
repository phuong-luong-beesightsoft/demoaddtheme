<?php

function get_cart_detail()
{
    $returnData = array(
        'success' => true,
        'data' => array(),
        'gifts' => array()
    );
    $cartItems = $_SESSION['cart-items'];
    if ($cartItems) {
        foreach ($cartItems as $key => $cartItem) {
            if ($cartItem['type'] == 'product') {

            }
        }
    }
}

function add_product()
{
    $returnData = array(
        'success' => false
    );
    $key = isset($_POST['key']) ? $_POST['key'] : null;
    $product = isset($_POST['product']) ? $_POST['product'] : null;
    $quantity = isset($_POST['quantity']) ? $_POST['quantity'] : 1;
    $idProduct = isset($_POST['idProduct']) ? $_POST['idProduct'] : null;
    $ChildIdProduct = isset($_POST['ChildIdProduct']) ? $_POST['ChildIdProduct'] : null;
    $infoProduct = productDetail::findByProductDetailId($idProduct);
    $total = isset($_POST['total']) ? $_POST['total'] : null;

    if ($product && $quantity) {
        if (!$key || !isset($_SESSION['cart-items'][$key])) {
            $key = time();
            $addOns = isset($_POST['addOns']) ? $_POST['addOns'] : array();
            $_SESSION['cart-items'][$key]['id'] = $idProduct;
            $_SESSION['cart-items'][$key]['name'] = $product;
            $_SESSION['cart-items'][$key]['childId'] = $ChildIdProduct;
            $_SESSION['cart-items'][$key]['type'] = 'product';
            $_SESSION['cart-items'][$key]['addOns'] = $addOns;
            $_SESSION['cart-items'][$key]['price'] = $total;
            $_SESSION['cart-items'][$key]['image'] = $infoProduct->image;
        }
        $_SESSION['cart-items'][$key]['quantity'] = $quantity;
        $_SESSION['last-updated'] = time();
        $returnData = array(
            'success' => true,
            'data' => array(
                'products' => $_SESSION['cart-items']
            )
        );
    }
    wp_send_json($returnData);

}

function remove_order_product()
{
    $returnData = array(
        'success' => false
    );
    $id = isset($_POST['id']) ? $_POST['id'] : null;
    if ($id && isset($_SESSION['cart-items'][$id])) {
        unset($_SESSION['cart-items'][$id]);
        $_SESSION['last-updated'] = time();
        $returnData = array(
            'success' => true,
            'data' => array(
                'products' => $_SESSION['cart-items']
            )
        );
    }
    wp_send_json($returnData);
}

function update_quantity()
{
    $returnData = array(
        'success' => false
    );
    $key = isset($_POST['key']) ? $_POST['key'] : null;
    $quantity = isset($_POST['quantity']) ? $_POST['quantity'] : 1;
    $oldQuantity = isset($_POST['oldQuantity']) ? $_POST['oldQuantity'] : 1;

    if ($key && isset($_SESSION['cart-items'][$key]) && ((int)$quantity > 0)) {
        $_SESSION['cart-items'][$key]['quantity'] = $quantity;
        $_SESSION['cart-items'][$key]['price'] = ($_SESSION['cart-items'][$key]['price'] * $quantity) / $oldQuantity;
        $_SESSION['last-updated'] = time();
        $returnData = array(
            'success' => true,
            'data' => array(
                'products' => $_SESSION['cart-items'],
            )
        );
    }
    wp_send_json($returnData);
}

function cart_details()
{
    if (!$_SESSION['cart-items']) {
        $products = false;
    } else {
        $products = $_SESSION['cart-items'];
    }
    $returnData = array(
        'success' => true,
        'data' => array(
            'products' => $products
        )
    );
    wp_send_json($returnData);
}

function get_delivery_list()
{
    $returnData = array(
        'success' => true,
        'data' => array()
    );
    $args = array(
        'post_type' => 'location',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'post_parent' => 0
    );
}

function save_info_customer()
{
    $returnData = array(
        'success' => false
    );
    if (isset($_POST['infoCustomer'])) {
        $infoCustomer = $_POST['infoCustomer'];
        $_SESSION['checkout']['customer_id'] = $infoCustomer['customer_id'];
        $_SESSION['checkout']['name'] = $infoCustomer['name'];
        $_SESSION['checkout']['phone'] = $infoCustomer['phone'];
        $_SESSION['checkout']['store'] = $infoCustomer['store'];
        $_SESSION['checkout']['time'] = $infoCustomer['time'];
        $_SESSION['checkout']['type'] = $infoCustomer['type'];
        $_SESSION['checkout']['address'] = $infoCustomer['address'];
        $_SESSION['checkout']['is_guest'] = $infoCustomer['is_guest'];
        $returnData = array(
            'success' => true,
            'data' => $_POST['infoCustomer']
        );
    }
    wp_send_json($returnData);
}

function order_product()
{
    global $bss_options;
    $returnData = array(
        'success' => false
    );
    $infoOrder = isset($_POST['infoOrder']) ? $_POST['infoOrder'] : null;
    if ($infoOrder) {

        $bodyAPI = array();
        isset($infoOrder['order_type']) ? $bodyAPI['order_type'] = $infoOrder['order_type'] : null;
        if($bodyAPI['order_type'] == 'pick-up'){
            $bodyAPI['order_type'] = 'pick_on_outlet';
        }
        (isset($infoOrder['is_guest']) && $infoOrder['is_guest'] == 1) ? $bodyAPI['is_guest'] = true : false;
        isset($infoOrder['customer_id']) ? $bodyAPI['customer_id'] = $infoOrder['customer_id'] : null;
        isset($infoOrder['total_item_count']) ? $bodyAPI['total_item_count'] = $infoOrder['total_item_count'] : null;
        isset($infoOrder['total_qty_ordered']) ? $bodyAPI['total_qty_ordered'] = $infoOrder['total_qty_ordered'] : null;
        isset($infoOrder['order_currency_code']) ? $bodyAPI['order_currency_code'] = $infoOrder['order_currency_code'] : null;
        $name_shipping = isset($infoOrder['name_shipping']) ? $infoOrder['name_shipping'] : null;
        if (isset($name_shipping)) {
            $bodyAPI['shipping_address'] = array();
            isset($infoOrder['name_shipping']) ? $bodyAPI['shipping_address']['name'] = $infoOrder['name_shipping'] : null;
            isset($infoOrder['address1_shipping']) ? $bodyAPI['shipping_address']['address1'] = $infoOrder['address1_shipping'] : null;
            isset($infoOrder['country_shipping']) ? $bodyAPI['shipping_address']['country'] = $infoOrder['country_shipping'] : null;
            isset($infoOrder['state_shipping']) ? $bodyAPI['shipping_address']['state'] = $infoOrder['state_shipping'] : null;
            isset($infoOrder['city_shipping']) ? $bodyAPI['shipping_address']['city'] = $infoOrder['city_shipping'] : null;
            isset($infoOrder['postcode_shipping']) ? $bodyAPI['shipping_address']['postcode'] = $infoOrder['postcode_shipping'] : null;
            isset($infoOrder['phone_shipping']) ? $bodyAPI['shipping_address']['phone'] = $infoOrder['phone_shipping'] : null;
            isset($infoOrder['building_name_shipping']) ? $bodyAPI['shipping_address']['building_name'] = $infoOrder['building_name_shipping'] : null;
            isset($infoOrder['address_nickname_shipping']) ? $bodyAPI['shipping_address']['address_nickname'] = $infoOrder['address_nickname_shipping'] : null;
            isset($infoOrder['notes_shipping']) ? $bodyAPI['shipping_address']['notes'] = $infoOrder['notes_shipping'] : null;
            isset($infoOrder['latitude_shipping']) ? $bodyAPI['shipping_address']['latitude'] = 10.8014797 : null;
            isset($infoOrder['longitude_shipping']) ? $bodyAPI['shipping_address']['longitude'] = 106.6526593 : null;
            isset($infoOrder['address_type_shipping']) ? $bodyAPI['shipping_address']['address_type'] = $infoOrder['address_type_shipping'] : null;
            $bodyAPI['billing_address'] = array();
            isset($infoOrder['name_shipping']) ? $bodyAPI['billing_address']['name'] = $infoOrder['name_shipping'] : null;
            isset($infoOrder['address1_shipping']) ? $bodyAPI['billing_address']['address1'] = $infoOrder['address1_shipping'] : null;
            isset($infoOrder['country_shipping']) ? $bodyAPI['billing_address']['country'] = $infoOrder['country_shipping'] : null;
            isset($infoOrder['state_shipping']) ? $bodyAPI['billing_address']['state'] = $infoOrder['state_shipping'] : null;
            isset($infoOrder['city_shipping']) ? $bodyAPI['billing_address']['city'] = $infoOrder['city_shipping'] : null;
            isset($infoOrder['postcode_shipping']) ? $bodyAPI['billing_address']['postcode'] = $infoOrder['postcode_shipping'] : null;
            isset($infoOrder['phone_shipping']) ? $bodyAPI['billing_address']['phone'] = $infoOrder['phone_shipping'] : null;
            isset($infoOrder['building_name_shipping']) ? $bodyAPI['billing_address']['building_name'] = $infoOrder['building_name_shipping'] : null;
            isset($infoOrder['address_nickname_shipping']) ? $bodyAPI['billing_address']['address_nickname'] = $infoOrder['address_nickname_shipping'] : null;
            isset($infoOrder['notes_shipping']) ? $bodyAPI['billing_address']['notes'] = $infoOrder['notes_shipping'] : null;
            isset($infoOrder['latitude_shipping']) ? $bodyAPI['billing_address']['latitude'] = 10.8014797 : null;
            isset($infoOrder['longitude_shipping']) ? $bodyAPI['billing_address']['longitude'] = 106.6526593 : null;
            isset($infoOrder['address_type_shipping']) ? $bodyAPI['billing_address']['address_type'] = $infoOrder['address_type_shipping'] : null;
        }
        isset($infoOrder['payment_method_id']) ? $bodyAPI['payment_method_id'] = $infoOrder['payment_method_id'] : null;
        isset($infoOrder['sub_total']) ? $bodyAPI['sub_total'] = $infoOrder['sub_total'] : null;
        isset($infoOrder['discount_amount']) ? $bodyAPI['discount_amount'] = $infoOrder['discount_amount'] : null;
        isset($infoOrder['brand_id']) ? $bodyAPI['brand_id'] = $infoOrder['brand_id'] : null;
        isset($infoOrder['outlet_id']) ? $bodyAPI['outlet_id'] = $infoOrder['outlet_id'] : null;
        isset($infoOrder['platform']) ? $bodyAPI['platform'] = $infoOrder['platform'] : null;

//        $bodyAPI['comments'] = array(
//            'id' => 0,
//            'content' => 'Comment here ..'
//        );

        // Items Array
        $arrItems = array();
        $bodyAPI['items'] = array();
        $cntArray = 0;
        if (isset($_SESSION['cart-items'])) {
            foreach ($_SESSION['cart-items'] as $key => $item) {
                $bodyAPI['items'][$cntArray] = array(
                    'id' => $item['id'],
                    'qty_ordered' => $item['quantity'],
                    'price' => $item['price'],
                    'total' => (int)$item['price'] * (int)$item['quantity'],
                    'addons' => $item['addons'],
                    'child' => array(
                        'id' => $item['childId'],
                        'qty_ordered' => $item['quantity'],
                        'price' => (int)$item['price'] * (int)$item['quantity'],
                        'total' => $item['price']
                    )
                );
                foreach ($item['addOns'] as $k => $addOn) {
                    $bodyAPI['items'][$cntArray]['addons'][$k] = array(
                        'addon_id' => $addOn['id'],
                        'qty_ordered' => $addOn['quantity'],
                        'price' => $addOn['price'],
                        'total' => (int)$addOn['price'] * (int)$addOn['quantity']
                    );
                }

                $cntArray++;
            }
        }
        // End Items Array

        $promotions_id = isset($infoOrder['promotions_id']) ? $infoOrder['promotions_id'] : null;
        $promotions_name = isset($infoOrder['promotions_name']) ? $infoOrder['promotions_name'] : null;
        $promotions_code = isset($infoOrder['promotions_code']) ? $infoOrder['promotions_code'] : null;
        $promotions_discount_amount = isset($infoOrder['promotions_discount_amount']) ? $infoOrder['promotions_discount_amount'] : null;
        $promotions_maximum_discount = isset($infoOrder['promotions_maximum_discount']) ? $infoOrder['promotions_maximum_discount'] : null;
        $promotion_code = isset($infoOrder['promotion_code']) ? $infoOrder['promotion_code'] : null;
        $coupon_code = isset($infoOrder['coupon_code']) ? $infoOrder['coupon_code'] : null;

        $url = $bss_options['API_CXI_URL'] . '/checkout';

        $args = array(
            'headers' => array(
                'X-APP-ID' => $bss_options['API_CXI_X_APP_ID'],
                'Authorization' => 'Bearer ' . $_SESSION['profile']['access_token'],
                'Content-Type' => 'application/json'
            ),
            'body' => json_encode($bodyAPI)
        );

        $response = wp_remote_post($url, $args);
        if(is_wp_error($response)){
            return false;
        }
        $body = wp_remote_retrieve_body($response);
        $data = json_decode($body);

        $returnData = array(
            'success' => true,
            'bodyAPI' => json_encode($bodyAPI),
            'data' => $data,
            'token' => 'Bearer ' . $_SESSION['profile']['access_token']
        );
    }
    wp_send_json($returnData);
}