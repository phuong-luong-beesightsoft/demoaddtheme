<?php
/**
 * add ajax endpoints
 */
function add_ajax_endpoints()
{
    $namespace = "bss-theme";
    $version = 1;
    $arrRoute = array(
        "custom-product/(?P<id>[\d]+)" => array(
            "methods" => "GET",
            "callback" => "get_custom_product_info"
        ),
        "update-status" => array(
            "methods" => "POST",
            "callback" => "update_status"
        ),
        "delete-db" => array(
            "methods" => "POST",
            "callback" => "delete_db"
        ),
        "update-products" => array(
            "methods" => "POST",
            "callback" => "update_products"
        ),
        "add_product_to_order" => array(
            "methods" => "POST",
            "callback" => "add_product"
        ),
        "remove_order_product" => array(
            "methods" => "POST",
            "callback" => "remove_order_product"
        ),
        "update_quantity" => array(
            "methods" => "POST",
            "callback" => "update_quantity"
        ),
        "login_form" => array(
            "methods" => "POST",
            "callback" => "login_form"
        ),
        "new_pwd" => array(
            "methods" => "POST",
            "callback" => "new_pwd"
        ),
        "change_profile" => array(
            "methods" => "POST",
            "callback" => "change_profile"
        ),
        "register_form" => array(
            "methods" => "POST",
            "callback" => "register_form"
        ),
        "save_token" => array(
            "methods" => "POST",
            "callback" => "save_token"
        ),
        "reset_pwd" => array(
            "methods" => "GET",
            "callback" => "reset_pwd"
        ),
        "otp_reset_pwd" => array(
            "methods" => "GET",
            "callback" => "otp_reset_pwd"
        ),
        "activation_form" => array(
            "methods" => "GET",
            "callback" => "activation_form"
        ),
        "resend_otp" => array(
            "methods" => "GET",
            "callback" => "resend_otp"
        ),
        "cart_details" => array(
            "methods" => "GET",
            "callback" => "cart_details"
        ),
        "save_info_customer" => array(
            "methods" => "POST",
            "callback" => "save_info_customer"
        ),
        "change_pwd" => array(
            "methods" => "POST",
            "callback" => "change_pwd"
        ),
        "log_out" => array(
            "methods" => "POST",
            "callback" => "log_out"
        ),
        "add_new_address" => array(
            'methods' => 'POST',
            'callback' => "add_new_address"
        ),
        "get_details_address" => array(
            'methods' => 'GET',
            'callback' => 'get_details_address'
        ),
        "edit_address" => array(
            'methods' => 'POST',
            'callback' => 'edit_address'
        ),
        "delete_address" => array(
            'methods' => 'POST',
            'callback' => 'delete_address'
        ),
        "order_product" => array(
            'methods' => 'POST',
            'callback' => 'order_product'
        )
    );
    foreach ($arrRoute as $route => $callback) {
        register_rest_route($namespace . '/v' . $version, '/' . $route, array(
            'methods' => $callback["methods"],
            'callback' => $callback["callback"],
            'permission_callback' => function () {
                return true;
            }
        ));
    }
}

add_action('rest_api_init', 'add_ajax_endpoints');
?>