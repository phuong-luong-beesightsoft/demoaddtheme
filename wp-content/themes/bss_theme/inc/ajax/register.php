<?php

function register_form()
{
    global $bss_options;
    $returnData = array(
        'success' => false
    );

    $first_name = isset($_POST['first_name']) ? $_POST['first_name'] : null;
    $last_name = isset($_POST['last_name']) ? $_POST['last_name'] : null;
    $username = isset($_POST['username']) ? $_POST['username'] : null;
    $password = isset($_POST['password']) ? $_POST['password'] : null;
    $phone = isset($_POST['phone']) ? $_POST['phone'] : null;
    $password_confirmation = isset($_POST['password_confirmation']) ? $_POST['password_confirmation'] : null;
    $gender = isset($_POST['gender']) ? $_POST['gender'] : null;
    $dob = isset($_POST['dob']) ? $_POST['dob'] : null;
    $address = isset($_POST['address']) ? $_POST['address'] : null;
    $brand_id = isset($_POST['brand_id']) ? $_POST['brand_id'] : null;

    if ($first_name && $last_name && $address && $brand_id) {
        $bodyAPI = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'username' => $username,
            'phone' => $phone,
            'password' => $password,
            'password_confirmation' => $password_confirmation,
            'gender' => $gender,
            'dob' => $dob,
            'address' => $address,
            'brand_id' => $brand_id
        );
        $url = $bss_options['API_CXI_URL'] . '/register';
        $args = array(
            'blocking' => true,
            'headers' => array(
                'X-APP-ID' => $bss_options['API_CXI_X_APP_ID'],
                'Content-Type' => 'application/json'
            ),
            'body' => json_encode($bodyAPI)
        );
        $response = wp_remote_post($url, $args);
        if (is_wp_error($response)) {
            return false;
        }
        $body = wp_remote_retrieve_body($response);
        $data = json_decode($body);
        $returnData = array(
            'success' => true,
            'data' => $data,
            'username' => $username
        );
    }
    wp_send_json($returnData);
}

function activation_form()
{
    global $bss_options;

    $username = isset($_GET['username']) ? $_GET['username'] : null;
    $otp = isset($_GET['otp']) ? $_GET['otp'] : null;
    $returnData = array(
        'success' => false,
        'username' => $_GET['account'],
        'otp' => $_GET['otp']
    );
    if ($username && $otp) {
        $url = $bss_options['API_CXI_URL'] . '/activations/complete/' . $otp . '?username=' . $username;
        $args = array(
            'headers' => array(
                'X-APP-ID' => $bss_options['API_CXI_X_APP_ID'],
                'username' => $username,
                'otp' => $otp
            )
        );
        $response = wp_remote_get($url, $args);
        if (is_wp_error($response)) {
            return false;
        }
        $body = wp_remote_retrieve_body($response);
        $data = json_decode($body);
        $returnData = array(
            'success' => true,
            'data' => $data
        );
    }

    wp_send_json($returnData);

}

function resend_otp(){
    global $bss_options;
    $returnData = array(
        'success' => false
    );

    $username = isset($_GET['username']) ? $_GET['username'] : null;
    if($username){
        $url = $bss_options['API_CXI_URL'] . '/activations/resend' . '?username='.$username;
        $args = array(
            'headers' => array(
                'X-APP-ID' => $bss_options['API_CXI_X_APP_ID'],
                'username' => $username
            )
        );
        $response = wp_remote_get($url, $args);
        if (is_wp_error($response)){
            return false;
        }
        $body = wp_remote_retrieve_body($response);
        $data = json_decode($body);
        $returnData = array(
            'success' => true,
            'data' => $data
        );
    }
    wp_send_json($returnData);
}
?>
