<?php
function get_custom_product_info($request)
{
  $id = (int)$request['id'];
  $product = productDetail::findByProductDetailId($id);
  $returnData = array(
    'success' => false,
    'data' => null
  );
  if ($product) {
    $addOns = productAddonDetail::all();
    $name = $product->name;
    $legendary = $product->description;
    $image = $product->image;
    $prices = $product->price;
    $variants = json_decode($product->variants);
    $type = $product->type;
    $returnData = array(
      'success' => true,
      'data' => array(
        'id' => $id,
        'name' => $name,
        'legendary' => $legendary,
        'image' => $image,
        'prices' => $prices,
        'variants' => $variants,
        'addOns' => $addOns,
        'type' => $type
      )
    );
  }
  wp_send_json($returnData);
}

?>
