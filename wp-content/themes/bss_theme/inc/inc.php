<?php

$arrFiles = array(
    "/inc/admin/product-menu.php",
    "/inc/admin/store.php",
    "/inc/classes/model.php",
    "/inc/classes/product.php",
    "/inc/classes/categories.php",
    "/inc/classes/attribute.php",
    "/inc/classes/product_add_on.php",
    "/inc/classes/product_add_on_type.php",
    "/inc/classes/promotion.php",
    "/inc/classes/gift.php",
    "/inc/classes/gift_categories.php",
    "/inc/classes/last_update_time.php",
    "/inc/classes/product_detail.php",
    "/inc/classes/store.php",
    "/inc/ajax/ajax-endpoints.php",
    "/inc/ajax/session.php",
    "/inc/ajax/login.php",
    "/inc/ajax/register.php",
    "/inc/table/create-table.php",
    "/inc/table/insert-table.php",
    "/inc/custom-types/custom-product.php"
);

foreach ($arrFiles as $fileRequire) {
    require get_template_directory() . $fileRequire;
}

class View
{
    static function render($file, $data, $echo = true, $includeOne = true)
    {
        extract($data);
        ob_start();
        if ($includeOne) {
            require_once get_template_directory() . "/template/" . $file . ".php";
        } else {
            include get_template_directory() . "/template/" . $file . ".php";
        }
        $content = ob_get_contents();
        ob_clean();
        if ($echo) {
            echo $content;
        } else {
            return $content;
        }
    }
}