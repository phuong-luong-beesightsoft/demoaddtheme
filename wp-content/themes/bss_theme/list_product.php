<?php
/**
 * Template Name: List Product
 */
?>
<?php get_header(); ?>
<div id="primary" class="content-area section section-product">
    <div class="section section-menu-list">
        <ul class="menu-list">
          <?php
          if (!empty($_GET['categories-id'])) {
            $active_categories = $_GET['categories-id'];
          } else {
            $active_categories = 1;
          }
          $list_parent_categories = productCategories::all();
          foreach ($list_parent_categories as $key => $parent_category) {
            if ($parent_category->parent_id == null) {
              ?>
                <li>
                    <form method="get" class="submit-<?php echo $parent_category->categories_id; ?>">
                        <a class="menu-list-item d-flex justify-content-center align-items-center <?php if ($active_categories == $parent_category->categories_id) echo 'active-menu'; ?>"
                           href="#"
                           onclick="jQuery('form.submit-<?php echo $parent_category->categories_id; ?>').submit();">
                            <div class="img-item">
                            </div>
                            <div class="title"><?= $parent_category->categories ?></div>
                        </a>
                        <input type="hidden" value="<?= $parent_category->categories_id; ?>" name="categories-id">
                    </form>
                </li>
              <?php
            }
          }
          ?>
        </ul>
    </div>
    <div class="container">
        <div id="main" class="site-main">
            <div class="list-product">
                <div class="row">
                  <?php
                  $option = 'products';
                  $fetch_data = get_data_from_db($option);
                  $list_data = $fetch_data['list_data'];
                  $total = $fetch_data['total'];
                  $limit = $fetch_data['limit'];
                  $page = $fetch_data['page'];
                  $list_data = productList::all();
                  $list_categories = productCategories::all();
                  $list_categories = productCategories::findByCategoriesId($active_categories);
                  $list_categories->children = json_decode($list_categories->children);
                  if (empty($list_categories->children)) {
                    $list_categories->children[0] = $list_categories;
                    ?>
                      <div class="col-sm-12">
                          <h1 class="block-title"
                              style="text-align: center"><?= $list_categories->children[0]->categories; ?></h1>
                      </div>
                      <div class="owl-carousel owl-theme">
                        <?php
                        foreach ($list_data as $key => $product) {
                          if ($product->enable != 1) {
                            unset($list_data[$key]);
                          }
                          if ($product->enable == 1 && $product->categories_id == $list_categories->children[0]->categories_id) {
                            ?>
                              <div class="product-item item">
                                  <div class="product-item-img">
                                      <img src="<?= $product->image; ?>">
                                  </div>
                                  <div class="product-item-title">
                                      <a href="<?php echo get_site_url() . '/single-product/?product-id=' . $product->product_id; ?>">
                                          <h4 style="text-align: center">
                                            <?= $product->name; ?>
                                          </h4>
                                      </a>
                                  </div>
                                  <div class="description">
                                      <h3 class="desktop-item title"><?php echo $product->name ?></h3>
                                    <?php echo $product->description; ?>
                                  </div>
                                  <div class="btn btn-order check-modal" data-id="<?= $product->product_id; ?>">
                                      ORDER NOW</div>
                              </div>
                            <?php
                            unset($list_data[$key]);
                          }
                        }
                        ?>
                      </div>
                    <?php
                  } else {
                    foreach ($list_categories->children as $key => $value) {
                      $child_categories = productCategories::findByCategoriesId($value->id);
                      if ($child_categories->enable == 1) {
                        ?>
                          <div class="col-sm-12">
                              <h1 class="block-title"
                                  style="text-align: center"><?= $child_categories->categories; ?></h1>
                          </div>
                          <div class="section section-size-list">
                              <ul class="size-price-list d-flex justify-content-center flex-wrap">
                                <?php
                                $detailofProduct = productDetail::findProductDetailByCategoriesId($child_categories->categories_id);
                                $variantsProduct = json_decode($detailofProduct->variants);
                                $idAttributes = json_decode($detailofProduct->super_attributes);
                                $optionsAttribute = $idAttributes[0]->options;

                                $idAttributes = $idAttributes[0]->id;
                                if (!empty($variantsProduct)) {
                                  foreach ($variantsProduct as $key => $variant) {
                                    foreach ($optionsAttribute as $k => $option) {
                                      if ($option->id == $variant->attributes->{$idAttributes}) {
                                        ?>
                                          <li>
                                              <div class="size-price-item d-flex justify-content-center">
                                                  <div class="text-left">
                                                      <div class="desc-item">
                                                          <div class="desktop-item">
                                                            <?=
                                                            number_format($variant->price, 0, ',', '.') . " VND";
                                                            ?>
                                                          </div>
                                                      </div>
                                                      <div class="desc-item">
                                                            <?= $option->admin_name; ?>
                                                      </div>
                                                  </div>
                                              </div>
                                          </li>
                                        <?php
                                      }
                                    }
                                  }
                                }
                                ?>
                              </ul>
                          </div>
                          <div class="owl-carousel owl-theme slider-product">
                            <?php
                            foreach ($list_data as $key => $product) {
                              if ($product->enable != 1) {
                                unset($list_data[$key]);
                              }
                              if ($product->enable == 1 && $product->categories_id == $child_categories->categories_id) {
                                ?>
                                  <div class="product-item item">
                                      <div class="product-item-img">
                                          <img src="<?= $product->image; ?>">
                                      </div>
                                      <div class="product-item-title">
                                          <a href="<?php echo get_site_url() . '/single-product/?product-id=' . $product->product_id; ?>">
                                              <h4 style="text-align: center">
                                                <?= $product->name; ?>
                                              </h4>
                                          </a>
                                      </div>
                                      <div class="description">
                                          <h3 class="desktop-item title"><?php echo $product->name ?></h3>
                                        <?php echo $product->description; ?>
                                      </div>
                                      <div class="btn btn-order check-modal" data-id="<?= $product->product_id; ?>">
                                              ORDER NOW</div>
                                  </div>
                                <?php
                                unset($list_data[$key]);
                              }
                            }
                            ?>
                          </div>
                        <?php
                      }
                    }
                  }
                  if (!empty($list_data) && !empty($list_categories)) {

                  } else {
                    echo "<h1> Don't Have Any Product. Please Add Product !!!</h1>";
                  }
                  ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
