<?php
    get_header();
?>
<?php
    if( have_posts() ){
        while(have_posts()){
            the_post();
        }
    }
?>
<h1>
    <?php
        the_title();
    ?>
</h1>
<?php
    the_content();
?>
<h1><a href="<?= get_site_url().'/list-product/' ?>">List Product Page</a></h1>
<?php
    get_footer();
?>