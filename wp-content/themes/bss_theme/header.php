<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="profile" href="https://beesightsoft.com/">
    <script> var _templateUrl = '<?php echo get_template_directory_uri() ?>';
        var _domainUrl = '<?php echo get_site_url() ?>';
    </script>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<?php
global $bss_options;
?>
<header>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-md">
            <a class="navbar-brand" href="<?php echo site_url() ?>">
                <img src="<?php echo get_template_directory_uri() ?>/assets/images/img-logo.png" alt="logo">
            </a>
            <div class="free-delivery">
                Delivery within 5kms is FREE. All deliveries must be a minimum of VND100,000
            </div>
            <div class="navbar-collapse">
                <ul class="navbar-nav mr-auto">
                    <li class="mobile-item">
                        <div class="custom-header-item">
                            <ul class="language-chooser language-chooser-custom qtranxs_language_chooser"
                                id="desktop-chooser">
                                <li class="language-chooser-item language-chooser-item-vi"><a
                                            href="https://www.yellowcabpizza.vn/vi/menu/pizza/" title="Tiếng Việt (vi)">
                                        <div class="img-language"><img
                                                    src="https://www.yellowcabpizza.vn/wp-content/uploads/2018/11/vn.png"
                                                    alt=""></div>
                                        <p>Tiếng Việt</p></a></li>
                                <li class="language-chooser-item language-chooser-item-en active"><a
                                            href="https://www.yellowcabpizza.vn/en/menu/pizza/" title="English (en)">
                                        <div class="img-language"><img
                                                    src="https://www.yellowcabpizza.vn/wp-content/uploads/2018/11/us.png"
                                                    alt=""></div>
                                        <p>English</p></a></li>
                            </ul>
                            <div class="qtranxs_widget_end"></div>
                        </div>
                    </li>
                    <li>
                        <div class="menu-main-menu-container">
                            <ul id="menu-main-menu" class="navbar-nav mr-auto">
                                <?php
                                $main_menus = wp_get_nav_menu_items(3);
                                foreach ($main_menus as $key => $main_menu) {
                                    ?>
                                    <li
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home nav-item">
                                        <a href="<?php echo $main_menu->url; ?>"
                                           class="nav-link d-flex align-items-center"><?= $main_menu->title; ?></a></li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </div>
                    </li>
                </ul>
                <div class="d-flex justify-content-center align-items-center flex-wrap desktop-item">
                    <div class="custom-header-item">
                        <div id="show-your-language"
                             class="btn my-sm-0 language-item d-flex justify-content-center align-items-center"
                        >
                            <div class="img-item d-flex align-items-center">
                                <img class="click-your-language"
                                     src="https://www.yellowcabpizza.vn/wp-content/uploads/2018/11/en.png" alt="">
                            </div>
                            <div class="img-item d-flex align-items-center my-account-dropdown">
                                <?php
                                if (isset($_SESSION['profile']['access_token'])) {
                                    ?>
                                    <div class="dropdown">
                                        <a class="btn btn-secondary dropdown-toggle" id="dropdownMenuButton"
                                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <?php
                                            if (isset($_SESSION['profile']['avatar'])) {
                                                ?>
                                                <img class="click-your-account"
                                                     src="<?php echo $bss_options['CXI_URL'] . $_SESSION['profile']['avatar']; ?>"
                                                     alt="">
                                                <?php
                                            } else {
                                                ?>
                                                <img class="click-your-account"
                                                     src="<?php echo get_template_directory_uri() ?>/assets/images/user-login.png"
                                                     alt="">
                                                <?php
                                            }
                                            ?>
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="<?php echo get_site_url().'/my-account'; ?>">Account</a>
                                            <a class="dropdown-item" href="<?php echo get_site_url().'/address-book'; ?>">Address Book</a>
                                            <a class="dropdown-item" href="<?php echo get_site_url().'/change-password'; ?>">Change Password</a>
                                            <a id="log_out_action" class="dropdown-item">Log out</a>
                                        </div>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <a href="<?php echo get_site_url() . '/login'; ?>">
                                        <img class="click-your-account"
                                             src="<?php echo get_template_directory_uri() ?>/assets/images/user-login.png"
                                             alt="">
                                    </a>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="custom-header-item cart-item"
                <?php if (isset($_SESSION['checkout']['type'])) {
                    echo 'data-type="' . $_SESSION['checkout']['type'] . '"';
                } ?> >
                <div class="btn my-sm-0 cart-item d-flex justify-content-center show-your-cart">
                    <span class="icon-cart click-your-cart"></span>
                    <a class="desktop-item click-your-cart"> YOUR CART</a>
                    <div class="quantity-product available click-your-cart"><?php
                        if (isset($_SESSION['cart-items'])) {
                            echo '(' . count($_SESSION['cart-items']) . ')';
                        } else {
                            echo "(0)";
                        }
                        ?>
                    </div>
                </div>
            </div>
        </nav>
        <div class="alert alert-dark alert-dismissible fade show free-delivery-mobile" role="alert">
            Delivery within 5kms is FREE. All deliveries must be a minimum of VND100,000
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true" class="icon-cancel-2 icon-close-modal"></span>
            </button>
        </div>
        <div class="overlay"></div>
    </div>
</header>
