# WordPress MySQL database migration
#
# Generated: Wednesday 12. February 2020 02:54 UTC
# Hostname: localhost
# Database: `demoaddtheme`
# URL: //192.168.1.145/demoaddtheme
# Path: C:\\xampp\\htdocs\\demoaddtheme
# Tables: wp_bss_product, wp_bss_product_add_on, wp_bss_product_add_on_type, wp_bss_product_attribute, wp_bss_product_categories, wp_bss_product_detail, wp_bss_product_gift, wp_bss_product_gift_categories, wp_bss_product_last_update_time, wp_bss_product_promotion, wp_commentmeta, wp_comments, wp_links, wp_options, wp_postmeta, wp_posts, wp_term_relationships, wp_term_taxonomy, wp_termmeta, wp_terms, wp_usermeta, wp_users
# Table Prefix: wp_
# Post Types: revision, attachment, page, post
# Protocol: http
# Multisite: false
# Subsite Export: false
# --------------------------------------------------------

/*!40101 SET NAMES utf8 */;

SET sql_mode='NO_AUTO_VALUE_ON_ZERO';



#
# Delete any existing table `wp_bss_product`
#

DROP TABLE IF EXISTS `wp_bss_product`;


#
# Table structure of table `wp_bss_product`
#

CREATE TABLE `wp_bss_product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enable` int(1) DEFAULT NULL,
  `categories_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_bss_product`
#
INSERT INTO `wp_bss_product` ( `id`, `product_id`, `name`, `image`, `description`, `price`, `enable`, `categories_id`) VALUES
(1, 209, 'Pescatore', 'http://cxi.web.beesightsoft.com/assets/media/5.png', 'Tomato sauce, Mozzarella Cheese, Romano Cheese, Mussel, Shrimp, Squid, Fresh Basil, Olive.', '100000.0000', 0, 46),
(2, 213, 'Smoked Bacon & Apples', 'http://cxi.web.beesightsoft.com/assets/media/sba-1.png', 'BBQ sauce, Mozzarella Cheese, Romano Cheese, Bacon, Apple, Green Bell Pepper, Red Onion, Button Mushroom', '189000.0000', 1, 46),
(3, 217, 'New York\'s Finest', 'http://cxi.web.beesightsoft.com/assets/media/nyf_4.png', 'Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Green Bell Pepper, Red Bell Pepper, Mushroom, Olive', '209000.0000', 1, 44),
(4, 221, 'Manhattan Meatlovers', 'http://cxi.web.beesightsoft.com/assets/media/mml_2.png', 'Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Salami', '209000.0000', 1, 44),
(5, 225, 'Teriyaki Salmons', 'http://cxi.web.beesightsoft.com/assets/media/salmon_3.png', 'Teriyaki sauce, mayonaise sauce, salmon, Alfalfa Sprouts', '209000.0000', 1, 44),
(6, 233, 'Roasted Garlic & Shrimp', 'http://cxi.web.beesightsoft.com/assets/media/rgs.png', 'White Wine Sauce, Mozzarella Cheese, Romano Cheese, Roasted Garlic, Shrimp, Onion', '69000.0000', 1, 45),
(7, 238, '#4 Cheese', 'http://cxi.web.beesightsoft.com/assets/media/cheese.png', 'Tomato sauce, Mozzarella Cheese, Romano Cheese, Cheddar Cheese, Cream Cheese', '100000.0000', 1, 1),
(8, 248, 'New York Classic', 'http://cxi.web.beesightsoft.com/assets/media/nyc_6.png', 'Tomato Sauce, Mozzarella Cheese, Romano Cheese, Pepperoni', '69000.0000', 1, 45),
(9, 258, 'Dear Darla Original', 'http://cxi.web.beesightsoft.com/assets/media/img-product-deardarlaoriginal.png', 'Tomato Sauce, Mozzarella Cheese, Romano Cheese, Pepperoni, Mushroom, Tomato, Onion, Black Olive, Caper, Arugula, Alfalfa Sprouts', '119000.0000', 1, 47),
(10, 262, 'Roasted Garlic & Ricotta', 'http://cxi.web.beesightsoft.com/assets/media/img-product-deardarlaroastedgarlicandricotta_2.png', 'Alfredo Sauce, Mozzarella Cheese, Romano Cheese, Roasted Garlic, Cream Cheese, Arugula, Alfalfa Sprouts', '119000.0000', 1, 47),
(11, 266, 'Charlie Chan Chicken', 'http://cxi.web.beesightsoft.com/assets/media/cccp-1.png', 'Spaghetti, Spicy Asian Sauce, Chicken, Mushroom, Roasted Peanut, Green Onion', '99000', 1, 49),
(12, 267, 'Chicken Alfredo', 'http://cxi.web.beesightsoft.com/assets/media/cha-1.png', 'Spaghetti, Alfredo Sauce, Chicken, Olive, Basil', '99000', 1, 49),
(13, 268, 'Spaghetti and Meatballs', 'http://cxi.web.beesightsoft.com/assets/media/smb-1_3.png', 'Spaghetti, Tomato Sauce, Meatball, Romano Cheese', '99000', 1, 49),
(14, 269, 'Shrimp Aglio Olio', 'http://cxi.web.beesightsoft.com/assets/media/olio.png', 'Spaghetti, Shrimp, Olive Oil, Basil, Sundried Tomato, Garlic', '99000', 1, 49),
(15, 270, 'Seafood', 'http://cxi.web.beesightsoft.com/assets/media/seafood-pasta-1_6.png', 'Spaghetti, Alfredo Sauce, Mussel, Shrimp, Black olive, Basil', '99000', 1, 49),
(16, 275, 'Hot sauce', 'http://cxi.web.beesightsoft.com/assets/media/1_27.png', 'Chicken Wings with Spicy Chicken Sauce', '69000.0000', 1, 29),
(17, 279, 'Garlic Parmesan', 'http://cxi.web.beesightsoft.com/assets/media/2_6.png', 'Chicken Wings with Garlic Butter, Parmesan Cheese and Dried Basil', '69000.0000', 1, 29),
(18, 283, 'BBQ Chicken Wings', 'http://cxi.web.beesightsoft.com/assets/media/4_11.png', 'Chicken Wings with Hickory Barbecue Sauce', '69000.0000', 1, 29),
(19, 287, 'Honey Garlic', 'http://cxi.web.beesightsoft.com/assets/media/3.png', 'Chicken Wings with Honey Garlic Sauce', '69000.0000', 1, 29),
(20, 291, 'Mango Habanero', 'http://cxi.web.beesightsoft.com/assets/media/5_1.png', 'Chicken Wings with Mango, Habanero Sauce', '69000.0000', 1, 29),
(21, 295, 'Korean BBQ', 'http://cxi.web.beesightsoft.com/assets/media/6_2.png', 'Chicken Wings with Korean Soy Sauce', '69000.0000', 1, 29),
(22, 299, 'Chicken Garlic Parmesan', 'http://cxi.web.beesightsoft.com/assets/media/garlic-popers.png', 'Chicken nuggets with Garlic Butter, Parmesan Cheese and Dried Basil', '59000', 1, 5),
(23, 300, 'Chicken Hot sauce', 'http://cxi.web.beesightsoft.com/assets/media/hot-poppers_5.png', 'Chicken Nuggets with Spicy Sauce', '59000', 1, 29),
(24, 301, 'Chicken Poppers BBQ', 'http://cxi.web.beesightsoft.com/assets/media/bbq-poppers.png', 'Chicken Nuggets with Hickory BBQ Sauce', '59000', 1, 5),
(25, 302, 'Baked Potato Wedges', 'http://cxi.web.beesightsoft.com/assets/media/2_7.png', 'Savory potato wedges baked to perfection', '59000', 1, 5),
(26, 303, 'Baked Potato Halves', 'http://cxi.web.beesightsoft.com/assets/media/1_28.png', 'Oven-baked potato halves loaded with mozzarella and romano cheese topped with crispy bacon bits', '59000', 1, 5),
(27, 304, 'Garden Salad', 'http://cxi.web.beesightsoft.com/assets/media/salad_5.png', 'Salad, broccoli, cucumber, tomatoes, corn, apple, boiled eggs', '59000', 1, 5),
(28, 305, 'Fries', 'http://cxi.web.beesightsoft.com/assets/media/fries.png', 'Fries', '19000', 1, 5),
(29, 306, 'Pale Ale', 'http://cxi.web.beesightsoft.com/assets/media/pale-ale.png', 'AROMA: Floral and Citrus, Notes of Stone Fruit and Lime FLAVOR: Mild Honey, Citrus zest, Lightly Toasted bread, Refreshing, Fairly dry ABV: 6%', '65000', 1, 13),
(30, 307, 'Far East Ipa', 'http://cxi.web.beesightsoft.com/assets/media/far-east-ipa.png', 'AROMA: Pine, Citrus, Melon, Passion Fruit, and White Wine FLAVOR: Lemon, Subtle Maltiness, Gooseberry, Light Grapefruit ABV: 6,7%', '65000', 1, 13),
(31, 308, 'Saigon Rose', 'http://cxi.web.beesightsoft.com/assets/media/saigon-rose.png', 'Aroma: Fresh Berries, Citrus, Floral Flavor: Raspberry, light maltiness, honey, citrus  ABV: 3%', '65000', 1, 13),
(32, 309, 'Strong Bow Elderflower', 'http://cxi.web.beesightsoft.com/assets/media/elder-flower.png', 'Strong Bow Elderflower - 330ml', '29000', 1, 13),
(33, 310, 'Strong Bow Honey', 'http://cxi.web.beesightsoft.com/assets/media/honey_5.png', 'Strong Bow Honey - 330ml', '29000', 1, 13),
(34, 311, 'Strong Bow Gold Apple', 'http://cxi.web.beesightsoft.com/assets/media/gold-apple-1.png', 'Strong Bow Gold Apple - 330ml', '29000', 1, 13),
(35, 312, 'Strong Bow Red Berries', 'http://cxi.web.beesightsoft.com/assets/media/red-berries.png', 'Strong Bow Red Berries - 330ml', '29000', 1, 13),
(36, 313, 'Heineken', 'http://cxi.web.beesightsoft.com/assets/media/img-product-heineken.png', 'Heineken - 330ml', '29000', 1, 13),
(37, 314, 'Pepsi 1.5L', 'http://cxi.web.beesightsoft.com/assets/media/pepsi-15l.png', 'Pepsi 1.5L', '38000', 1, 13),
(38, 315, 'Oolong Tea', 'http://cxi.web.beesightsoft.com/assets/media/img-product-tea-plus.png', 'Oolong Tea - 330ml', '19000', 1, 13),
(39, 316, 'Sting Strawberry', 'http://cxi.web.beesightsoft.com/assets/media/sting.png', 'Sting Strawberry - 330ml', '19000', 1, 13),
(40, 317, 'Mirinda Organe', 'http://cxi.web.beesightsoft.com/assets/media/mirinda.png', 'Mirinda Organe - 330ml', '19000', 1, 13),
(41, 318, '7UP', 'http://cxi.web.beesightsoft.com/assets/media/7up.png', '7UP - 330ml', '19000', 1, 13),
(42, 319, 'Pepsi Light', 'http://cxi.web.beesightsoft.com/assets/media/pepsi-light.png', 'Pepsi Light - 330ml', '19000', 1, 13),
(43, 320, 'Pepsi', 'http://cxi.web.beesightsoft.com/assets/media/pepsi.png', 'Pepsi - 330ml', '19000', 1, 13),
(44, 321, 'Aquafina', 'http://cxi.web.beesightsoft.com/assets/media/img-product-aquafina_4.png', 'Aquafina - 500ml', '9000', 1, 13),
(45, 330, 'Teriyaki Wings Rice', 'http://cxi.web.beesightsoft.com/assets/media/com-ga_6.png', 'Jasmine rice served with our favorable teriyaki chicken wings and vegetables ( lettuce, cucumber, tomato)', '49000', 1, 50),
(46, 331, 'Teriyaki Popper Rice', 'http://cxi.web.beesightsoft.com/assets/media/com-ga-2.png', 'Jasmine rice served with our favorable teriyaki chicken poppers and vegetables ( lettuce, cucumber, tomato)', '49000', 1, 50),
(47, 374, 'Chicken 3 flavors', 'http://cxi.web.beesightsoft.com/assets/media/bbq-wings_12.png', 'Chicken 3 flavors', '69000.0000', 1, 29),
(48, 409, 'Hawaiian', 'http://cxi.web.beesightsoft.com/assets/media/hwn_9.png', 'Tomato Sauce, Mozzrella Cheese, Romano Cheese, Ham, Bacon, Pineapple', '69000.0000', 1, 45),
(49, 429, 'Chicken Burger', 'http://cxi.web.beesightsoft.com/assets/media/7.jpg', 'Bread, Chicken, Tomatoes, Salad, Cheese, Butter', '85000', 1, 5),
(50, 435, 'Squid ring', 'http://cxi.web.beesightsoft.com/assets/media/13.jpg', 'Squid Ring, Butter, Cheese', '25000', 1, 5),
(51, 436, 'Chicken Garlic Parmesan', 'http://cxi.web.beesightsoft.com/assets/media/14.jpg', 'Chicken nuggets with Garlic Butter, Parmesan Cheese and Dried Basil', '75000', 1, 5),
(52, 437, 'Chicken Hot sauce', 'http://cxi.web.beesightsoft.com/assets/media/15.jpg', 'Chicken Nuggets with Spicy Sauce', '95000', 1, 54),
(53, 438, 'Chicken Poppers BBQ', 'http://cxi.web.beesightsoft.com/assets/media/16.jpg', 'Chicken Nuggets with Hickory BBQ Sauce', '105000', 1, 29),
(54, 439, 'Afritada', 'http://cxi.web.beesightsoft.com/assets/media/17.jpg', 'Spaghetti, Spicy Asian Sauce, Chicken, Mushroom, Roasted Peanut, Green Onion', '85000', 1, 29),
(55, 440, 'Airline chicken', 'http://cxi.web.beesightsoft.com/assets/media/18.jpg', 'Spaghetti, Alfredo Sauce, Chicken, Olive, Basil', '75000', 1, 29),
(56, 441, 'Adobo', 'http://cxi.web.beesightsoft.com/assets/media/19.jpg', 'Spaghetti, Tomato Sauce, Meatball, Romano Cheese', '65000', 1, 29),
(57, 442, 'Andong jjimdak', 'http://cxi.web.beesightsoft.com/assets/media/20.jpg', 'Spaghetti, Pesto Sauce (Fresh Basil, Cashew, Olive Oil), Shrimp, Sundried Tomato', '85000', 1, 29),
(58, 444, 'Ayam bakar', 'http://cxi.web.beesightsoft.com/assets/media/22.jpg', 'Bread, Chicken, Tomatoes, Salad, Cheese, Butter', '85000', 1, 29),
(59, 445, 'Ayam betutu', 'http://cxi.web.beesightsoft.com/assets/media/23.jpg', 'Bread, Egg, Tomatoes, Salad, Cheese, Butter', '75000', 1, 29),
(60, 446, 'Ayam goreng', 'http://cxi.web.beesightsoft.com/assets/media/23_1.jpg', 'Bread, Egg, Tomatoes, Salad, Cheese, Butter', '65000', 1, 29),
(61, 447, 'Ayam kecap', 'http://cxi.web.beesightsoft.com/assets/media/24.jpg', 'Potatoes, garlics', '55000', 1, 29),
(62, 478, 'Pizza & Fries', 'http://cxi.web.beesightsoft.com/assets/media/combo-1_7.png', '01 Pizza Classic 01 Fries 01 Pepsi 330ml', '0', 1, 73),
(63, 479, 'Our Classic Combo', 'http://cxi.web.beesightsoft.com/assets/media/combo2-classic-1_1.png', '01 Pizza Classic 10inch 04 pcs chicken wings 01 Fries 01 Pepsi 330ml', '108000', 1, 73),
(64, 481, 'Group Combo Classicc', 'http://cxi.web.beesightsoft.com/assets/media/combo-3-classic-1_1.png', '01 Pizza Classic 14inch 06 pcs chicken wings 01 Salad 01 Fries 01 1,5L Pepsi', '179000', 1, 73),
(65, 489, 'Four Seasons', 'http://cxi.web.beesightsoft.com/assets/media/fs_3.png', 'Four flavors in one great pizza! Hawaiian, #4Cheese, Roasted Garlic and Shrimp, New York Classic', '219000.0000', 1, 46),
(66, 509, 'test', 'http://cxi.web.beesightsoft.com/assets/media/sba-1_3.png', 'Tomato sauce, Mozzarella Cheese, Romano Cheese, Mussel, Shrimp, Squid, Fresh Basil, Olive', '101400', 1, 73),
(67, 510, 'combo', 'http://cxi.web.beesightsoft.com/assets/media/img-product-deardarlaroastedgarlicandricotta-2.png', 'Tomato sauce, Mozzarella Cheese, Romano Cheese, Mussel, Shrimp, Squid, Fresh Basil, Olive', '151500', 1, 73),
(68, 527, 'Half - Half', 'http://cxi.web.beesightsoft.com/assets/media/half-half.png', 'half - halt', '249000.0000', 1, NULL),
(69, 588, 'Quattro', 'http://cxi.web.beesightsoft.com/assets/media/4-4-pizza-1_4.png', 'Choose 4 favorite pizza flavors Apply for 14 and 18 inch Not apply for Dear Darla', '599000.0000', 1, 74),
(70, 630, 'Sting', 'http://cxi.web.beesightsoft.com', 'just for testing', '10000', 1, 13),
(71, 631, 'Sting', 'http://cxi.web.beesightsoft.com', 'just for testing', '10000', 1, 13),
(72, 632, 'Sting', 'http://cxi.web.beesightsoft.com', 'just for testing', '10000', 1, 13),
(73, 634, 'Sting', 'http://cxi.web.beesightsoft.com/assets/media/download_1581317632.jpg', 'just for testing', '10000', 1, 13) ;

#
# End of data contents of table `wp_bss_product`
# --------------------------------------------------------



#
# Delete any existing table `wp_bss_product_add_on`
#

DROP TABLE IF EXISTS `wp_bss_product_add_on`;


#
# Table structure of table `wp_bss_product_add_on`
#

CREATE TABLE `wp_bss_product_add_on` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_add_on_id` bigint(20) DEFAULT NULL,
  `product_add_on` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enable` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_bss_product_add_on`
#
INSERT INTO `wp_bss_product_add_on` ( `id`, `product_add_on_id`, `product_add_on`, `image`, `description`, `enable`) VALUES
(1, 1, 'Ham', 'http://cxi.web.beesightsoft.com/assets/media/download-1_1.jpg', 'Ham is pork from a leg cut that has been preserved by wet or dry', 1),
(2, 2, 'Bacon', 'http://cxi.web.beesightsoft.com/assets/media/download_12.jpg', 'Bacon is a type of salt-cured pork', 1),
(3, 6, 'Button Mushroom', 'http://cxi.web.beesightsoft.com/assets/media/fresh-button-mushroom-500x500.jpg', 'Agaricus bisporus is an edible basidiomycete mushroom', 1),
(4, 9, 'Mozzarella Cheese', 'http://cxi.web.beesightsoft.com/assets/media/download-2.jpg', 'Cheese is a relatively hard, off-white, sometimes sharp-tasting', 1),
(5, 10, 'Black olives', 'http://cxi.web.beesightsoft.com/assets/media/pitted-black-olives.jpg', 'The olive, known by the botanical name Olea europaea', 1),
(6, 11, 'Green bell pepper', 'http://cxi.web.beesightsoft.com/assets/media/81szof2mhgl-sl1500.jpg', 'The green bell pepper is a sweet pepper picked before ripening', 1),
(7, 12, 'Square-cut', 'http://cxi.web.beesightsoft.com/assets/media/93743046-delicious-homemade-cheese-pizza-cut-into-square-slices.jpg', 'Square-cut is different kinds of cutting', 1),
(8, 13, 'Pie-cut', 'http://cxi.web.beesightsoft.com/assets/media/569ba998e6183e7c008ba0c5-750-563.jpg', 'Pie-cut is a natural pie', 1),
(9, 14, 'Pepperoni', 'http://cxi.web.beesightsoft.com/assets/media/download-3.jpg', 'Pepperoni from pork and very delicious', 1),
(10, 15, 'Shrimp', 'http://cxi.web.beesightsoft.com/assets/media/shrimp-on-wooden-platter-1296x728.jpg', 'Fresh shrimp is prepared with the carefulness', 1),
(11, 16, 'Alfalfa', 'http://cxi.web.beesightsoft.com/assets/media/download-4.jpg', 'Alfalfa, also called lucerne and called Medicago sativa in binomi', 1),
(12, 17, 'Romano cheese', 'http://cxi.web.beesightsoft.com/assets/media/italian-cheese-pecorino-romano-cheese-dop-made-with-whole-sheep-s-milk-free-shipping-1-2000x.png', 'Natural cheese from milk', 1),
(13, 18, 'Cheddar cheese', 'http://cxi.web.beesightsoft.com/assets/media/cheddar-cheese-600x600-300x300.jpg', 'Cheddar cheese is a relatively hard, off-white.', 1) ;

#
# End of data contents of table `wp_bss_product_add_on`
# --------------------------------------------------------



#
# Delete any existing table `wp_bss_product_add_on_type`
#

DROP TABLE IF EXISTS `wp_bss_product_add_on_type`;


#
# Table structure of table `wp_bss_product_add_on_type`
#

CREATE TABLE `wp_bss_product_add_on_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_add_on_type_id` bigint(20) DEFAULT NULL,
  `product_add_on_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enable` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_bss_product_add_on_type`
#
INSERT INTO `wp_bss_product_add_on_type` ( `id`, `product_add_on_type_id`, `product_add_on_type`, `description`, `enable`) VALUES
(1, 13, 'Topping', 'A layer of food poured or spread over a base of a different type', 1),
(2, 15, 'Cutting', 'Cutting is the separation or opening of a physical object', 1),
(3, 16, 'Cheese', 'Cheese is a dairy product derived from milk', 1) ;

#
# End of data contents of table `wp_bss_product_add_on_type`
# --------------------------------------------------------



#
# Delete any existing table `wp_bss_product_attribute`
#

DROP TABLE IF EXISTS `wp_bss_product_attribute`;


#
# Table structure of table `wp_bss_product_attribute`
#

CREATE TABLE `wp_bss_product_attribute` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attribute_id` bigint(20) DEFAULT NULL,
  `attribute` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `options` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `required` int(1) DEFAULT NULL,
  `unique_attribute` int(1) DEFAULT NULL,
  `enable` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_bss_product_attribute`
#
INSERT INTO `wp_bss_product_attribute` ( `id`, `attribute_id`, `attribute`, `type`, `options`, `required`, `unique_attribute`, `enable`) VALUES
(1, 19, 'Pizza - Size', 'select', '[{"id":1,"admin_name":"6 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"6 inches"},{"locale":"en","label":"6 inches"}]},{"id":2,"admin_name":"10 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"10 inches"},{"locale":"en","label":"10 inches"}]},{"id":3,"admin_name":"14 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"14 inches"},{"locale":"en","label":"14 inches"}]},{"id":13,"admin_name":"18 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"18 inches"},{"locale":"en","label":"18 inches"}]},{"id":161,"admin_name":"60 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"60 inches"},{"locale":"en","label":"60 inches"}]}]', 0, 0, 1),
(2, 20, 'Chicken - Piece', 'select', '[{"id":5,"admin_name":"4 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"4 mi\\u1ebfng"},{"locale":"en","label":"4 pieces"}]},{"id":6,"admin_name":"6 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"6 mi\\u1ebfng"},{"locale":"en","label":"6 pieces"}]},{"id":7,"admin_name":"8 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"8 mi\\u1ebfng"},{"locale":"en","label":"8 pieces"}]}]', 0, 0, 1),
(3, 21, 'Spicy', 'select', '[{"id":14,"admin_name":"Normal","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"B\\u00ecnh th\\u01b0\\u1eddng"},{"locale":"en","label":"Normal"}]},{"id":15,"admin_name":"Medium","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"Trung b\\u00ecnh"},{"locale":"en","label":"Medium"}]},{"id":16,"admin_name":"Extreme","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"Si\\u00eau to kh\\u1ed5ng l\\u1ed3"},{"locale":"en","label":"Extreme"}]}]', 0, 0, 1),
(4, 22, 'Flavor', 'select', '[{"id":17,"admin_name":"Normal","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"B\\u00ecnh th\\u01b0\\u1eddng"},{"locale":"en","label":"Normal"}]},{"id":18,"admin_name":"Cheese","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"Ph\\u00f4 mai"},{"locale":"en","label":"Cheese"}]},{"id":19,"admin_name":"Honey","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"B\\u01a1"},{"locale":"en","label":"Honey"}]}]', 0, 0, 1),
(5, 26, '#1 First Half', 'multiselect', '[{"id":25,"admin_name":"NEW YORK\'S FINEST","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Green Bell Pepper, Red Bell Pepper, Mushroom, Olive","image":{"small_thumb":"\\/assets\\/media\\/nyf_1578293502_small_thumb.png","medium_thumb":"\\/assets\\/media\\/nyf_1578293502_medium_thumb.png","original_image":"\\/assets\\/media\\/nyf_1578293502.png"},"image_id":1065,"translations":[{"locale":"vn","label":"NEW YORK\'S FINEST"},{"locale":"en","label":"NEW YORK\'S FINEST"}]},{"id":26,"admin_name":"MANHATTAN MEATLOVERS","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Salami","image":{"small_thumb":"\\/assets\\/media\\/mml_1578293565_small_thumb.png","medium_thumb":"\\/assets\\/media\\/mml_1578293565_medium_thumb.png","original_image":"\\/assets\\/media\\/mml_1578293565.png"},"image_id":1066,"translations":[{"locale":"vn","label":"MANHATTAN MEATLOVERS"},{"locale":"en","label":"MANHATTAN MEATLOVERS"}]},{"id":27,"admin_name":"PESCATORE","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Mussel, Shrimp, Squid, Fresh Basil, Olive","image":{"small_thumb":"\\/assets\\/media\\/pescatore_1578293612_small_thumb.png","medium_thumb":"\\/assets\\/media\\/pescatore_1578293612_medium_thumb.png","original_image":"\\/assets\\/media\\/pescatore_1578293612.png"},"image_id":1067,"translations":[{"locale":"vn","label":"PESCATORE"},{"locale":"en","label":"PESCATORE"}]},{"id":28,"admin_name":"#4CHEESE","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Cheddar Cheese, Cream Cheese","image":{"small_thumb":"\\/assets\\/media\\/4c_1578293624_small_thumb.png","medium_thumb":"\\/assets\\/media\\/4c_1578293624_medium_thumb.png","original_image":"\\/assets\\/media\\/4c_1578293624.png"},"image_id":1068,"translations":[{"locale":"vn","label":"#4PH\\u00d4MAI"},{"locale":"en","label":"#4CHEESE"}]},{"id":29,"admin_name":"HAWAIIAN","description":"Tomato Sauce, Mozzrella Cheese, Romano Cheese, Ham, Bacon, Pineapple","image":{"small_thumb":"\\/assets\\/media\\/hwn_1578293644_small_thumb.png","medium_thumb":"\\/assets\\/media\\/hwn_1578293644_medium_thumb.png","original_image":"\\/assets\\/media\\/hwn_1578293644.png"},"image_id":1069,"translations":[{"locale":"vn","label":"HAWAIIAN"},{"locale":"en","label":"HAWAIIAN"}]},{"id":30,"admin_name":"NY CLASSIC","description":"Tomato Sauce, Mozzarella Cheese, Romano Cheese, Pepperoni","image":{"small_thumb":"\\/assets\\/media\\/nyc_1578293665_small_thumb.png","medium_thumb":"\\/assets\\/media\\/nyc_1578293665_medium_thumb.png","original_image":"\\/assets\\/media\\/nyc_1578293665.png"},"image_id":1070,"translations":[{"locale":"vn","label":"NY CLASSIC"},{"locale":"en","label":"NY CLASSIC"}]}]', 1, 0, 1),
(6, 27, '#2 Second Half', 'multiselect', '[{"id":33,"admin_name":"NEW YORK\'S FINEST","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Green Bell Pepper, Red Bell Pepper, Mushroom, Olive","image":{"small_thumb":"\\/assets\\/media\\/nyf_1578294070_small_thumb.png","medium_thumb":"\\/assets\\/media\\/nyf_1578294070_medium_thumb.png","original_image":"\\/assets\\/media\\/nyf_1578294070.png"},"image_id":1071,"translations":[{"locale":"vn","label":"NEW YORK\'S FINEST"},{"locale":"en","label":"NEW YORK\'S FINEST"}]},{"id":34,"admin_name":"MANHATTAN MEATLOVERS","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Salami","image":{"small_thumb":"\\/assets\\/media\\/mml_1578294088_small_thumb.png","medium_thumb":"\\/assets\\/media\\/mml_1578294088_medium_thumb.png","original_image":"\\/assets\\/media\\/mml_1578294088.png"},"image_id":1072,"translations":[{"locale":"vn","label":"MANHATTAN MEATLOVERS"},{"locale":"en","label":"MANHATTAN MEATLOVERS"}]},{"id":35,"admin_name":"PESCATORE","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Mussel, Shrimp, Squid, Fresh Basil, Olive","image":{"small_thumb":"\\/assets\\/media\\/pescatore_1578294106_small_thumb.png","medium_thumb":"\\/assets\\/media\\/pescatore_1578294106_medium_thumb.png","original_image":"\\/assets\\/media\\/pescatore_1578294106.png"},"image_id":1073,"translations":[{"locale":"vn","label":"PESCATORE"},{"locale":"en","label":"PESCATORE"}]},{"id":36,"admin_name":"#4CHEESE","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Cheddar Cheese, Cream Cheese","image":{"small_thumb":"\\/assets\\/media\\/4c_1578294121_small_thumb.png","medium_thumb":"\\/assets\\/media\\/4c_1578294121_medium_thumb.png","original_image":"\\/assets\\/media\\/4c_1578294121.png"},"image_id":1074,"translations":[{"locale":"vn","label":"#4CHEESE"},{"locale":"en","label":"#4CHEESE"}]},{"id":37,"admin_name":"HAWAIIAN","description":"Tomato Sauce, Mozzrella Cheese, Romano Cheese, Ham, Bacon, Pineapple","image":{"small_thumb":"\\/assets\\/media\\/hwn_1578294199_small_thumb.png","medium_thumb":"\\/assets\\/media\\/hwn_1578294199_medium_thumb.png","original_image":"\\/assets\\/media\\/hwn_1578294199.png"},"image_id":1075,"translations":[{"locale":"vn","label":"HAWAIIAN"},{"locale":"en","label":"HAWAIIAN"}]},{"id":38,"admin_name":"NY CLASSIC","description":"Tomato Sauce, Mozzarella Cheese, Romano Cheese, Pepperoni","image":{"small_thumb":"\\/assets\\/media\\/nyc_1578294214_small_thumb.png","medium_thumb":"\\/assets\\/media\\/nyc_1578294214_medium_thumb.png","original_image":"\\/assets\\/media\\/nyc_1578294214.png"},"image_id":1076,"translations":[{"locale":"vn","label":"NY CLASSIC"},{"locale":"en","label":"NY CLASSIC"}]}]', 1, 0, 1),
(7, 29, '1ST FLAVOR', 'multiselect', '[{"id":39,"admin_name":"NEW YORK\'S FINEST","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Green Bell Pepper, Red Bell Pepper, Mushroom, Olive","image":{"small_thumb":"\\/assets\\/media\\/nyf_1578294250_small_thumb.png","medium_thumb":"\\/assets\\/media\\/nyf_1578294250_medium_thumb.png","original_image":"\\/assets\\/media\\/nyf_1578294250.png"},"image_id":1077,"translations":[{"locale":"vn","label":"NEW YORK\'S FINEST"},{"locale":"en","label":"NEW YORK\'S FINEST"}]},{"id":40,"admin_name":"MANHATTAN MEATLOVERS","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Salami","image":{"small_thumb":"\\/assets\\/media\\/mml_1578294257_small_thumb.png","medium_thumb":"\\/assets\\/media\\/mml_1578294257_medium_thumb.png","original_image":"\\/assets\\/media\\/mml_1578294257.png"},"image_id":1078,"translations":[{"locale":"vn","label":"TH\\u1ecaT MANHATTAN"},{"locale":"en","label":"MANHATTAN MEATLOVERS"}]},{"id":41,"admin_name":"PESCATORE","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Mussel, Shrimp, Squid, Fresh Basil, Olive","image":{"small_thumb":"\\/assets\\/media\\/pescatore_1578294265_small_thumb.png","medium_thumb":"\\/assets\\/media\\/pescatore_1578294265_medium_thumb.png","original_image":"\\/assets\\/media\\/pescatore_1578294265.png"},"image_id":1079,"translations":[{"locale":"vn","label":"PESCATORE"},{"locale":"en","label":"PESCATORE"}]},{"id":42,"admin_name":"#4CHEESE","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Cheddar Cheese, Cream Cheese","image":{"small_thumb":"\\/assets\\/media\\/4c_1578294277_small_thumb.png","medium_thumb":"\\/assets\\/media\\/4c_1578294277_medium_thumb.png","original_image":"\\/assets\\/media\\/4c_1578294277.png"},"image_id":1080,"translations":[{"locale":"vn","label":"#4PH\\u00d4MAI"},{"locale":"en","label":"#4CHEESE"}]},{"id":43,"admin_name":"HAWAIIAN","description":"Tomato Sauce, Mozzrella Cheese, Romano Cheese, Ham, Bacon, Pineapple","image":{"small_thumb":"\\/assets\\/media\\/hwn_1578294285_small_thumb.png","medium_thumb":"\\/assets\\/media\\/hwn_1578294285_medium_thumb.png","original_image":"\\/assets\\/media\\/hwn_1578294285.png"},"image_id":1081,"translations":[{"locale":"vn","label":"HAWAIIAN"},{"locale":"en","label":"HAWAIIAN"}]},{"id":44,"admin_name":"NY CLASSIC","description":"Tomato Sauce, Mozzarella Cheese, Romano Cheese, Pepperoni","image":{"small_thumb":"\\/assets\\/media\\/nyc_1578294292_small_thumb.png","medium_thumb":"\\/assets\\/media\\/nyc_1578294292_medium_thumb.png","original_image":"\\/assets\\/media\\/nyc_1578294292.png"},"image_id":1082,"translations":[{"locale":"vn","label":"NY CLASSIC"},{"locale":"en","label":"NY CLASSIC"}]}]', 1, 0, 1),
(8, 30, '2ND FLAVOR', 'multiselect', '[{"id":45,"admin_name":"NEW YORK\'S FINEST","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Green Bell Pepper, Red Bell Pepper, Mushroom, Olive","image":{"small_thumb":"\\/assets\\/media\\/nyf_1578294371_small_thumb.png","medium_thumb":"\\/assets\\/media\\/nyf_1578294371_medium_thumb.png","original_image":"\\/assets\\/media\\/nyf_1578294371.png"},"image_id":1083,"translations":[{"locale":"vn","label":"NEW YORK\'S FINEST"},{"locale":"en","label":"NEW YORK\'S FINEST"}]},{"id":46,"admin_name":"MANHATTAN MEATLOVERS","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Salami","image":{"small_thumb":"\\/assets\\/media\\/mml_1578294377_small_thumb.png","medium_thumb":"\\/assets\\/media\\/mml_1578294377_medium_thumb.png","original_image":"\\/assets\\/media\\/mml_1578294377.png"},"image_id":1084,"translations":[{"locale":"vn","label":"TH\\u1ecaT MANHATTAN"},{"locale":"en","label":"MANHATTAN MEATLOVERS"}]},{"id":47,"admin_name":"PESCATORE","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Mussel, Shrimp, Squid, Fresh Basil, Olive","image":{"small_thumb":"\\/assets\\/media\\/pescatore_1578294384_small_thumb.png","medium_thumb":"\\/assets\\/media\\/pescatore_1578294384_medium_thumb.png","original_image":"\\/assets\\/media\\/pescatore_1578294384.png"},"image_id":1085,"translations":[{"locale":"vn","label":"PESCATORE"},{"locale":"en","label":"PESCATORE"}]},{"id":48,"admin_name":"#4CHEESE","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Cheddar Cheese, Cream Cheese","image":{"small_thumb":"\\/assets\\/media\\/4c_1578294389_small_thumb.png","medium_thumb":"\\/assets\\/media\\/4c_1578294389_medium_thumb.png","original_image":"\\/assets\\/media\\/4c_1578294389.png"},"image_id":1086,"translations":[{"locale":"vn","label":"#4PH\\u00d4MAI"},{"locale":"en","label":"#4CHEESE"}]},{"id":49,"admin_name":"HAWAIIAN","description":"Tomato Sauce, Mozzrella Cheese, Romano Cheese, Ham, Bacon, Pineapple","image":{"small_thumb":"\\/assets\\/media\\/hwn_1578294399_small_thumb.png","medium_thumb":"\\/assets\\/media\\/hwn_1578294399_medium_thumb.png","original_image":"\\/assets\\/media\\/hwn_1578294399.png"},"image_id":1087,"translations":[{"locale":"vn","label":"HAWAIIAN"},{"locale":"en","label":"HAWAIIAN"}]},{"id":50,"admin_name":"NY CLASSIC","description":"Tomato Sauce, Mozzarella Cheese, Romano Cheese, Pepperoni","image":{"small_thumb":"\\/assets\\/media\\/nyc_1578294406_small_thumb.png","medium_thumb":"\\/assets\\/media\\/nyc_1578294406_medium_thumb.png","original_image":"\\/assets\\/media\\/nyc_1578294406.png"},"image_id":1088,"translations":[{"locale":"vn","label":"NY CLASSIC"},{"locale":"en","label":"NY CLASSIC"}]}]', 1, 0, 1),
(9, 31, '3RD FLAVOR', 'multiselect', '[{"id":51,"admin_name":"NEW YORK\'S FINEST","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Green Bell Pepper, Red Bell Pepper, Mushroom, Olive","image":{"small_thumb":"\\/assets\\/media\\/nyf_1578294470_small_thumb.png","medium_thumb":"\\/assets\\/media\\/nyf_1578294470_medium_thumb.png","original_image":"\\/assets\\/media\\/nyf_1578294470.png"},"image_id":1089,"translations":[{"locale":"vn","label":"NEW YORK\'S FINEST"},{"locale":"en","label":"NEW YORK\'S FINEST"}]},{"id":52,"admin_name":"MANHATTAN MEATLOVERS","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Salami","image":{"small_thumb":"\\/assets\\/media\\/mml_1578294477_small_thumb.png","medium_thumb":"\\/assets\\/media\\/mml_1578294477_medium_thumb.png","original_image":"\\/assets\\/media\\/mml_1578294477.png"},"image_id":1090,"translations":[{"locale":"vn","label":"MANHATTAN MEATLOVERS"},{"locale":"en","label":"MANHATTAN MEATLOVERS"}]},{"id":53,"admin_name":"PESCATORE","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Mussel, Shrimp, Squid, Fresh Basil, Olive","image":{"small_thumb":"\\/assets\\/media\\/pescatore_1578294485_small_thumb.png","medium_thumb":"\\/assets\\/media\\/pescatore_1578294485_medium_thumb.png","original_image":"\\/assets\\/media\\/pescatore_1578294485.png"},"image_id":1091,"translations":[{"locale":"vn","label":"PESCATORE"},{"locale":"en","label":"PESCATORE"}]},{"id":54,"admin_name":"#4CHEESE","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Cheddar Cheese, Cream Cheese","image":{"small_thumb":"\\/assets\\/media\\/4c_1578294490_small_thumb.png","medium_thumb":"\\/assets\\/media\\/4c_1578294490_medium_thumb.png","original_image":"\\/assets\\/media\\/4c_1578294490.png"},"image_id":1092,"translations":[{"locale":"vn","label":"#4PH\\u00d4MAI"},{"locale":"en","label":"#4CHEESE"}]},{"id":55,"admin_name":"HAWAIIAN","description":"Tomato Sauce, Mozzrella Cheese, Romano Cheese, Ham, Bacon, Pineapple","image":{"small_thumb":"\\/assets\\/media\\/hwn_1578294496_small_thumb.png","medium_thumb":"\\/assets\\/media\\/hwn_1578294496_medium_thumb.png","original_image":"\\/assets\\/media\\/hwn_1578294496.png"},"image_id":1093,"translations":[{"locale":"vn","label":"HAWAIIAN"},{"locale":"en","label":"HAWAIIAN"}]},{"id":56,"admin_name":"NY CLASSIC","description":"Tomato Sauce, Mozzarella Cheese, Romano Cheese, Pepperoni","image":{"small_thumb":"\\/assets\\/media\\/nyc_1578294504_small_thumb.png","medium_thumb":"\\/assets\\/media\\/nyc_1578294504_medium_thumb.png","original_image":"\\/assets\\/media\\/nyc_1578294504.png"},"image_id":1094,"translations":[{"locale":"vn","label":"NY CLASSIC"},{"locale":"en","label":"NY CLASSIC"}]}]', 1, 0, 1),
(10, 32, '4TH FLAVOR', 'multiselect', '[{"id":57,"admin_name":"NEW YORK\'S FINEST","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Green Bell Pepper, Red Bell Pepper, Mushroom, Olive","image":{"small_thumb":"\\/assets\\/media\\/nyf_1578294578_small_thumb.png","medium_thumb":"\\/assets\\/media\\/nyf_1578294578_medium_thumb.png","original_image":"\\/assets\\/media\\/nyf_1578294578.png"},"image_id":1095,"translations":[{"locale":"vn","label":"NEW YORK\'S FINEST"},{"locale":"en","label":"NEW YORK\'S FINEST"}]},{"id":58,"admin_name":"MANHATTAN MEATLOVERS","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Salami","image":{"small_thumb":"\\/assets\\/media\\/mml_1578294585_small_thumb.png","medium_thumb":"\\/assets\\/media\\/mml_1578294585_medium_thumb.png","original_image":"\\/assets\\/media\\/mml_1578294585.png"},"image_id":1096,"translations":[{"locale":"vn","label":"TH\\u1ecaT MANHATTAN"},{"locale":"en","label":"MANHATTAN MEATLOVERS"}]},{"id":59,"admin_name":"PESCATORE","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Mussel, Shrimp, Squid, Fresh Basil, Olive","image":{"small_thumb":"\\/assets\\/media\\/pescatore_1578294590_small_thumb.png","medium_thumb":"\\/assets\\/media\\/pescatore_1578294590_medium_thumb.png","original_image":"\\/assets\\/media\\/pescatore_1578294590.png"},"image_id":1097,"translations":[{"locale":"vn","label":"PESCATORE"},{"locale":"en","label":"PESCATORE"}]},{"id":60,"admin_name":"#4CHEESE","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Cheddar Cheese, Cream Cheese","image":{"small_thumb":"\\/assets\\/media\\/4c_1578294595_small_thumb.png","medium_thumb":"\\/assets\\/media\\/4c_1578294595_medium_thumb.png","original_image":"\\/assets\\/media\\/4c_1578294595.png"},"image_id":1098,"translations":[{"locale":"vn","label":"#4PH\\u00d4MAI"},{"locale":"en","label":"#4CHEESE"}]},{"id":61,"admin_name":"HAWAIIAN","description":"Tomato Sauce, Mozzrella Cheese, Romano Cheese, Ham, Bacon, Pineapple","image":{"small_thumb":"\\/assets\\/media\\/hwn_1578294604_small_thumb.png","medium_thumb":"\\/assets\\/media\\/hwn_1578294604_medium_thumb.png","original_image":"\\/assets\\/media\\/hwn_1578294604.png"},"image_id":1099,"translations":[{"locale":"vn","label":"HAWAIIAN"},{"locale":"en","label":"HAWAIIAN"}]},{"id":62,"admin_name":"NY CLASSIC","description":"Tomato Sauce, Mozzarella Cheese, Romano Cheese, Pepperoni","image":{"small_thumb":"\\/assets\\/media\\/nyc_1578294611_small_thumb.png","medium_thumb":"\\/assets\\/media\\/nyc_1578294611_medium_thumb.png","original_image":"\\/assets\\/media\\/nyc_1578294611.png"},"image_id":1100,"translations":[{"locale":"vn","label":"NY CLASSIC"},{"locale":"en","label":"NY CLASSIC"}]}]', 1, 0, 1),
(11, 46, 'test', 'select', '[{"id":162,"admin_name":"test","description":"test","image":null,"image_id":null,"translations":[{"locale":"vn","label":"test"},{"locale":"en","label":"test"}]}]', 0, 0, 1) ;

#
# End of data contents of table `wp_bss_product_attribute`
# --------------------------------------------------------



#
# Delete any existing table `wp_bss_product_categories`
#

DROP TABLE IF EXISTS `wp_bss_product_categories`;


#
# Table structure of table `wp_bss_product_categories`
#

CREATE TABLE `wp_bss_product_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `categories_id` bigint(20) DEFAULT NULL,
  `categories` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `children` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enable` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_bss_product_categories`
#
INSERT INTO `wp_bss_product_categories` ( `id`, `categories_id`, `categories`, `image`, `description`, `parent_id`, `children`, `enable`) VALUES
(1, 1, 'Pizza', 'http://cxi.web.beesightsoft.com/assets/media/img-product-half-half_1.png', 'Pizza description', NULL, '[{"id":40,"name":"Custom Pizza","slug":"custom-pizza","image":"\\/assets\\/media\\/img-product-half-half_9.png","is_activated":1,"display_mode":"products_and_description","description":"Custom Pizza description","children":[],"parent_id":1,"brand_id":44,"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/categories\\/40"}},{"id":44,"name":"Premium Pizza","slug":"premium-pizza","image":"\\/assets\\/media\\/salmon_4.png","is_activated":1,"display_mode":"products_and_description","description":"Premium pizza description","children":[],"parent_id":1,"brand_id":44,"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/categories\\/44"}},{"id":45,"name":"Classic Pizza","slug":"classic-pizza","image":"\\/assets\\/media\\/4c_12.png","is_activated":1,"display_mode":"products_and_description","description":"Classic Pizza description","children":[],"parent_id":1,"brand_id":44,"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/categories\\/45"}},{"id":46,"name":"Deluxe Pizza","slug":"deluxe-pizza","image":"\\/assets\\/media\\/fs_2.png","is_activated":1,"display_mode":"products_and_description","description":"Deluxe Pizza description","children":[],"parent_id":1,"brand_id":44,"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/categories\\/46"}},{"id":47,"name":"Special Pizza","slug":"special-pizza","image":"\\/assets\\/media\\/img-product-deardarlaroastedgarlicandricotta_1.png","is_activated":1,"display_mode":"products_and_description","description":"Special Pizza description","children":[],"parent_id":1,"brand_id":44,"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/categories\\/47"}},{"id":74,"name":"Quattro Pizza","slug":"quattro-pizza","image":"\\/assets\\/media\\/4-4-pizza-1.png","is_activated":1,"display_mode":"products_and_description","description":"","children":[],"parent_id":1,"brand_id":44,"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/categories\\/74"}},{"id":84,"name":"Clothes","slug":"clothes","image":"\\/assets\\/media\\/i2_1578884011.png","is_activated":1,"display_mode":"products_and_description","description":"mo ta","children":[],"parent_id":1,"brand_id":44,"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/categories\\/84"}},{"id":85,"name":"Clothess","slug":"clothess","image":"\\/assets\\/media\\/i3_1578884220.png","is_activated":1,"display_mode":"products_and_description","description":"mo ta","children":[],"parent_id":1,"brand_id":44,"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/categories\\/85"}}]', 1),
(2, 5, 'Appetizers', 'http://cxi.web.beesightsoft.com/assets/media/salad_6.png', 'Appetizer description', NULL, '[]', 1),
(3, 13, 'Drinks', 'http://cxi.web.beesightsoft.com/assets/media/img-product-aquafina.png', 'Drink description', NULL, '[]', 1),
(4, 27, 'Pasta/Rice', 'http://cxi.web.beesightsoft.com/assets/media/smb-1_1.png', 'Pasta description', NULL, '[{"id":49,"name":"Pasta - VND 99,000","slug":"pasta-vnd-99000","image":"\\/assets\\/media\\/smb-1_4.png","is_activated":1,"display_mode":"products_and_description","description":"Pasta - VND 99,000","children":[],"parent_id":27,"brand_id":44,"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/categories\\/49"}},{"id":50,"name":"Rice - VND 49,000","slug":"rice-vnd-49000","image":"\\/assets\\/media\\/com-ga.png","is_activated":1,"display_mode":"products_and_description","description":"Rice - VND 49,000","children":[],"parent_id":27,"brand_id":44,"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/categories\\/50"}}]', 1),
(5, 29, 'Chicken', 'http://cxi.web.beesightsoft.com/assets/media/bbq-wings_7.png', 'Chicken description', NULL, '[]', 1),
(6, 40, 'Custom Pizza', 'http://cxi.web.beesightsoft.com/assets/media/img-product-half-half_9.png', 'Custom Pizza description', 1, '[]', 0),
(7, 44, 'Premium Pizza', 'http://cxi.web.beesightsoft.com/assets/media/salmon_4.png', 'Premium pizza description', 1, '[]', 1),
(8, 45, 'Classic Pizza', 'http://cxi.web.beesightsoft.com/assets/media/4c_12.png', 'Classic Pizza description', 1, '[]', 1),
(9, 46, 'Deluxe Pizza', 'http://cxi.web.beesightsoft.com/assets/media/fs_2.png', 'Deluxe Pizza description', 1, '[]', 1),
(10, 47, 'Special Pizza', 'http://cxi.web.beesightsoft.com/assets/media/img-product-deardarlaroastedgarlicandricotta_1.png', 'Special Pizza description', 1, '[]', 1),
(11, 49, 'Pasta - VND 99,000', 'http://cxi.web.beesightsoft.com/assets/media/smb-1_4.png', 'Pasta - VND 99,000', 27, '[]', 1),
(12, 50, 'Rice - VND 49,000', 'http://cxi.web.beesightsoft.com/assets/media/com-ga.png', 'Rice - VND 49,000', 27, '[]', 1),
(13, 73, 'Combo', 'http://cxi.web.beesightsoft.com/assets/media/combo-1_1.png', 'combo', NULL, '[]', 1),
(14, 74, 'Quattro Pizza', 'http://cxi.web.beesightsoft.com/assets/media/4-4-pizza-1.png', '', 1, '[]', 1),
(15, 84, 'Clothes', 'http://cxi.web.beesightsoft.com/assets/media/i2_1578884011.png', 'mo ta', 1, '[]', 0),
(16, 85, 'Clothess', 'http://cxi.web.beesightsoft.com/assets/media/i3_1578884220.png', 'mo ta', 1, '[]', 0) ;

#
# End of data contents of table `wp_bss_product_categories`
# --------------------------------------------------------



#
# Delete any existing table `wp_bss_product_detail`
#

DROP TABLE IF EXISTS `wp_bss_product_detail`;


#
# Table structure of table `wp_bss_product_detail`
#

CREATE TABLE `wp_bss_product_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `price_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `formated_prices` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sku` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `variants` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `super_attributes` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `combo` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories_id` int(11) DEFAULT NULL,
  `updated_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_bss_product_detail`
#
INSERT INTO `wp_bss_product_detail` ( `id`, `product_id`, `type`, `name`, `status`, `price`, `price_type`, `formated_prices`, `description`, `sku`, `categories`, `image`, `variants`, `super_attributes`, `combo`, `categories_id`, `updated_at`) VALUES
(1, 209, 'configurable', 'Pescatore', '', 189000, NULL, NULL, 'Tomato sauce, Mozzarella Cheese, Romano Cheese, Mussel, Shrimp, Squid, Fresh Basil, Olive.', '1579509692806', '[{"id":46,"name":"Deluxe Pizza"}]', 'http://cxi.web.beesightsoft.com/assets/media/5.png', '[{"id":211,"type":"simple","name":"Pescatore 14 inches","status":true,"price":299000,"prices":[],"formated_price":"\\u20ab299,000","sku":"1568346825594","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":29000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":1,"name":"Ham","price":29000,"prices":[]}]}],"attributes":{"19":3}},{"id":212,"type":"simple","name":"Pescatore 18 inches","status":true,"price":459000,"prices":[],"formated_price":"\\u20ab459,000","sku":"1568346887781","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":49000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":1,"name":"Ham","price":49000,"prices":[]}]}],"attributes":{"19":13}},{"id":497,"type":"simple","name":"30 inches","status":true,"price":100000,"prices":[],"formated_price":"\\u20ab100,000","sku":"1574744483374","addon_types":[],"attributes":{"19":25}},{"id":615,"type":"simple","name":"60 inches","status":true,"price":1000000,"prices":[],"formated_price":"\\u20ab1,000,000","sku":"1578472392262","addon_types":[],"attributes":{"19":161}}]', '[{"id":19,"code":"pizza-size","admin_name":"Pizza - Size","description":null,"image":null,"image_id":null,"type":"select","is_required":0,"is_unique":0,"is_filterable":1,"is_configurable":1,"translations":[{"locale":"vn","name":"vn"},{"locale":"en","name":"en"}],"options":[{"id":1,"admin_name":"6 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"6 inches"},{"locale":"en","label":"6 inches"}]},{"id":2,"admin_name":"10 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"10 inches"},{"locale":"en","label":"10 inches"}]},{"id":3,"admin_name":"14 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"14 inches"},{"locale":"en","label":"14 inches"}]},{"id":13,"admin_name":"18 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"18 inches"},{"locale":"en","label":"18 inches"}]},{"id":161,"admin_name":"60 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"60 inches"},{"locale":"en","label":"60 inches"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/19"}}]', NULL, 46, '2020-01-20 08:41:32'),
(2, 213, 'configurable', 'Smoked Bacon & Apples', '1', 189000, NULL, NULL, 'BBQ sauce, Mozzarella Cheese, Romano Cheese, Bacon, Apple, Green Bell Pepper, Red Onion, Button Mushroom', '1568944509133', '[{"id":46,"name":"Deluxe Pizza"}]', 'http://cxi.web.beesightsoft.com/assets/media/sba-1.png', '[{"id":214,"type":"simple","name":"Smoked Bacon & Apples 10 inches","status":true,"price":189000,"prices":[],"formated_price":"\\u20ab189,000","sku":"1568347163460","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":19000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":1,"name":"Ham","price":19000,"prices":[]}]}],"attributes":{"19":2}},{"id":215,"type":"simple","name":"Smoked Bacon & Apples 14 inches","status":true,"price":299000,"prices":[],"formated_price":"\\u20ab299,000","sku":"1568347214847","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":29000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":1,"name":"Ham","price":29000,"prices":[]}]}],"attributes":{"19":3}},{"id":216,"type":"simple","name":"Smoked Bacon & Apples 18 inches","status":true,"price":459000,"prices":[],"formated_price":"\\u20ab459,000","sku":"1568347271538","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":49000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":1,"name":"Ham","price":49000,"prices":[]}]}],"attributes":{"19":13}}]', '[{"id":19,"code":"pizza-size","admin_name":"Pizza - Size","description":null,"image":null,"image_id":null,"type":"select","is_required":0,"is_unique":0,"is_filterable":1,"is_configurable":1,"translations":[{"locale":"vn","name":"vn"},{"locale":"en","name":"en"}],"options":[{"id":1,"admin_name":"6 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"6 inches"},{"locale":"en","label":"6 inches"}]},{"id":2,"admin_name":"10 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"10 inches"},{"locale":"en","label":"10 inches"}]},{"id":3,"admin_name":"14 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"14 inches"},{"locale":"en","label":"14 inches"}]},{"id":13,"admin_name":"18 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"18 inches"},{"locale":"en","label":"18 inches"}]},{"id":161,"admin_name":"60 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"60 inches"},{"locale":"en","label":"60 inches"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/19"}}]', NULL, 46, '2019-12-19 08:12:24'),
(3, 217, 'configurable', 'New York\'s Finest', '1', 209000, NULL, NULL, 'Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Green Bell Pepper, Red Bell Pepper, Mushroom, Olive', '1568864647934', '[{"id":44,"name":"Premium Pizza"}]', 'http://cxi.web.beesightsoft.com/assets/media/nyf_4.png', '[{"id":218,"type":"simple","name":"New York\'s Finest 10 inches","status":true,"price":209000,"prices":[],"formated_price":"\\u20ab209,000","sku":"1568347488764","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":19000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":1,"name":"Ham","price":19000,"prices":[]}]}],"attributes":{"19":2}},{"id":219,"type":"simple","name":"New York\'s Finest 14 inches","status":true,"price":399000,"prices":[],"formated_price":"\\u20ab399,000","sku":"1568347565317","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":29000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":1,"name":"Ham","price":29000,"prices":[]}]}],"attributes":{"19":3}},{"id":220,"type":"simple","name":"New York\'s Finest 18 inches","status":true,"price":509000,"prices":[],"formated_price":"\\u20ab509,000","sku":"1568347626231","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":49000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":1,"name":"Ham","price":49000,"prices":[]}]}],"attributes":{"19":13}}]', '[{"id":19,"code":"pizza-size","admin_name":"Pizza - Size","description":null,"image":null,"image_id":null,"type":"select","is_required":0,"is_unique":0,"is_filterable":1,"is_configurable":1,"translations":[{"locale":"vn","name":"vn"},{"locale":"en","name":"en"}],"options":[{"id":1,"admin_name":"6 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"6 inches"},{"locale":"en","label":"6 inches"}]},{"id":2,"admin_name":"10 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"10 inches"},{"locale":"en","label":"10 inches"}]},{"id":3,"admin_name":"14 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"14 inches"},{"locale":"en","label":"14 inches"}]},{"id":13,"admin_name":"18 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"18 inches"},{"locale":"en","label":"18 inches"}]},{"id":161,"admin_name":"60 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"60 inches"},{"locale":"en","label":"60 inches"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/19"}}]', NULL, 44, '2019-12-11 10:00:32'),
(4, 221, 'configurable', 'Manhattan Meatlovers', '1', 209000, NULL, NULL, 'Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Salami', '1568943940908', '[{"id":44,"name":"Premium Pizza"}]', 'http://cxi.web.beesightsoft.com/assets/media/mml_2.png', '[{"id":222,"type":"simple","name":"Manhattan Meatlovers 10 inches","status":true,"price":209000,"prices":[],"formated_price":"\\u20ab209,000","sku":"1568347745225","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":19000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":9,"name":"Mozzarella Cheese","price":19000,"prices":[]}]}],"attributes":{"19":2}},{"id":223,"type":"simple","name":"Manhattan Meatlovers 14 inches","status":true,"price":399000,"prices":[],"formated_price":"\\u20ab399,000","sku":"1568347800164","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":29000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":1,"name":"Ham","price":29000,"prices":[]}]}],"attributes":{"19":3}},{"id":224,"type":"simple","name":"Manhattan Meatlovers 18 inches","status":true,"price":509000,"prices":[],"formated_price":"\\u20ab509,000","sku":"1568347834517","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":49000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":6,"name":"Button Mushroom","price":49000,"prices":[]}]}],"attributes":{"19":13}}]', '[{"id":19,"code":"pizza-size","admin_name":"Pizza - Size","description":null,"image":null,"image_id":null,"type":"select","is_required":0,"is_unique":0,"is_filterable":1,"is_configurable":1,"translations":[{"locale":"vn","name":"vn"},{"locale":"en","name":"en"}],"options":[{"id":1,"admin_name":"6 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"6 inches"},{"locale":"en","label":"6 inches"}]},{"id":2,"admin_name":"10 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"10 inches"},{"locale":"en","label":"10 inches"}]},{"id":3,"admin_name":"14 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"14 inches"},{"locale":"en","label":"14 inches"}]},{"id":13,"admin_name":"18 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"18 inches"},{"locale":"en","label":"18 inches"}]},{"id":161,"admin_name":"60 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"60 inches"},{"locale":"en","label":"60 inches"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/19"}}]', NULL, 44, '2019-12-11 10:00:30'),
(5, 225, 'configurable', 'Teriyaki Salmons', '1', 209000, NULL, NULL, 'Teriyaki sauce, mayonaise sauce, salmon, Alfalfa Sprouts', '1578038834640', '[{"id":44,"name":"Premium Pizza"}]', 'http://cxi.web.beesightsoft.com/assets/media/salmon_3.png', '[{"id":226,"type":"simple","name":"Teriyaki Salmon 10 inches","status":true,"price":209000,"prices":[],"formated_price":"\\u20ab209,000","sku":"1568347936160","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":19000,"prices":[],"enable_promotion":0,"apply_same_price":1,"addons":[{"id":1,"name":"Ham","price":19000,"prices":[]},{"id":16,"name":"Alfalfa","price":19000,"prices":[]},{"id":15,"name":"Shrimp","price":19000,"prices":[]},{"id":14,"name":"Pepperoni","price":19000,"prices":[]},{"id":11,"name":"Green bell pepper","price":19000,"prices":[]},{"id":10,"name":"Black olives","price":19000,"prices":[]},{"id":6,"name":"Button Mushroom","price":19000,"prices":[]},{"id":2,"name":"Bacon","price":19000,"prices":[]}]},{"id":15,"name":"Cutting","display_type":"single","price":12000,"prices":[],"enable_promotion":null,"apply_same_price":1,"addons":[{"id":13,"name":"Pie-cut","price":12000,"prices":[]},{"id":12,"name":"Square-cut","price":12000,"prices":[]}]},{"id":16,"name":"Cheese","display_type":"true-false","price":19000,"prices":[],"enable_promotion":null,"apply_same_price":1,"addons":[{"id":18,"name":"Cheddar cheese","price":19000,"prices":[]},{"id":17,"name":"Romano cheese","price":19000,"prices":[]},{"id":9,"name":"Mozzarella Cheese","price":19000,"prices":[]}]}],"attributes":{"19":2}},{"id":227,"type":"simple","name":"Teriyaki Salmon 14 inches","status":true,"price":399000,"prices":[],"formated_price":"\\u20ab399,000","sku":"1568347969844","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":29000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":1,"name":"Ham","price":29000,"prices":[]}]}],"attributes":{"19":3}},{"id":228,"type":"simple","name":"Teriyaki Salmon 18 inches","status":true,"price":509000,"prices":[],"formated_price":"\\u20ab509,000","sku":"1568348002155","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":49000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":1,"name":"Ham","price":49000,"prices":[]}]}],"attributes":{"19":13}}]', '[{"id":19,"code":"pizza-size","admin_name":"Pizza - Size","description":null,"image":null,"image_id":null,"type":"select","is_required":0,"is_unique":0,"is_filterable":1,"is_configurable":1,"translations":[{"locale":"vn","name":"vn"},{"locale":"en","name":"en"}],"options":[{"id":1,"admin_name":"6 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"6 inches"},{"locale":"en","label":"6 inches"}]},{"id":2,"admin_name":"10 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"10 inches"},{"locale":"en","label":"10 inches"}]},{"id":3,"admin_name":"14 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"14 inches"},{"locale":"en","label":"14 inches"}]},{"id":13,"admin_name":"18 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"18 inches"},{"locale":"en","label":"18 inches"}]},{"id":161,"admin_name":"60 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"60 inches"},{"locale":"en","label":"60 inches"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/19"}}]', NULL, 44, '2020-01-03 08:07:14'),
(6, 233, 'configurable', 'Roasted Garlic & Shrimp', '1', 69000, NULL, NULL, 'White Wine Sauce, Mozzarella Cheese, Romano Cheese, Roasted Garlic, Shrimp, Onion', '1568944141157', '[{"id":45,"name":"Classic Pizza"}]', 'http://cxi.web.beesightsoft.com/assets/media/rgs.png', '[{"id":234,"type":"simple","name":"Roasted Garlic & Shrimp 6 inches","status":true,"price":69000,"prices":[],"formated_price":"\\u20ab69,000","sku":"1568348600415","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":9000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":1,"name":"Ham","price":9000,"prices":[]}]}],"attributes":{"19":1}},{"id":235,"type":"simple","name":"Roasted Garlic & Shrimp 10 inches","status":true,"price":159000,"prices":[],"formated_price":"\\u20ab159,000","sku":"1568348638526","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":19000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":1,"name":"Ham","price":19000,"prices":[]}]}],"attributes":{"19":2}},{"id":236,"type":"simple","name":"Roasted Garlic & Shrimp 14 inches","status":true,"price":269000,"prices":[],"formated_price":"\\u20ab269,000","sku":"1568348665150","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":29000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":1,"name":"Ham","price":29000,"prices":[]}]}],"attributes":{"19":3}},{"id":237,"type":"simple","name":"Roasted Garlic & Shrimp 18 inches","status":true,"price":389000,"prices":[],"formated_price":"\\u20ab389,000","sku":"1568348701214","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":49000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":1,"name":"Ham","price":49000,"prices":[]}]}],"attributes":{"19":13}}]', '[{"id":19,"code":"pizza-size","admin_name":"Pizza - Size","description":null,"image":null,"image_id":null,"type":"select","is_required":0,"is_unique":0,"is_filterable":1,"is_configurable":1,"translations":[{"locale":"vn","name":"vn"},{"locale":"en","name":"en"}],"options":[{"id":1,"admin_name":"6 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"6 inches"},{"locale":"en","label":"6 inches"}]},{"id":2,"admin_name":"10 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"10 inches"},{"locale":"en","label":"10 inches"}]},{"id":3,"admin_name":"14 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"14 inches"},{"locale":"en","label":"14 inches"}]},{"id":13,"admin_name":"18 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"18 inches"},{"locale":"en","label":"18 inches"}]},{"id":161,"admin_name":"60 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"60 inches"},{"locale":"en","label":"60 inches"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/19"}}]', NULL, 45, '2019-09-20 08:49:00'),
(7, 238, 'configurable', '#4 Cheese', '1', 69000, NULL, NULL, 'Tomato sauce, Mozzarella Cheese, Romano Cheese, Cheddar Cheese, Cream Cheese', '1577086126700', '[{"id":1,"name":"Pizza"},{"id":45,"name":"Classic Pizza"}]', 'http://cxi.web.beesightsoft.com/assets/media/cheese.png', '[{"id":239,"type":"simple","name":"#4 Cheese 6 inches","status":true,"price":100000,"prices":[],"formated_price":"\\u20ab100,000","sku":"1568348809153","addon_types":[{"id":16,"name":"Cheese","display_type":"true-false","price":9000,"prices":[],"enable_promotion":1,"apply_same_price":1,"addons":[{"id":1,"name":"Ham","price":9000,"prices":[]},{"id":11,"name":"Green bell pepper","price":9000,"prices":[]}]}],"attributes":{"19":1}},{"id":240,"type":"simple","name":"#4 Cheese 10 inches","status":true,"price":159000,"prices":[],"formated_price":"\\u20ab159,000","sku":"1568348842133","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":10000,"prices":[],"enable_promotion":null,"apply_same_price":1,"addons":[{"id":16,"name":"Alfalfa","price":10000,"prices":[]}]},{"id":15,"name":"Cutting","display_type":"single","price":10000,"prices":[],"enable_promotion":0,"apply_same_price":1,"addons":[{"id":12,"name":"Square-cut","price":10000,"prices":[]},{"id":13,"name":"Pie-cut","price":10000,"prices":[]}]},{"id":16,"name":"Cheese","display_type":"true-false","price":null,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":11,"name":"Green bell pepper","price":5000,"prices":[]},{"id":9,"name":"Mozzarella Cheese","price":15000,"prices":[]},{"id":6,"name":"Button Mushroom","price":10000,"prices":[]},{"id":10,"name":"Black olives","price":6000,"prices":[]}]}],"attributes":{"19":2}},{"id":241,"type":"simple","name":"#4 Cheese 14 inches","status":true,"price":269000,"prices":[],"formated_price":"\\u20ab269,000","sku":"1568348873646","addon_types":[{"id":16,"name":"Cheese","display_type":"true-false","price":29000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":9,"name":"Mozzarella Cheese","price":29000,"prices":[]}]}],"attributes":{"19":3}},{"id":242,"type":"simple","name":"#4 Cheese 18 inches","status":true,"price":389000,"prices":[],"formated_price":"\\u20ab389,000","sku":"1568348908124","addon_types":[{"id":16,"name":"Cheese","display_type":"true-false","price":49000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":9,"name":"Mozzarella Cheese","price":49000,"prices":[]}]}],"attributes":{"19":13}}]', '[{"id":19,"code":"pizza-size","admin_name":"Pizza - Size","description":null,"image":null,"image_id":null,"type":"select","is_required":0,"is_unique":0,"is_filterable":1,"is_configurable":1,"translations":[{"locale":"vn","name":"vn"},{"locale":"en","name":"en"}],"options":[{"id":1,"admin_name":"6 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"6 inches"},{"locale":"en","label":"6 inches"}]},{"id":2,"admin_name":"10 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"10 inches"},{"locale":"en","label":"10 inches"}]},{"id":3,"admin_name":"14 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"14 inches"},{"locale":"en","label":"14 inches"}]},{"id":13,"admin_name":"18 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"18 inches"},{"locale":"en","label":"18 inches"}]},{"id":161,"admin_name":"60 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"60 inches"},{"locale":"en","label":"60 inches"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/19"}}]', NULL, 1, '2019-12-23 07:28:46'),
(8, 248, 'configurable', 'New York Classic', '1', 69000, NULL, NULL, 'Tomato Sauce, Mozzarella Cheese, Romano Cheese, Pepperoni', '1568857656387', '[{"id":45,"name":"Classic Pizza"}]', 'http://cxi.web.beesightsoft.com/assets/media/nyc_6.png', '[{"id":249,"type":"simple","name":"New York Classic 6 inches","status":true,"price":69000,"prices":[],"formated_price":"\\u20ab69,000","sku":"1568349359057","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":9000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":1,"name":"Ham","price":9000,"prices":[]}]}],"attributes":{"19":1}},{"id":250,"type":"simple","name":"New York Classic 10 inches","status":true,"price":159000,"prices":[],"formated_price":"\\u20ab159,000","sku":"1568349397055","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":19000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":1,"name":"Ham","price":19000,"prices":[]}]}],"attributes":{"19":2}},{"id":251,"type":"simple","name":"New York Classic 14 inches","status":true,"price":269000,"prices":[],"formated_price":"\\u20ab269,000","sku":"1568349421352","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":29000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":1,"name":"Ham","price":29000,"prices":[]}]}],"attributes":{"19":3}},{"id":252,"type":"simple","name":"New York Classic 18 inches","status":true,"price":389000,"prices":[],"formated_price":"\\u20ab389,000","sku":"1568349461488","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":49000,"prices":[],"enable_promotion":0,"apply_same_price":0,"addons":[{"id":1,"name":"Ham","price":49000,"prices":[]}]}],"attributes":{"19":13}}]', '[{"id":19,"code":"pizza-size","admin_name":"Pizza - Size","description":null,"image":null,"image_id":null,"type":"select","is_required":0,"is_unique":0,"is_filterable":1,"is_configurable":1,"translations":[{"locale":"vn","name":"vn"},{"locale":"en","name":"en"}],"options":[{"id":1,"admin_name":"6 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"6 inches"},{"locale":"en","label":"6 inches"}]},{"id":2,"admin_name":"10 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"10 inches"},{"locale":"en","label":"10 inches"}]},{"id":3,"admin_name":"14 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"14 inches"},{"locale":"en","label":"14 inches"}]},{"id":13,"admin_name":"18 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"18 inches"},{"locale":"en","label":"18 inches"}]},{"id":161,"admin_name":"60 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"60 inches"},{"locale":"en","label":"60 inches"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/19"}}]', NULL, 45, '2019-09-19 08:47:35'),
(9, 258, 'configurable', 'Dear Darla Original', '1', 119000, NULL, NULL, 'Tomato Sauce, Mozzarella Cheese, Romano Cheese, Pepperoni, Mushroom, Tomato, Onion, Black Olive, Caper, Arugula, Alfalfa Sprouts', '1568857384752', '[{"id":47,"name":"Special Pizza"}]', 'http://cxi.web.beesightsoft.com/assets/media/img-product-deardarlaoriginal.png', '[{"id":259,"type":"simple","name":"Dear Darla Original 10 inches","status":true,"price":119000,"prices":[],"formated_price":"\\u20ab119,000","sku":"1568350020192","addon_types":[],"attributes":{"19":2}},{"id":260,"type":"simple","name":"Dear Darla Original 14 inches","status":true,"price":239000,"prices":[],"formated_price":"\\u20ab239,000","sku":"1568350107919","addon_types":[],"attributes":{"19":3}},{"id":261,"type":"simple","name":"Dear Darla Original 18 inches","status":true,"price":349000,"prices":[],"formated_price":"\\u20ab349,000","sku":"1568350141787","addon_types":[],"attributes":{"19":13}}]', '[{"id":19,"code":"pizza-size","admin_name":"Pizza - Size","description":null,"image":null,"image_id":null,"type":"select","is_required":0,"is_unique":0,"is_filterable":1,"is_configurable":1,"translations":[{"locale":"vn","name":"vn"},{"locale":"en","name":"en"}],"options":[{"id":1,"admin_name":"6 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"6 inches"},{"locale":"en","label":"6 inches"}]},{"id":2,"admin_name":"10 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"10 inches"},{"locale":"en","label":"10 inches"}]},{"id":3,"admin_name":"14 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"14 inches"},{"locale":"en","label":"14 inches"}]},{"id":13,"admin_name":"18 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"18 inches"},{"locale":"en","label":"18 inches"}]},{"id":161,"admin_name":"60 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"60 inches"},{"locale":"en","label":"60 inches"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/19"}}]', NULL, 47, '2019-09-19 08:43:02'),
(10, 262, 'configurable', 'Roasted Garlic & Ricotta', '1', 119000, NULL, NULL, 'Alfredo Sauce, Mozzarella Cheese, Romano Cheese, Roasted Garlic, Cream Cheese, Arugula, Alfalfa Sprouts', '1574222331896', '[{"id":47,"name":"Special Pizza"}]', 'http://cxi.web.beesightsoft.com/assets/media/img-product-deardarlaroastedgarlicandricotta_2.png', '[{"id":263,"type":"simple","name":"Roasted Garlic & Ricotta 10 inches","status":true,"price":119000,"prices":[],"formated_price":"\\u20ab119,000","sku":"1568350297042","addon_types":[],"attributes":{"19":2}},{"id":264,"type":"simple","name":"Roasted Garlic & Ricotta 14 inches","status":true,"price":239000,"prices":[],"formated_price":"\\u20ab239,000","sku":"1568350328949","addon_types":[],"attributes":{"19":3}},{"id":265,"type":"simple","name":"Roasted Garlic & Ricotta 18 inches","status":true,"price":349000,"prices":[],"formated_price":"\\u20ab349,000","sku":"1568350343448","addon_types":[],"attributes":{"19":13}}]', '[{"id":19,"code":"pizza-size","admin_name":"Pizza - Size","description":null,"image":null,"image_id":null,"type":"select","is_required":0,"is_unique":0,"is_filterable":1,"is_configurable":1,"translations":[{"locale":"vn","name":"vn"},{"locale":"en","name":"en"}],"options":[{"id":1,"admin_name":"6 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"6 inches"},{"locale":"en","label":"6 inches"}]},{"id":2,"admin_name":"10 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"10 inches"},{"locale":"en","label":"10 inches"}]},{"id":3,"admin_name":"14 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"14 inches"},{"locale":"en","label":"14 inches"}]},{"id":13,"admin_name":"18 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"18 inches"},{"locale":"en","label":"18 inches"}]},{"id":161,"admin_name":"60 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"60 inches"},{"locale":"en","label":"60 inches"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/19"}}]', NULL, 47, '2019-11-20 10:58:48'),
(11, 266, 'simple', 'Charlie Chan Chicken', '1', 99000, NULL, NULL, 'Spaghetti, Spicy Asian Sauce, Chicken, Mushroom, Roasted Peanut, Green Onion', '1570000322475', '[{"id":49,"name":"Pasta - VND 99,000"}]', 'http://cxi.web.beesightsoft.com/assets/media/cccp-1.png', '[]', NULL, NULL, 49, '2019-10-02 14:12:01'),
(12, 267, 'simple', 'Chicken Alfredo', '1', 99000, NULL, NULL, 'Spaghetti, Alfredo Sauce, Chicken, Olive, Basil', '1570000354037', '[{"id":49,"name":"Pasta - VND 99,000"}]', 'http://cxi.web.beesightsoft.com/assets/media/cha-1.png', '[]', NULL, NULL, 49, '2019-10-02 14:12:33'),
(13, 268, 'simple', 'Spaghetti and Meatballs', '1', 99000, NULL, NULL, 'Spaghetti, Tomato Sauce, Meatball, Romano Cheese', '1570000427911', '[{"id":49,"name":"Pasta - VND 99,000"}]', 'http://cxi.web.beesightsoft.com/assets/media/smb-1_3.png', '[]', NULL, NULL, 49, '2019-10-02 14:13:47'),
(14, 269, 'simple', 'Shrimp Aglio Olio', '1', 99000, NULL, NULL, 'Spaghetti, Shrimp, Olive Oil, Basil, Sundried Tomato, Garlic', '1570000407038', '[{"id":49,"name":"Pasta - VND 99,000"}]', 'http://cxi.web.beesightsoft.com/assets/media/olio.png', '[]', NULL, NULL, 49, '2019-10-02 14:13:26'),
(15, 270, 'simple', 'Seafood', '1', 99000, NULL, NULL, 'Spaghetti, Alfredo Sauce, Mussel, Shrimp, Black olive, Basil', '1570000384273', '[{"id":49,"name":"Pasta - VND 99,000"}]', 'http://cxi.web.beesightsoft.com/assets/media/seafood-pasta-1_6.png', '[]', NULL, NULL, 49, '2019-10-02 14:13:03'),
(16, 275, 'configurable', 'Hot sauce', '1', 69000, NULL, NULL, 'Chicken Wings with Spicy Chicken Sauce', '1568358158332', '[{"id":29,"name":"Chicken"}]', 'http://cxi.web.beesightsoft.com/assets/media/1_27.png', '[{"id":276,"type":"simple","name":"Hot sauce 4 pieces","status":true,"price":69000,"prices":[],"formated_price":"\\u20ab69,000","sku":"1568358093639","addon_types":[],"attributes":{"20":5}},{"id":277,"type":"simple","name":"Hot sauce 6 pieces","status":true,"price":89000,"prices":[],"formated_price":"\\u20ab89,000","sku":"1568358126793","addon_types":[],"attributes":{"20":6}},{"id":278,"type":"simple","name":"Hot sauce 8 pieces","status":true,"price":119000,"prices":[],"formated_price":"\\u20ab119,000","sku":"1568358143216","addon_types":[],"attributes":{"20":7}}]', '[{"id":20,"code":"Chicken - Piece","admin_name":"Chicken - Piece","description":null,"image":null,"image_id":null,"type":"select","is_required":0,"is_unique":0,"is_filterable":1,"is_configurable":1,"translations":[{"locale":"vn","name":"Chicken - Piece"},{"locale":"en","name":"Chicken - Piece"}],"options":[{"id":5,"admin_name":"4 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"4 mi\\u1ebfng"},{"locale":"en","label":"4 pieces"}]},{"id":6,"admin_name":"6 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"6 mi\\u1ebfng"},{"locale":"en","label":"6 pieces"}]},{"id":7,"admin_name":"8 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"8 mi\\u1ebfng"},{"locale":"en","label":"8 pieces"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/20"}}]', NULL, 29, '2019-09-13 14:02:36'),
(17, 279, 'configurable', 'Garlic Parmesan', '1', 69000, NULL, NULL, 'Chicken Wings with Garlic Butter, Parmesan Cheese and Dried Basil', '1568358295035', '[{"id":29,"name":"Chicken"}]', 'http://cxi.web.beesightsoft.com/assets/media/2_6.png', '[{"id":280,"type":"simple","name":"Garlic Parmesan 4 pieces","status":true,"price":69000,"prices":[],"formated_price":"\\u20ab69,000","sku":"1568358264339","addon_types":[],"attributes":{"20":5}},{"id":281,"type":"simple","name":"Garlic Parmesan 6 pieces","status":true,"price":89000,"prices":[],"formated_price":"\\u20ab89,000","sku":"1568358276135","addon_types":[],"attributes":{"20":6}},{"id":282,"type":"simple","name":"Garlic Parmesan 8 pieces","status":true,"price":119000,"prices":[],"formated_price":"\\u20ab119,000","sku":"1568358283056","addon_types":[],"attributes":{"20":7}}]', '[{"id":20,"code":"Chicken - Piece","admin_name":"Chicken - Piece","description":null,"image":null,"image_id":null,"type":"select","is_required":0,"is_unique":0,"is_filterable":1,"is_configurable":1,"translations":[{"locale":"vn","name":"Chicken - Piece"},{"locale":"en","name":"Chicken - Piece"}],"options":[{"id":5,"admin_name":"4 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"4 mi\\u1ebfng"},{"locale":"en","label":"4 pieces"}]},{"id":6,"admin_name":"6 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"6 mi\\u1ebfng"},{"locale":"en","label":"6 pieces"}]},{"id":7,"admin_name":"8 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"8 mi\\u1ebfng"},{"locale":"en","label":"8 pieces"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/20"}}]', NULL, 29, '2019-09-13 14:04:53'),
(18, 283, 'configurable', 'BBQ Chicken Wings', '1', 69000, NULL, NULL, 'Chicken Wings with Hickory Barbecue Sauce', '1568358434322', '[{"id":29,"name":"Chicken"}]', 'http://cxi.web.beesightsoft.com/assets/media/4_11.png', '[{"id":284,"type":"simple","name":"BBQ Chicken Wings 4 pieces","status":true,"price":69000,"prices":[],"formated_price":"\\u20ab69,000","sku":"1568358396802","addon_types":[],"attributes":{"20":5}},{"id":285,"type":"simple","name":"BBQ Chicken Wings 6 pieces","status":true,"price":89000,"prices":[],"formated_price":"\\u20ab89,000","sku":"1568358410055","addon_types":[],"attributes":{"20":6}},{"id":286,"type":"simple","name":"BBQ Chicken Wings 8 pieces","status":true,"price":119000,"prices":[],"formated_price":"\\u20ab119,000","sku":"1568358420928","addon_types":[],"attributes":{"20":7}}]', '[{"id":20,"code":"Chicken - Piece","admin_name":"Chicken - Piece","description":null,"image":null,"image_id":null,"type":"select","is_required":0,"is_unique":0,"is_filterable":1,"is_configurable":1,"translations":[{"locale":"vn","name":"Chicken - Piece"},{"locale":"en","name":"Chicken - Piece"}],"options":[{"id":5,"admin_name":"4 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"4 mi\\u1ebfng"},{"locale":"en","label":"4 pieces"}]},{"id":6,"admin_name":"6 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"6 mi\\u1ebfng"},{"locale":"en","label":"6 pieces"}]},{"id":7,"admin_name":"8 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"8 mi\\u1ebfng"},{"locale":"en","label":"8 pieces"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/20"}}]', NULL, 29, '2019-12-11 10:00:59'),
(19, 287, 'configurable', 'Honey Garlic', '1', 69000, NULL, NULL, 'Chicken Wings with Honey Garlic Sauce', '1568858798942', '[{"id":29,"name":"Chicken"}]', 'http://cxi.web.beesightsoft.com/assets/media/3.png', '[{"id":288,"type":"simple","name":"Honey Garlic 4 pieces","status":true,"price":69000,"prices":[],"formated_price":"\\u20ab69,000","sku":"1568358828823","addon_types":[],"attributes":{"20":5}},{"id":289,"type":"simple","name":"Honey Garlic 6 pieces","status":true,"price":89000,"prices":[],"formated_price":"\\u20ab89,000","sku":"1568358852735","addon_types":[],"attributes":{"20":6}},{"id":290,"type":"simple","name":"Honey Garlic 8 pieces","status":true,"price":119000,"prices":[],"formated_price":"\\u20ab119,000","sku":"1568358861180","addon_types":[],"attributes":{"20":7}}]', '[{"id":20,"code":"Chicken - Piece","admin_name":"Chicken - Piece","description":null,"image":null,"image_id":null,"type":"select","is_required":0,"is_unique":0,"is_filterable":1,"is_configurable":1,"translations":[{"locale":"vn","name":"Chicken - Piece"},{"locale":"en","name":"Chicken - Piece"}],"options":[{"id":5,"admin_name":"4 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"4 mi\\u1ebfng"},{"locale":"en","label":"4 pieces"}]},{"id":6,"admin_name":"6 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"6 mi\\u1ebfng"},{"locale":"en","label":"6 pieces"}]},{"id":7,"admin_name":"8 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"8 mi\\u1ebfng"},{"locale":"en","label":"8 pieces"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/20"}}]', NULL, 29, '2019-09-19 09:06:36'),
(20, 291, 'configurable', 'Mango Habanero', '1', 69000, NULL, NULL, 'Chicken Wings with Mango, Habanero Sauce', '1568359080515', '[{"id":29,"name":"Chicken"}]', 'http://cxi.web.beesightsoft.com/assets/media/5_1.png', '[{"id":292,"type":"simple","name":"Mango Habanero 4 pieces","status":true,"price":69000,"prices":[],"formated_price":"\\u20ab69,000","sku":"1568359045154","addon_types":[],"attributes":{"20":5}},{"id":293,"type":"simple","name":"Mango Habanero 6 pieces","status":true,"price":89000,"prices":[],"formated_price":"\\u20ab89,000","sku":"1568359058326","addon_types":[],"attributes":{"20":6}},{"id":294,"type":"simple","name":"Mango Habanero 8 pieces","status":true,"price":119000,"prices":[],"formated_price":"\\u20ab119,000","sku":"1568359069986","addon_types":[],"attributes":{"20":7}}]', '[{"id":20,"code":"Chicken - Piece","admin_name":"Chicken - Piece","description":null,"image":null,"image_id":null,"type":"select","is_required":0,"is_unique":0,"is_filterable":1,"is_configurable":1,"translations":[{"locale":"vn","name":"Chicken - Piece"},{"locale":"en","name":"Chicken - Piece"}],"options":[{"id":5,"admin_name":"4 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"4 mi\\u1ebfng"},{"locale":"en","label":"4 pieces"}]},{"id":6,"admin_name":"6 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"6 mi\\u1ebfng"},{"locale":"en","label":"6 pieces"}]},{"id":7,"admin_name":"8 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"8 mi\\u1ebfng"},{"locale":"en","label":"8 pieces"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/20"}}]', NULL, 29, '2019-09-13 14:17:58'),
(21, 295, 'configurable', 'Korean BBQ', '1', 69000, NULL, NULL, 'Chicken Wings with Korean Soy Sauce', '1568858827958', '[{"id":29,"name":"Chicken"}]', 'http://cxi.web.beesightsoft.com/assets/media/6_2.png', '[{"id":296,"type":"simple","name":"Korean BBQ 4 pieces","status":true,"price":69000,"prices":[],"formated_price":"\\u20ab69,000","sku":"1568359160284","addon_types":[],"attributes":{"20":5}},{"id":297,"type":"simple","name":"Korean BBQ 6 pieces","status":true,"price":89000,"prices":[],"formated_price":"\\u20ab89,000","sku":"1568359173681","addon_types":[],"attributes":{"20":6}},{"id":298,"type":"simple","name":"Korean BBQ 8 pieces","status":true,"price":119000,"prices":[],"formated_price":"\\u20ab119,000","sku":"1568359181528","addon_types":[],"attributes":{"20":7}}]', '[{"id":20,"code":"Chicken - Piece","admin_name":"Chicken - Piece","description":null,"image":null,"image_id":null,"type":"select","is_required":0,"is_unique":0,"is_filterable":1,"is_configurable":1,"translations":[{"locale":"vn","name":"Chicken - Piece"},{"locale":"en","name":"Chicken - Piece"}],"options":[{"id":5,"admin_name":"4 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"4 mi\\u1ebfng"},{"locale":"en","label":"4 pieces"}]},{"id":6,"admin_name":"6 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"6 mi\\u1ebfng"},{"locale":"en","label":"6 pieces"}]},{"id":7,"admin_name":"8 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"8 mi\\u1ebfng"},{"locale":"en","label":"8 pieces"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/20"}}]', NULL, 29, '2019-09-19 09:07:05'),
(22, 299, 'simple', 'Chicken Garlic Parmesan', '1', 59000, NULL, NULL, 'Chicken nuggets with Garlic Butter, Parmesan Cheese and Dried Basil', '1568864251511', '[{"id":5,"name":"Appetizers"}]', 'http://cxi.web.beesightsoft.com/assets/media/garlic-popers.png', '[]', NULL, NULL, 5, '2019-09-19 10:37:28'),
(23, 300, 'simple', 'Chicken Hot sauce', '1', 59000, NULL, NULL, 'Chicken Nuggets with Spicy Sauce', '1568864140053', '[{"id":29,"name":"Chicken"}]', 'http://cxi.web.beesightsoft.com/assets/media/hot-poppers_5.png', '[]', NULL, NULL, 29, '2019-09-19 10:35:37'),
(24, 301, 'simple', 'Chicken Poppers BBQ', '1', 59000, NULL, NULL, 'Chicken Nuggets with Hickory BBQ Sauce', '1568864236647', '[{"id":5,"name":"Appetizers"}]', 'http://cxi.web.beesightsoft.com/assets/media/bbq-poppers.png', '[]', NULL, NULL, 5, '2019-09-19 10:37:13'),
(25, 302, 'simple', 'Baked Potato Wedges', '1', 59000, NULL, NULL, 'Savory potato wedges baked to perfection', '1568858509487', '[{"id":5,"name":"Appetizers"}]', 'http://cxi.web.beesightsoft.com/assets/media/2_7.png', '[]', NULL, NULL, 5, '2019-09-19 09:01:46'),
(26, 303, 'simple', 'Baked Potato Halves', '1', 59000, NULL, NULL, 'Oven-baked potato halves loaded with mozzarella and romano cheese topped with crispy bacon bits', '1568859391434', '[{"id":5,"name":"Appetizers"}]', 'http://cxi.web.beesightsoft.com/assets/media/1_28.png', '[]', NULL, NULL, 5, '2019-09-19 09:16:28'),
(27, 304, 'simple', 'Garden Salad', '1', 59000, NULL, NULL, 'Salad, broccoli, cucumber, tomatoes, corn, apple, boiled eggs', '1568858738653', '[{"id":5,"name":"Appetizers"}]', 'http://cxi.web.beesightsoft.com/assets/media/salad_5.png', '[]', NULL, NULL, 5, '2019-09-19 09:05:35'),
(28, 305, 'simple', 'Fries', '1', 19000, NULL, NULL, 'Fries', '1568360103786', '[{"id":5,"name":"Appetizers"}]', 'http://cxi.web.beesightsoft.com/assets/media/fries.png', '[]', NULL, NULL, 5, '2019-09-13 14:35:02'),
(29, 306, 'simple', 'Pale Ale', '1', 65000, NULL, NULL, 'AROMA: Floral and Citrus, Notes of Stone Fruit and Lime FLAVOR: Mild Honey, Citrus zest, Lightly Toasted bread, Refreshing, Fairly dry ABV: 6%', '1568360198546', '[{"id":13,"name":"Drinks"}]', 'http://cxi.web.beesightsoft.com/assets/media/pale-ale.png', '[]', NULL, NULL, 13, '2019-09-13 14:36:36'),
(30, 307, 'simple', 'Far East Ipa', '1', 65000, NULL, NULL, 'AROMA: Pine, Citrus, Melon, Passion Fruit, and White Wine FLAVOR: Lemon, Subtle Maltiness, Gooseberry, Light Grapefruit ABV: 6,7%', '1568864297903', '[{"id":13,"name":"Drinks"}]', 'http://cxi.web.beesightsoft.com/assets/media/far-east-ipa.png', '[]', NULL, NULL, 13, '2019-09-19 10:38:15'),
(31, 308, 'simple', 'Saigon Rose', '1', 65000, NULL, NULL, 'Aroma: Fresh Berries, Citrus, Floral Flavor: Raspberry, light maltiness, honey, citrus  ABV: 3%', '1568864410429', '[{"id":13,"name":"Drinks"}]', 'http://cxi.web.beesightsoft.com/assets/media/saigon-rose.png', '[]', NULL, NULL, 13, '2019-09-19 10:40:07'),
(32, 309, 'simple', 'Strong Bow Elderflower', '1', 29000, NULL, NULL, 'Strong Bow Elderflower - 330ml', '1568360392761', '[{"id":13,"name":"Drinks"}]', 'http://cxi.web.beesightsoft.com/assets/media/elder-flower.png', '[]', NULL, NULL, 13, '2019-09-13 14:39:51'),
(33, 310, 'simple', 'Strong Bow Honey', '1', 29000, NULL, NULL, 'Strong Bow Honey - 330ml', '1568859136907', '[{"id":13,"name":"Drinks"}]', 'http://cxi.web.beesightsoft.com/assets/media/honey_5.png', '[]', NULL, NULL, 13, '2019-09-19 09:12:14'),
(34, 311, 'simple', 'Strong Bow Gold Apple', '1', 29000, NULL, NULL, 'Strong Bow Gold Apple - 330ml', '1568360485103', '[{"id":13,"name":"Drinks"}]', 'http://cxi.web.beesightsoft.com/assets/media/gold-apple-1.png', '[]', NULL, NULL, 13, '2019-09-13 14:41:23'),
(35, 312, 'simple', 'Strong Bow Red Berries', '1', 29000, NULL, NULL, 'Strong Bow Red Berries - 330ml', '1569830038846', '[{"id":13,"name":"Drinks"}]', 'http://cxi.web.beesightsoft.com/assets/media/red-berries.png', '[]', NULL, NULL, 13, '2019-09-30 14:53:58'),
(36, 313, 'simple', 'Heineken', '1', 29000, NULL, NULL, 'Heineken - 330ml', '1568859250059', '[{"id":13,"name":"Drinks"}]', 'http://cxi.web.beesightsoft.com/assets/media/img-product-heineken.png', '[]', NULL, NULL, 13, '2019-09-19 09:14:07'),
(37, 314, 'simple', 'Pepsi 1.5L', '1', 38000, NULL, NULL, 'Pepsi 1.5L', '1568859049849', '[{"id":13,"name":"Drinks"}]', 'http://cxi.web.beesightsoft.com/assets/media/pepsi-15l.png', '[]', NULL, NULL, 13, '2019-09-19 09:10:47'),
(38, 315, 'simple', 'Oolong Tea', '1', 19000, NULL, NULL, 'Oolong Tea - 330ml', '1568864369397', '[{"id":13,"name":"Drinks"}]', 'http://cxi.web.beesightsoft.com/assets/media/img-product-tea-plus.png', '[]', NULL, NULL, 13, '2019-09-19 10:39:26'),
(39, 316, 'simple', 'Sting Strawberry', '1', 19000, NULL, NULL, 'Sting Strawberry - 330ml', '1568864551789', '[{"id":13,"name":"Drinks"}]', 'http://cxi.web.beesightsoft.com/assets/media/sting.png', '[]', NULL, NULL, 13, '2019-09-19 10:42:28'),
(40, 317, 'simple', 'Mirinda Organe', '1', 19000, NULL, NULL, 'Mirinda Organe - 330ml', '1568360868157', '[{"id":13,"name":"Drinks"}]', 'http://cxi.web.beesightsoft.com/assets/media/mirinda.png', '[]', NULL, NULL, 13, '2019-09-13 14:47:46'),
(41, 318, 'simple', '7UP', '1', 19000, NULL, NULL, '7UP - 330ml', '1569827206701', '[{"id":13,"name":"Drinks"}]', 'http://cxi.web.beesightsoft.com/assets/media/7up.png', '[]', NULL, NULL, 13, '2019-09-30 14:06:47'),
(42, 319, 'simple', 'Pepsi Light', '1', 19000, NULL, NULL, 'Pepsi Light - 330ml', '1568360997444', '[{"id":13,"name":"Drinks"}]', 'http://cxi.web.beesightsoft.com/assets/media/pepsi-light.png', '[]', NULL, NULL, 13, '2019-09-13 14:49:55'),
(43, 320, 'simple', 'Pepsi', '1', 19000, NULL, NULL, 'Pepsi - 330ml', '1568864392629', '[{"id":13,"name":"Drinks"}]', 'http://cxi.web.beesightsoft.com/assets/media/pepsi.png', '[]', NULL, NULL, 13, '2019-09-19 10:39:49'),
(44, 321, 'simple', 'Aquafina', '', 9000, NULL, NULL, 'Aquafina - 500ml', '1570699766215', '[{"id":13,"name":"Drinks"}]', 'http://cxi.web.beesightsoft.com/assets/media/img-product-aquafina_4.png', '[]', NULL, NULL, 13, '2019-12-10 10:14:17'),
(45, 330, 'simple', 'Teriyaki Wings Rice', '1', 49000, NULL, NULL, 'Jasmine rice served with our favorable teriyaki chicken wings and vegetables ( lettuce, cucumber, tomato)', '1568946398343', '[{"id":50,"name":"Rice - VND 49,000"}]', 'http://cxi.web.beesightsoft.com/assets/media/com-ga_6.png', '[]', NULL, NULL, 50, '2019-09-20 09:26:37'),
(46, 331, 'simple', 'Teriyaki Popper Rice', '1', 49000, NULL, NULL, 'Jasmine rice served with our favorable teriyaki chicken poppers and vegetables ( lettuce, cucumber, tomato)', '1568946529570', '[{"id":50,"name":"Rice - VND 49,000"}]', 'http://cxi.web.beesightsoft.com/assets/media/com-ga-2.png', '[]', NULL, NULL, 50, '2019-09-20 09:28:48') ;
INSERT INTO `wp_bss_product_detail` ( `id`, `product_id`, `type`, `name`, `status`, `price`, `price_type`, `formated_prices`, `description`, `sku`, `categories`, `image`, `variants`, `super_attributes`, `combo`, `categories_id`, `updated_at`) VALUES
(47, 374, 'configurable', 'Chicken 3 flavors', '1', 69000, NULL, NULL, 'Chicken 3 flavors', '1569484791627', '[{"id":29,"name":"Chicken"}]', 'http://cxi.web.beesightsoft.com/assets/media/bbq-wings_12.png', '[{"id":375,"type":"simple","name":"Chicken 3 flavors 1","status":true,"price":69000,"prices":[],"formated_price":"\\u20ab69,000","sku":"1569484651072","addon_types":[],"attributes":{"20":5,"21":14,"22":17}},{"id":376,"type":"simple","name":"Chicken 3 flavors 2","status":true,"price":69000,"prices":[],"formated_price":"\\u20ab69,000","sku":"1569484668385","addon_types":[],"attributes":{"20":5,"21":15,"22":18}},{"id":377,"type":"simple","name":"Chicken 3 flavors 3","status":true,"price":69000,"prices":[],"formated_price":"\\u20ab69,000","sku":"1569484683617","addon_types":[],"attributes":{"20":5,"21":16,"22":19}},{"id":378,"type":"simple","name":"Chicken 3 flavors 4","status":true,"price":79000,"prices":[],"formated_price":"\\u20ab79,000","sku":"1569484700768","addon_types":[],"attributes":{"20":6,"21":14,"22":17}},{"id":379,"type":"simple","name":"Chicken 3 flavors 5","status":true,"price":79000,"prices":[],"formated_price":"\\u20ab79,000","sku":"1569484716008","addon_types":[],"attributes":{"20":6,"21":15,"22":18}},{"id":380,"type":"simple","name":"Chicken 3 flavors 6","status":true,"price":79000,"prices":[],"formated_price":"\\u20ab79,000","sku":"1569484729776","addon_types":[],"attributes":{"20":6,"21":16,"22":19}},{"id":381,"type":"simple","name":"Chicken 3 flavors 7","status":true,"price":89000,"prices":[],"formated_price":"\\u20ab89,000","sku":"1569484747256","addon_types":[],"attributes":{"20":7,"21":14,"22":17}},{"id":382,"type":"simple","name":"Chicken 3 flavors 8","status":true,"price":89000,"prices":[],"formated_price":"\\u20ab89,000","sku":"1569484757664","addon_types":[],"attributes":{"20":7,"21":15,"22":18}},{"id":383,"type":"simple","name":"Chicken 3 flavors 9","status":true,"price":89000,"prices":[],"formated_price":"\\u20ab89,000","sku":"1569484770849","addon_types":[],"attributes":{"20":7,"21":16,"22":19}}]', '[{"id":20,"code":"Chicken - Piece","admin_name":"Chicken - Piece","description":null,"image":null,"image_id":null,"type":"select","is_required":0,"is_unique":0,"is_filterable":1,"is_configurable":1,"translations":[{"locale":"vn","name":"Chicken - Piece"},{"locale":"en","name":"Chicken - Piece"}],"options":[{"id":5,"admin_name":"4 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"4 mi\\u1ebfng"},{"locale":"en","label":"4 pieces"}]},{"id":6,"admin_name":"6 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"6 mi\\u1ebfng"},{"locale":"en","label":"6 pieces"}]},{"id":7,"admin_name":"8 pieces","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"8 mi\\u1ebfng"},{"locale":"en","label":"8 pieces"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/20"}},{"id":21,"code":"Spicy","admin_name":"Spicy","description":null,"image":null,"image_id":null,"type":"select","is_required":0,"is_unique":0,"is_filterable":0,"is_configurable":1,"translations":[{"locale":"vn","name":"Spicy"},{"locale":"en","name":"Spicy"}],"options":[{"id":14,"admin_name":"Normal","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"B\\u00ecnh th\\u01b0\\u1eddng"},{"locale":"en","label":"Normal"}]},{"id":15,"admin_name":"Medium","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"Trung b\\u00ecnh"},{"locale":"en","label":"Medium"}]},{"id":16,"admin_name":"Extreme","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"Si\\u00eau to kh\\u1ed5ng l\\u1ed3"},{"locale":"en","label":"Extreme"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/21"}},{"id":22,"code":"Flavor","admin_name":"Flavor","description":null,"image":null,"image_id":null,"type":"select","is_required":0,"is_unique":0,"is_filterable":0,"is_configurable":1,"translations":[{"locale":"vn","name":"Flavor"},{"locale":"en","name":"Flavor"}],"options":[{"id":17,"admin_name":"Normal","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"B\\u00ecnh th\\u01b0\\u1eddng"},{"locale":"en","label":"Normal"}]},{"id":18,"admin_name":"Cheese","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"Ph\\u00f4 mai"},{"locale":"en","label":"Cheese"}]},{"id":19,"admin_name":"Honey","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"B\\u01a1"},{"locale":"en","label":"Honey"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/22"}}]', NULL, 29, '2019-09-26 14:59:49'),
(48, 409, 'configurable', 'Hawaiian', '1', 69000, NULL, NULL, 'Tomato Sauce, Mozzrella Cheese, Romano Cheese, Ham, Bacon, Pineapple', '1570177572977', '[{"id":45,"name":"Classic Pizza"}]', 'http://cxi.web.beesightsoft.com/assets/media/hwn_9.png', '[{"id":410,"type":"simple","name":"Hawaiian 10 inches","status":true,"price":179000,"prices":[],"formated_price":"\\u20ab179,000","sku":"1569987129235","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":19000,"prices":[],"enable_promotion":1,"apply_same_price":1,"addons":[{"id":15,"name":"Shrimp","price":19000,"prices":[]}]},{"id":15,"name":"Cutting","display_type":"single","price":2,"prices":[],"enable_promotion":1,"apply_same_price":1,"addons":[{"id":16,"name":"Alfalfa","price":2,"prices":[]}]},{"id":16,"name":"Cheese","display_type":"true-false","price":19000,"prices":[],"enable_promotion":1,"apply_same_price":1,"addons":[{"id":9,"name":"Mozzarella Cheese","price":19000,"prices":[]}]}],"attributes":{"19":2}},{"id":411,"type":"simple","name":"Hawaiian 6 inches","status":true,"price":69000,"prices":[],"formated_price":"\\u20ab69,000","sku":"1569987274147","addon_types":[{"id":15,"name":"Cutting","display_type":"single","price":22222,"prices":[],"enable_promotion":1,"apply_same_price":1,"addons":[{"id":13,"name":"Pie-cut","price":22222,"prices":[]},{"id":12,"name":"Square-cut","price":22222,"prices":[]}]}],"attributes":{"19":1}},{"id":412,"type":"simple","name":"Hawaiian 14 inches","status":true,"price":299000,"prices":[],"formated_price":"\\u20ab299,000","sku":"1569987316405","addon_types":[{"id":16,"name":"Cheese","display_type":"true-false","price":29000,"prices":[],"enable_promotion":1,"apply_same_price":1,"addons":[{"id":9,"name":"Mozzarella Cheese","price":29000,"prices":[]}]}],"attributes":{"19":3}},{"id":413,"type":"simple","name":"Hawaiian 18 inches","status":true,"price":489000,"prices":[],"formated_price":"\\u20ab489,000","sku":"1569987405778","addon_types":[{"id":16,"name":"Cheese","display_type":"true-false","price":39000,"prices":[],"enable_promotion":1,"apply_same_price":1,"addons":[{"id":9,"name":"Mozzarella Cheese","price":39000,"prices":[]}]}],"attributes":{"19":13}}]', '[{"id":19,"code":"pizza-size","admin_name":"Pizza - Size","description":null,"image":null,"image_id":null,"type":"select","is_required":0,"is_unique":0,"is_filterable":1,"is_configurable":1,"translations":[{"locale":"vn","name":"vn"},{"locale":"en","name":"en"}],"options":[{"id":1,"admin_name":"6 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"6 inches"},{"locale":"en","label":"6 inches"}]},{"id":2,"admin_name":"10 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"10 inches"},{"locale":"en","label":"10 inches"}]},{"id":3,"admin_name":"14 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"14 inches"},{"locale":"en","label":"14 inches"}]},{"id":13,"admin_name":"18 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"18 inches"},{"locale":"en","label":"18 inches"}]},{"id":161,"admin_name":"60 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"60 inches"},{"locale":"en","label":"60 inches"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/19"}}]', NULL, 45, '2019-10-04 15:26:10'),
(49, 429, 'simple', 'Chicken Burger', '1', 85000, NULL, NULL, 'Bread, Chicken, Tomatoes, Salad, Cheese, Butter', '1570531778252', '[{"id":5,"name":"Appetizers"}]', 'http://cxi.web.beesightsoft.com/assets/media/7.jpg', '[]', NULL, NULL, 5, '2019-10-08 17:49:36'),
(50, 435, 'simple', 'Squid ring', '1', 25000, NULL, NULL, 'Squid Ring, Butter, Cheese', '1570532019371', '[{"id":5,"name":"Appetizers"}]', 'http://cxi.web.beesightsoft.com/assets/media/13.jpg', '[]', NULL, NULL, 5, '2019-10-08 17:53:37'),
(51, 436, 'simple', 'Chicken Garlic Parmesan', '1', 75000, NULL, NULL, 'Chicken nuggets with Garlic Butter, Parmesan Cheese and Dried Basil', '1570532054652', '[{"id":5,"name":"Appetizers"}]', 'http://cxi.web.beesightsoft.com/assets/media/14.jpg', '[]', NULL, NULL, 5, '2019-10-08 17:54:12'),
(52, 437, 'simple', 'Chicken Hot sauce', '1', 95000, NULL, NULL, 'Chicken Nuggets with Spicy Sauce', '1570532122307', '[{"id":54,"name":"Chicken Special"}]', 'http://cxi.web.beesightsoft.com/assets/media/15.jpg', '[]', NULL, NULL, 54, '2019-10-08 17:55:20'),
(53, 438, 'simple', 'Chicken Poppers BBQ', '1', 105000, NULL, NULL, 'Chicken Nuggets with Hickory BBQ Sauce', '1570532211212', '[{"id":29,"name":"Chicken"}]', 'http://cxi.web.beesightsoft.com/assets/media/16.jpg', '[]', NULL, NULL, 29, '2019-10-08 17:56:49'),
(54, 439, 'simple', 'Afritada', '1', 85000, NULL, NULL, 'Spaghetti, Spicy Asian Sauce, Chicken, Mushroom, Roasted Peanut, Green Onion', '1570532243267', '[{"id":29,"name":"Chicken"}]', 'http://cxi.web.beesightsoft.com/assets/media/17.jpg', '[]', NULL, NULL, 29, '2019-10-08 17:57:21'),
(55, 440, 'simple', 'Airline chicken', '1', 75000, NULL, NULL, 'Spaghetti, Alfredo Sauce, Chicken, Olive, Basil', '1570532273635', '[{"id":29,"name":"Chicken"}]', 'http://cxi.web.beesightsoft.com/assets/media/18.jpg', '[]', NULL, NULL, 29, '2019-10-08 17:57:51'),
(56, 441, 'simple', 'Adobo', '1', 65000, NULL, NULL, 'Spaghetti, Tomato Sauce, Meatball, Romano Cheese', '1570532312723', '[{"id":29,"name":"Chicken"}]', 'http://cxi.web.beesightsoft.com/assets/media/19.jpg', '[]', NULL, NULL, 29, '2019-10-08 17:58:30'),
(57, 442, 'simple', 'Andong jjimdak', '1', 85000, NULL, NULL, 'Spaghetti, Pesto Sauce (Fresh Basil, Cashew, Olive Oil), Shrimp, Sundried Tomato', '1570532354707', '[{"id":29,"name":"Chicken"}]', 'http://cxi.web.beesightsoft.com/assets/media/20.jpg', '[]', NULL, NULL, 29, '2019-10-08 17:59:12'),
(58, 444, 'simple', 'Ayam bakar', '1', 85000, NULL, NULL, 'Bread, Chicken, Tomatoes, Salad, Cheese, Butter', '1570532421291', '[{"id":29,"name":"Chicken"}]', 'http://cxi.web.beesightsoft.com/assets/media/22.jpg', '[]', NULL, NULL, 29, '2019-10-08 18:00:19'),
(59, 445, 'simple', 'Ayam betutu', '1', 75000, NULL, NULL, 'Bread, Egg, Tomatoes, Salad, Cheese, Butter', '1570532452820', '[{"id":29,"name":"Chicken"}]', 'http://cxi.web.beesightsoft.com/assets/media/23.jpg', '[]', NULL, NULL, 29, '2019-10-08 18:00:51'),
(60, 446, 'simple', 'Ayam goreng', '1', 65000, NULL, NULL, 'Bread, Egg, Tomatoes, Salad, Cheese, Butter', '1570532488572', '[{"id":29,"name":"Chicken"}]', 'http://cxi.web.beesightsoft.com/assets/media/23_1.jpg', '[]', NULL, NULL, 29, '2019-10-08 18:01:26'),
(61, 447, 'simple', 'Ayam kecap', '1', 55000, NULL, NULL, 'Potatoes, garlics', '1570532523979', '[{"id":29,"name":"Chicken"}]', 'http://cxi.web.beesightsoft.com/assets/media/24.jpg', '[]', NULL, NULL, 29, '2019-10-08 18:02:02'),
(62, 478, 'combo', 'Pizza & Fries', '1', 0, 'fixed', NULL, '01 Pizza Classic 01 Fries 01 Pepsi 330ml', '1577243811998', '[{"id":73,"name":"Combo"}]', 'http://cxi.web.beesightsoft.com/assets/media/combo-1_7.png', '[]', NULL, NULL, 73, '2019-12-25 03:16:50'),
(63, 479, 'combo', 'Our Classic Combo', '1', 108000, 'fixed', NULL, '01 Pizza Classic 10inch 04 pcs chicken wings 01 Fries 01 Pepsi 330ml', '1579512753866', '[{"id":73,"name":"Combo"}]', 'http://cxi.web.beesightsoft.com/assets/media/combo2-classic-1_1.png', '[]', NULL, NULL, 73, '2020-01-20 09:32:33'),
(64, 481, 'combo', 'Group Combo Classicc', '1', 179000, 'fixed', NULL, '01 Pizza Classic 14inch 06 pcs chicken wings 01 Salad 01 Fries 01 1,5L Pepsi', '1577093439846', '[{"id":73,"name":"Combo"}]', 'http://cxi.web.beesightsoft.com/assets/media/combo-3-classic-1_1.png', '[]', NULL, NULL, 73, '2019-12-23 09:30:40'),
(65, 489, 'configurable', 'Four Seasons', '1', 219000, 'fixed', NULL, 'Four flavors in one great pizza! Hawaiian, #4Cheese, Roasted Garlic and Shrimp, New York Classic', '1574334154124', '[{"id":46,"name":"Deluxe Pizza"}]', 'http://cxi.web.beesightsoft.com/assets/media/fs_3.png', '[{"id":490,"type":"simple","name":"Four Seasons 10 inches","status":true,"price":219000,"prices":[],"formated_price":"\\u20ab219,000","sku":"1574333938483","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":19000,"prices":[],"enable_promotion":1,"apply_same_price":1,"addons":[{"id":1,"name":"Ham","price":19000,"prices":[]},{"id":2,"name":"Bacon","price":19000,"prices":[]},{"id":6,"name":"Button Mushroom","price":19000,"prices":[]},{"id":10,"name":"Black olives","price":19000,"prices":[]}]},{"id":15,"name":"Cutting","display_type":"single","price":0,"prices":[],"enable_promotion":1,"apply_same_price":1,"addons":[{"id":12,"name":"Square-cut","price":0,"prices":[]},{"id":13,"name":"Pie-cut","price":0,"prices":[]}]},{"id":16,"name":"Cheese","display_type":"true-false","price":19000,"prices":[],"enable_promotion":1,"apply_same_price":1,"addons":[{"id":9,"name":"Mozzarella Cheese","price":19000,"prices":[]},{"id":17,"name":"Romano cheese","price":19000,"prices":[]}]}],"attributes":{"19":2}},{"id":491,"type":"simple","name":"Four Seasons 14 inches","status":true,"price":349000,"prices":[],"formated_price":"\\u20ab349,000","sku":"1574334044548","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":29000,"prices":[],"enable_promotion":1,"apply_same_price":1,"addons":[{"id":1,"name":"Ham","price":29000,"prices":[]},{"id":2,"name":"Bacon","price":29000,"prices":[]}]},{"id":15,"name":"Cutting","display_type":"single","price":0,"prices":[],"enable_promotion":1,"apply_same_price":1,"addons":[{"id":12,"name":"Square-cut","price":0,"prices":[]},{"id":13,"name":"Pie-cut","price":0,"prices":[]}]},{"id":16,"name":"Cheese","display_type":"true-false","price":29000,"prices":[],"enable_promotion":1,"apply_same_price":1,"addons":[{"id":9,"name":"Mozzarella Cheese","price":29000,"prices":[]},{"id":18,"name":"Cheddar cheese","price":29000,"prices":[]}]}],"attributes":{"19":3}},{"id":492,"type":"simple","name":"Four Seasons 18 inches","status":true,"price":559000,"prices":[],"formated_price":"\\u20ab559,000","sku":"1574334102532","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":39000,"prices":[],"enable_promotion":1,"apply_same_price":1,"addons":[{"id":1,"name":"Ham","price":39000,"prices":[]},{"id":2,"name":"Bacon","price":39000,"prices":[]}]},{"id":15,"name":"Cutting","display_type":"single","price":0,"prices":[],"enable_promotion":1,"apply_same_price":1,"addons":[{"id":12,"name":"Square-cut","price":0,"prices":[]},{"id":13,"name":"Pie-cut","price":0,"prices":[]}]},{"id":16,"name":"Cheese","display_type":"true-false","price":39000,"prices":[],"enable_promotion":1,"apply_same_price":1,"addons":[{"id":9,"name":"Mozzarella Cheese","price":39000,"prices":[]}]}],"attributes":{"19":13}}]', '[{"id":19,"code":"pizza-size","admin_name":"Pizza - Size","description":null,"image":null,"image_id":null,"type":"select","is_required":0,"is_unique":0,"is_filterable":1,"is_configurable":1,"translations":[{"locale":"vn","name":"vn"},{"locale":"en","name":"en"}],"options":[{"id":1,"admin_name":"6 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"6 inches"},{"locale":"en","label":"6 inches"}]},{"id":2,"admin_name":"10 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"10 inches"},{"locale":"en","label":"10 inches"}]},{"id":3,"admin_name":"14 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"14 inches"},{"locale":"en","label":"14 inches"}]},{"id":13,"admin_name":"18 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"18 inches"},{"locale":"en","label":"18 inches"}]},{"id":161,"admin_name":"60 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"60 inches"},{"locale":"en","label":"60 inches"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/19"}}]', NULL, 46, '2019-11-21 18:02:32'),
(66, 509, 'combo', 'test', '1', 101400, 'flexible', NULL, 'Tomato sauce, Mozzarella Cheese, Romano Cheese, Mussel, Shrimp, Squid, Fresh Basil, Olive', '1576828395705', '[{"id":73,"name":"Combo"}]', 'http://cxi.web.beesightsoft.com/assets/media/sba-1_3.png', '[]', NULL, NULL, 73, '2019-12-20 07:53:15'),
(67, 510, 'combo', 'combo', '1', 151500, 'flexible', NULL, 'Tomato sauce, Mozzarella Cheese, Romano Cheese, Mussel, Shrimp, Squid, Fresh Basil, Olive', '1576828427879', '[{"id":73,"name":"Combo"}]', 'http://cxi.web.beesightsoft.com/assets/media/img-product-deardarlaroastedgarlicandricotta-2.png', '[]', NULL, NULL, 73, '2019-12-20 07:53:48'),
(68, 588, 'configurable', 'Quattro', '1', 599000, 'fixed', NULL, 'Choose 4 favorite pizza flavors Apply for 14 and 18 inch Not apply for Dear Darla', '1578462025745', '[{"id":74,"name":"Quattro Pizza"}]', 'http://cxi.web.beesightsoft.com/assets/media/4-4-pizza-1_4.png', '[{"id":589,"type":"simple","name":"18 inches QUATTRO 1","status":true,"price":599000,"prices":[],"formated_price":"\\u20ab599,000","sku":"1577091457844","addon_types":[{"id":13,"name":"Topping","display_type":"multiple","price":29000,"prices":[],"enable_promotion":0,"apply_same_price":1,"addons":[{"id":1,"name":"Ham","price":29000,"prices":[]},{"id":2,"name":"Bacon","price":29000,"prices":[]},{"id":6,"name":"Button Mushroom","price":29000,"prices":[]},{"id":9,"name":"Mozzarella Cheese","price":29000,"prices":[]},{"id":10,"name":"Black olives","price":29000,"prices":[]},{"id":11,"name":"Green bell pepper","price":29000,"prices":[]},{"id":12,"name":"Square-cut","price":29000,"prices":[]},{"id":13,"name":"Pie-cut","price":29000,"prices":[]}]}],"attributes":{"19":13,"29":"40","30":"47","31":"54","32":"57"}},{"id":590,"type":"simple","name":"18 inches QUATTRO 2","status":true,"price":599000,"prices":[],"formated_price":"\\u20ab599,000","sku":"1577091536399","addon_types":[],"attributes":{"19":13,"29":"40","30":"49","31":"56","32":"57"}},{"id":591,"type":"simple","name":"18 inches QUATTRO 3","status":true,"price":599000,"prices":[],"formated_price":"\\u20ab599,000","sku":"1577091560286","addon_types":[],"attributes":{"19":13,"29":"41","30":"48","31":"52","32":"57"}},{"id":592,"type":"simple","name":"18 inches QUATTRO 4","status":true,"price":599000,"prices":[],"formated_price":"\\u20ab599,000","sku":"1577091593255","addon_types":[],"attributes":{"19":13,"29":"41","30":"49","31":"51","32":"62"}},{"id":593,"type":"simple","name":"18 inches QUATTRO 5","status":true,"price":599000,"prices":[],"formated_price":"\\u20ab599,000","sku":"1577091612887","addon_types":[],"attributes":{"19":13,"29":"41","30":"50","31":"52","32":"61"}},{"id":594,"type":"simple","name":"18 inches QUATTRO 6","status":true,"price":599000,"prices":[],"formated_price":"\\u20ab599,000","sku":"1577091830393","addon_types":[],"attributes":{"19":13,"29":"42","30":"46","31":"53","32":"57"}},{"id":595,"type":"simple","name":"18 inches QUATTRO 7","status":true,"price":599000,"prices":[],"formated_price":"\\u20ab599,000","sku":"1577091857657","addon_types":[],"attributes":{"19":13,"29":"42","30":"49","31":"52","32":"62"}},{"id":596,"type":"simple","name":"18 inches QUATTRO 8","status":true,"price":599000,"prices":[],"formated_price":"\\u20ab599,000","sku":"1577091876808","addon_types":[],"attributes":{"19":13,"29":"42","30":"45","31":"56","32":"61"}},{"id":597,"type":"simple","name":"18 inches QUATTRO 9","status":true,"price":599000,"prices":[],"formated_price":"\\u20ab599,000","sku":"1577091909179","addon_types":[],"attributes":{"19":13,"29":"44","30":"46","31":"55","32":"57"}},{"id":598,"type":"simple","name":"18 inches QUATTRO 10","status":true,"price":599000,"prices":[],"formated_price":"\\u20ab599,000","sku":"1577091962978","addon_types":[],"attributes":{"19":13,"29":"44","30":"47","31":"54","32":"61"}},{"id":599,"type":"simple","name":"18 inches QUATTRO 11","status":true,"price":599000,"prices":[],"formated_price":"\\u20ab599,000","sku":"1577091982912","addon_types":[],"attributes":{"19":13,"29":"44","30":"49","31":"51","32":"58"}},{"id":600,"type":"simple","name":"18 inches QUATTRO 12","status":true,"price":599000,"prices":[],"formated_price":"\\u20ab599,000","sku":"1577092008915","addon_types":[],"attributes":{"19":13,"29":"43","30":"50","31":"51","32":"59"}},{"id":601,"type":"simple","name":"18 inches QUATTRO 13","status":true,"price":599000,"prices":[],"formated_price":"\\u20ab599,000","sku":"1577092026544","addon_types":[],"attributes":{"19":13,"29":"43","30":"50","31":"54","32":"57"}},{"id":602,"type":"simple","name":"18 inches QUATTRO 14","status":true,"price":599000,"prices":[],"formated_price":"\\u20ab599,000","sku":"1577092058754","addon_types":[],"attributes":{"19":13,"29":"39","30":"49","31":"54","32":"58"}}]', '[{"id":19,"code":"pizza-size","admin_name":"Pizza - Size","description":null,"image":null,"image_id":null,"type":"select","is_required":0,"is_unique":0,"is_filterable":1,"is_configurable":1,"translations":[{"locale":"vn","name":"vn"},{"locale":"en","name":"en"}],"options":[{"id":1,"admin_name":"6 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"6 inches"},{"locale":"en","label":"6 inches"}]},{"id":2,"admin_name":"10 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"10 inches"},{"locale":"en","label":"10 inches"}]},{"id":3,"admin_name":"14 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"14 inches"},{"locale":"en","label":"14 inches"}]},{"id":13,"admin_name":"18 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"18 inches"},{"locale":"en","label":"18 inches"}]},{"id":161,"admin_name":"60 inches","description":null,"image":null,"image_id":null,"translations":[{"locale":"vn","label":"60 inches"},{"locale":"en","label":"60 inches"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/19"}},{"id":29,"code":"ABC","admin_name":"1ST FLAVOR","description":null,"image":null,"image_id":null,"type":"multiselect","is_required":1,"is_unique":0,"is_filterable":0,"is_configurable":1,"translations":[{"locale":"vn","name":"vn"},{"locale":"en","name":"en"}],"options":[{"id":39,"admin_name":"NEW YORK\'S FINEST","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Green Bell Pepper, Red Bell Pepper, Mushroom, Olive","image":{"small_thumb":"\\/assets\\/media\\/nyf_1578294250_small_thumb.png","medium_thumb":"\\/assets\\/media\\/nyf_1578294250_medium_thumb.png","original_image":"\\/assets\\/media\\/nyf_1578294250.png"},"image_id":1077,"translations":[{"locale":"vn","label":"NEW YORK\'S FINEST"},{"locale":"en","label":"NEW YORK\'S FINEST"}]},{"id":40,"admin_name":"MANHATTAN MEATLOVERS","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Salami","image":{"small_thumb":"\\/assets\\/media\\/mml_1578294257_small_thumb.png","medium_thumb":"\\/assets\\/media\\/mml_1578294257_medium_thumb.png","original_image":"\\/assets\\/media\\/mml_1578294257.png"},"image_id":1078,"translations":[{"locale":"vn","label":"TH\\u1ecaT MANHATTAN"},{"locale":"en","label":"MANHATTAN MEATLOVERS"}]},{"id":41,"admin_name":"PESCATORE","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Mussel, Shrimp, Squid, Fresh Basil, Olive","image":{"small_thumb":"\\/assets\\/media\\/pescatore_1578294265_small_thumb.png","medium_thumb":"\\/assets\\/media\\/pescatore_1578294265_medium_thumb.png","original_image":"\\/assets\\/media\\/pescatore_1578294265.png"},"image_id":1079,"translations":[{"locale":"vn","label":"PESCATORE"},{"locale":"en","label":"PESCATORE"}]},{"id":42,"admin_name":"#4CHEESE","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Cheddar Cheese, Cream Cheese","image":{"small_thumb":"\\/assets\\/media\\/4c_1578294277_small_thumb.png","medium_thumb":"\\/assets\\/media\\/4c_1578294277_medium_thumb.png","original_image":"\\/assets\\/media\\/4c_1578294277.png"},"image_id":1080,"translations":[{"locale":"vn","label":"#4PH\\u00d4MAI"},{"locale":"en","label":"#4CHEESE"}]},{"id":43,"admin_name":"HAWAIIAN","description":"Tomato Sauce, Mozzrella Cheese, Romano Cheese, Ham, Bacon, Pineapple","image":{"small_thumb":"\\/assets\\/media\\/hwn_1578294285_small_thumb.png","medium_thumb":"\\/assets\\/media\\/hwn_1578294285_medium_thumb.png","original_image":"\\/assets\\/media\\/hwn_1578294285.png"},"image_id":1081,"translations":[{"locale":"vn","label":"HAWAIIAN"},{"locale":"en","label":"HAWAIIAN"}]},{"id":44,"admin_name":"NY CLASSIC","description":"Tomato Sauce, Mozzarella Cheese, Romano Cheese, Pepperoni","image":{"small_thumb":"\\/assets\\/media\\/nyc_1578294292_small_thumb.png","medium_thumb":"\\/assets\\/media\\/nyc_1578294292_medium_thumb.png","original_image":"\\/assets\\/media\\/nyc_1578294292.png"},"image_id":1082,"translations":[{"locale":"vn","label":"NY CLASSIC"},{"locale":"en","label":"NY CLASSIC"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/29"}},{"id":30,"code":"5678","admin_name":"2ND FLAVOR","description":null,"image":null,"image_id":null,"type":"multiselect","is_required":1,"is_unique":0,"is_filterable":0,"is_configurable":1,"translations":[{"locale":"vn","name":"vn"},{"locale":"en","name":"en"}],"options":[{"id":45,"admin_name":"NEW YORK\'S FINEST","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Green Bell Pepper, Red Bell Pepper, Mushroom, Olive","image":{"small_thumb":"\\/assets\\/media\\/nyf_1578294371_small_thumb.png","medium_thumb":"\\/assets\\/media\\/nyf_1578294371_medium_thumb.png","original_image":"\\/assets\\/media\\/nyf_1578294371.png"},"image_id":1083,"translations":[{"locale":"vn","label":"NEW YORK\'S FINEST"},{"locale":"en","label":"NEW YORK\'S FINEST"}]},{"id":46,"admin_name":"MANHATTAN MEATLOVERS","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Salami","image":{"small_thumb":"\\/assets\\/media\\/mml_1578294377_small_thumb.png","medium_thumb":"\\/assets\\/media\\/mml_1578294377_medium_thumb.png","original_image":"\\/assets\\/media\\/mml_1578294377.png"},"image_id":1084,"translations":[{"locale":"vn","label":"TH\\u1ecaT MANHATTAN"},{"locale":"en","label":"MANHATTAN MEATLOVERS"}]},{"id":47,"admin_name":"PESCATORE","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Mussel, Shrimp, Squid, Fresh Basil, Olive","image":{"small_thumb":"\\/assets\\/media\\/pescatore_1578294384_small_thumb.png","medium_thumb":"\\/assets\\/media\\/pescatore_1578294384_medium_thumb.png","original_image":"\\/assets\\/media\\/pescatore_1578294384.png"},"image_id":1085,"translations":[{"locale":"vn","label":"PESCATORE"},{"locale":"en","label":"PESCATORE"}]},{"id":48,"admin_name":"#4CHEESE","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Cheddar Cheese, Cream Cheese","image":{"small_thumb":"\\/assets\\/media\\/4c_1578294389_small_thumb.png","medium_thumb":"\\/assets\\/media\\/4c_1578294389_medium_thumb.png","original_image":"\\/assets\\/media\\/4c_1578294389.png"},"image_id":1086,"translations":[{"locale":"vn","label":"#4PH\\u00d4MAI"},{"locale":"en","label":"#4CHEESE"}]},{"id":49,"admin_name":"HAWAIIAN","description":"Tomato Sauce, Mozzrella Cheese, Romano Cheese, Ham, Bacon, Pineapple","image":{"small_thumb":"\\/assets\\/media\\/hwn_1578294399_small_thumb.png","medium_thumb":"\\/assets\\/media\\/hwn_1578294399_medium_thumb.png","original_image":"\\/assets\\/media\\/hwn_1578294399.png"},"image_id":1087,"translations":[{"locale":"vn","label":"HAWAIIAN"},{"locale":"en","label":"HAWAIIAN"}]},{"id":50,"admin_name":"NY CLASSIC","description":"Tomato Sauce, Mozzarella Cheese, Romano Cheese, Pepperoni","image":{"small_thumb":"\\/assets\\/media\\/nyc_1578294406_small_thumb.png","medium_thumb":"\\/assets\\/media\\/nyc_1578294406_medium_thumb.png","original_image":"\\/assets\\/media\\/nyc_1578294406.png"},"image_id":1088,"translations":[{"locale":"vn","label":"NY CLASSIC"},{"locale":"en","label":"NY CLASSIC"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/30"}},{"id":31,"code":"yzk","admin_name":"3RD FLAVOR","description":null,"image":null,"image_id":null,"type":"multiselect","is_required":1,"is_unique":0,"is_filterable":0,"is_configurable":1,"translations":[{"locale":"vn","name":"vn"},{"locale":"en","name":"en"}],"options":[{"id":51,"admin_name":"NEW YORK\'S FINEST","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Green Bell Pepper, Red Bell Pepper, Mushroom, Olive","image":{"small_thumb":"\\/assets\\/media\\/nyf_1578294470_small_thumb.png","medium_thumb":"\\/assets\\/media\\/nyf_1578294470_medium_thumb.png","original_image":"\\/assets\\/media\\/nyf_1578294470.png"},"image_id":1089,"translations":[{"locale":"vn","label":"NEW YORK\'S FINEST"},{"locale":"en","label":"NEW YORK\'S FINEST"}]},{"id":52,"admin_name":"MANHATTAN MEATLOVERS","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Salami","image":{"small_thumb":"\\/assets\\/media\\/mml_1578294477_small_thumb.png","medium_thumb":"\\/assets\\/media\\/mml_1578294477_medium_thumb.png","original_image":"\\/assets\\/media\\/mml_1578294477.png"},"image_id":1090,"translations":[{"locale":"vn","label":"MANHATTAN MEATLOVERS"},{"locale":"en","label":"MANHATTAN MEATLOVERS"}]},{"id":53,"admin_name":"PESCATORE","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Mussel, Shrimp, Squid, Fresh Basil, Olive","image":{"small_thumb":"\\/assets\\/media\\/pescatore_1578294485_small_thumb.png","medium_thumb":"\\/assets\\/media\\/pescatore_1578294485_medium_thumb.png","original_image":"\\/assets\\/media\\/pescatore_1578294485.png"},"image_id":1091,"translations":[{"locale":"vn","label":"PESCATORE"},{"locale":"en","label":"PESCATORE"}]},{"id":54,"admin_name":"#4CHEESE","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Cheddar Cheese, Cream Cheese","image":{"small_thumb":"\\/assets\\/media\\/4c_1578294490_small_thumb.png","medium_thumb":"\\/assets\\/media\\/4c_1578294490_medium_thumb.png","original_image":"\\/assets\\/media\\/4c_1578294490.png"},"image_id":1092,"translations":[{"locale":"vn","label":"#4PH\\u00d4MAI"},{"locale":"en","label":"#4CHEESE"}]},{"id":55,"admin_name":"HAWAIIAN","description":"Tomato Sauce, Mozzrella Cheese, Romano Cheese, Ham, Bacon, Pineapple","image":{"small_thumb":"\\/assets\\/media\\/hwn_1578294496_small_thumb.png","medium_thumb":"\\/assets\\/media\\/hwn_1578294496_medium_thumb.png","original_image":"\\/assets\\/media\\/hwn_1578294496.png"},"image_id":1093,"translations":[{"locale":"vn","label":"HAWAIIAN"},{"locale":"en","label":"HAWAIIAN"}]},{"id":56,"admin_name":"NY CLASSIC","description":"Tomato Sauce, Mozzarella Cheese, Romano Cheese, Pepperoni","image":{"small_thumb":"\\/assets\\/media\\/nyc_1578294504_small_thumb.png","medium_thumb":"\\/assets\\/media\\/nyc_1578294504_medium_thumb.png","original_image":"\\/assets\\/media\\/nyc_1578294504.png"},"image_id":1094,"translations":[{"locale":"vn","label":"NY CLASSIC"},{"locale":"en","label":"NY CLASSIC"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/31"}},{"id":32,"code":"opp","admin_name":"4TH FLAVOR","description":null,"image":null,"image_id":null,"type":"multiselect","is_required":1,"is_unique":0,"is_filterable":0,"is_configurable":1,"translations":[{"locale":"vn","name":"vn"},{"locale":"en","name":"en"}],"options":[{"id":57,"admin_name":"NEW YORK\'S FINEST","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Green Bell Pepper, Red Bell Pepper, Mushroom, Olive","image":{"small_thumb":"\\/assets\\/media\\/nyf_1578294578_small_thumb.png","medium_thumb":"\\/assets\\/media\\/nyf_1578294578_medium_thumb.png","original_image":"\\/assets\\/media\\/nyf_1578294578.png"},"image_id":1095,"translations":[{"locale":"vn","label":"NEW YORK\'S FINEST"},{"locale":"en","label":"NEW YORK\'S FINEST"}]},{"id":58,"admin_name":"MANHATTAN MEATLOVERS","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Bacon, Ground Beef, Ham, Italian Sausage, Pepperoni, Salami","image":{"small_thumb":"\\/assets\\/media\\/mml_1578294585_small_thumb.png","medium_thumb":"\\/assets\\/media\\/mml_1578294585_medium_thumb.png","original_image":"\\/assets\\/media\\/mml_1578294585.png"},"image_id":1096,"translations":[{"locale":"vn","label":"TH\\u1ecaT MANHATTAN"},{"locale":"en","label":"MANHATTAN MEATLOVERS"}]},{"id":59,"admin_name":"PESCATORE","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Mussel, Shrimp, Squid, Fresh Basil, Olive","image":{"small_thumb":"\\/assets\\/media\\/pescatore_1578294590_small_thumb.png","medium_thumb":"\\/assets\\/media\\/pescatore_1578294590_medium_thumb.png","original_image":"\\/assets\\/media\\/pescatore_1578294590.png"},"image_id":1097,"translations":[{"locale":"vn","label":"PESCATORE"},{"locale":"en","label":"PESCATORE"}]},{"id":60,"admin_name":"#4CHEESE","description":"Tomato sauce, Mozzarella Cheese, Romano Cheese, Cheddar Cheese, Cream Cheese","image":{"small_thumb":"\\/assets\\/media\\/4c_1578294595_small_thumb.png","medium_thumb":"\\/assets\\/media\\/4c_1578294595_medium_thumb.png","original_image":"\\/assets\\/media\\/4c_1578294595.png"},"image_id":1098,"translations":[{"locale":"vn","label":"#4PH\\u00d4MAI"},{"locale":"en","label":"#4CHEESE"}]},{"id":61,"admin_name":"HAWAIIAN","description":"Tomato Sauce, Mozzrella Cheese, Romano Cheese, Ham, Bacon, Pineapple","image":{"small_thumb":"\\/assets\\/media\\/hwn_1578294604_small_thumb.png","medium_thumb":"\\/assets\\/media\\/hwn_1578294604_medium_thumb.png","original_image":"\\/assets\\/media\\/hwn_1578294604.png"},"image_id":1099,"translations":[{"locale":"vn","label":"HAWAIIAN"},{"locale":"en","label":"HAWAIIAN"}]},{"id":62,"admin_name":"NY CLASSIC","description":"Tomato Sauce, Mozzarella Cheese, Romano Cheese, Pepperoni","image":{"small_thumb":"\\/assets\\/media\\/nyc_1578294611_small_thumb.png","medium_thumb":"\\/assets\\/media\\/nyc_1578294611_medium_thumb.png","original_image":"\\/assets\\/media\\/nyc_1578294611.png"},"image_id":1100,"translations":[{"locale":"vn","label":"NY CLASSIC"},{"locale":"en","label":"NY CLASSIC"}]}],"urls":{"delete_url":"http:\\/api\\/v1\\/admin\\/catalog\\/attributes\\/32"}}]', NULL, 74, '2020-01-08 05:40:25'),
(69, 630, 'simple', 'Sting', '1', 10000, 'fixed', NULL, 'just for testing', '1581318072846', '[{"id":13,"name":"Drinks"}]', 'http://cxi.web.beesightsoft.com', '[]', NULL, NULL, 13, '2020-02-10 07:01:13'),
(70, 631, 'simple', 'Sting', '1', 10000, 'fixed', NULL, 'just for testing', '1581318080114', '[{"id":13,"name":"Drinks"}]', 'http://cxi.web.beesightsoft.com', '[]', NULL, NULL, 13, '2020-02-10 07:01:22'),
(71, 632, 'simple', 'Sting', '1', 10000, 'fixed', NULL, 'just for testing', '1581318171715', '[{"id":13,"name":"Drinks"}]', 'http://cxi.web.beesightsoft.com', '[]', NULL, NULL, 13, '2020-02-10 07:02:52'),
(72, 634, 'simple', 'Sting', '1', 10000, 'fixed', NULL, 'just for testing', '1581318291140', '[{"id":13,"name":"Drinks"}]', 'http://cxi.web.beesightsoft.com/assets/media/download_1581317632.jpg', '[]', NULL, NULL, 13, '2020-02-10 07:04:52') ;

#
# End of data contents of table `wp_bss_product_detail`
# --------------------------------------------------------



#
# Delete any existing table `wp_bss_product_gift`
#

DROP TABLE IF EXISTS `wp_bss_product_gift`;


#
# Table structure of table `wp_bss_product_gift`
#

CREATE TABLE `wp_bss_product_gift` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gift_id` bigint(20) DEFAULT NULL,
  `gift` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enable` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_bss_product_gift`
#
INSERT INTO `wp_bss_product_gift` ( `id`, `gift_id`, `gift`, `image`, `description`, `enable`) VALUES
(1, 2, 'Teddy bear', 'http://cxi.web.beesightsoft.com/assets/media/small-size-plush-teddy-bear-toy-mini-350x350_1.jpg', 'cute Teddy bear', 1),
(2, 4, 'Christmas snow globe', 'http://cxi.web.beesightsoft.com/assets/media/qua-cau-tuyet-co-den-md-1-lemonshop.jpg', 'christmas snow globe', 1),
(3, 18, 'T-shirt', 'http://cxi.web.beesightsoft.com/assets/media/42-1.png', 'plain black shirt', 1),
(4, 19, 'Jogger pants', 'http://cxi.web.beesightsoft.com/assets/media/qd192-061.jpg', 'jogger pants', 1) ;

#
# End of data contents of table `wp_bss_product_gift`
# --------------------------------------------------------



#
# Delete any existing table `wp_bss_product_gift_categories`
#

DROP TABLE IF EXISTS `wp_bss_product_gift_categories`;


#
# Table structure of table `wp_bss_product_gift_categories`
#

CREATE TABLE `wp_bss_product_gift_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gift_categories_id` bigint(20) DEFAULT NULL,
  `gift_categories` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enable` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_bss_product_gift_categories`
#
INSERT INTO `wp_bss_product_gift_categories` ( `id`, `gift_categories_id`, `gift_categories`, `image`, `description`, `enable`) VALUES
(1, 9, 'Souvenir', 'http://cxi.web.beesightsoft.com/assets/media/small-size-plush-teddy-bear-toy-mini-350x350.jpg', 'description', 1),
(2, 10, 'Clothes', 'http://cxi.web.beesightsoft.com/assets/media/combo-2-ao-thun-nam-co-tron-ta-5840-6839-1568278079.png', 'pants, shirt,...', 1) ;

#
# End of data contents of table `wp_bss_product_gift_categories`
# --------------------------------------------------------



#
# Delete any existing table `wp_bss_product_last_update_time`
#

DROP TABLE IF EXISTS `wp_bss_product_last_update_time`;


#
# Table structure of table `wp_bss_product_last_update_time`
#

CREATE TABLE `wp_bss_product_last_update_time` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_addon_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_addon_type_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promotion_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gift_last_update_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gift_categories_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_bss_product_last_update_time`
#
INSERT INTO `wp_bss_product_last_update_time` ( `id`, `product_time`, `categories_time`, `attribute_time`, `product_addon_time`, `product_addon_type_time`, `promotion_time`, `gift_last_update_time`, `gift_categories_time`) VALUES
(1, '2020-02-12 02:36:28', '2020-02-10 06:44:44', '2020-02-10 03:44:11', '2020-02-11 09:26:33', '2020-02-11 10:23:32', '2020-02-10 03:44:20', '2020-02-10 03:44:22', '2020-02-10 03:44:25') ;

#
# End of data contents of table `wp_bss_product_last_update_time`
# --------------------------------------------------------



#
# Delete any existing table `wp_bss_product_promotion`
#

DROP TABLE IF EXISTS `wp_bss_product_promotion`;


#
# Table structure of table `wp_bss_product_promotion`
#

CREATE TABLE `wp_bss_product_promotion` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_promotion_id` bigint(20) DEFAULT NULL,
  `product_promotion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `starts_from` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ends_till` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enable` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_bss_product_promotion`
#
INSERT INTO `wp_bss_product_promotion` ( `id`, `product_promotion_id`, `product_promotion`, `image`, `description`, `starts_from`, `ends_till`, `enable`) VALUES
(1, 89, 'PIZZA PARTY OFF 40%', 'http://cxi.web.beesightsoft.com/assets/media/face-ads-sinh-nhat-ta_1.jpg', 'Discount 40% total bill Apply for receipt from VND 2,000,000. Cannot combined with other promotions. Not apply for Combo. Not apply for gift voucher, discount voucher', '2019-11-01 00:00:00', '2020-12-31 00:00:00', 1),
(2, 91, '40% OFF PIZZA', 'http://cxi.web.beesightsoft.com/assets/media/giam-40-website_5.jpg', 'Discount 40% Pizza only for Wednesday and Thursday. Not apply for Half Half and Quattro Pizza. Cannot combined with other promotions', '2019-11-01 00:00:00', '2020-12-31 00:00:00', 1),
(3, 92, 'BUY 1 GET 1 (GIFT)', 'http://cxi.web.beesightsoft.com/assets/media/mua-1-tang-1-ta-website_1.jpg', 'Buy 1 Pizza get free 1 Gift', '2019-11-01 00:00:00', '2020-12-30 00:00:00', 1),
(4, 124, '50% Off Unconditionally', 'http://cxi.web.beesightsoft.com/assets/media/limited-offer-mega-sale-banner-sale-poster-big-sale-special-offer-discounts-50-off-vector-illustration-for-web-design-and-application-interf-p8ca0f_2.jpg', '50% Off Unconditionally', '2019-12-24 00:00:00', '2020-12-31 00:00:00', 1),
(5, 160, 'BUY 1 GET 1 (PRODUCT)', 'http://cxi.web.beesightsoft.com/assets/media/buy-1-get-1-free-banner-105164-423_1578298665.jpg', 'Buy 1 Pizza get free 1 Product', '2020-01-01 00:00:00', '2020-02-29 00:00:00', 1),
(6, 163, 'Except Product', 'http://cxi.web.beesightsoft.com', 'Not apply "Half-half" and "Quattro" for this promotion', '2020-01-07 00:00:00', '2020-01-31 00:00:00', 1),
(7, 165, 'Single Price', 'http://cxi.web.beesightsoft.com', 'aaaaaaaaa', '2020-01-09 00:00:00', '2020-01-31 00:00:00', 1) ;

#
# End of data contents of table `wp_bss_product_promotion`
# --------------------------------------------------------



#
# Delete any existing table `wp_commentmeta`
#

DROP TABLE IF EXISTS `wp_commentmeta`;


#
# Table structure of table `wp_commentmeta`
#

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_commentmeta`
#

#
# End of data contents of table `wp_commentmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_comments`
#

DROP TABLE IF EXISTS `wp_comments`;


#
# Table structure of table `wp_comments`
#

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT 0,
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT 0,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_comments`
#
INSERT INTO `wp_comments` ( `comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-12-26 07:38:07', '2019-12-26 07:38:07', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0) ;

#
# End of data contents of table `wp_comments`
# --------------------------------------------------------



#
# Delete any existing table `wp_links`
#

DROP TABLE IF EXISTS `wp_links`;


#
# Table structure of table `wp_links`
#

CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_links`
#

#
# End of data contents of table `wp_links`
# --------------------------------------------------------



#
# Delete any existing table `wp_options`
#

DROP TABLE IF EXISTS `wp_options`;


#
# Table structure of table `wp_options`
#

CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`),
  KEY `autoload` (`autoload`)
) ENGINE=InnoDB AUTO_INCREMENT=552 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_options`
#
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://192.168.1.145/demoaddtheme', 'yes'),
(2, 'home', 'http://192.168.1.145/demoaddtheme', 'yes'),
(3, 'blogname', 'BeeSight Soft Theme', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'phuong.luong@beesightsoft.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:89:{s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:58:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:68:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:88:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:64:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:53:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$";s:91:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$";s:85:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1";s:77:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:65:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]";s:61:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]";s:47:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:57:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:77:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:53:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]";s:51:"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]";s:38:"([0-9]{4})/comment-page-([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&cpage=$matches[2]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:2:{i:0;s:35:"redux-framework/redux-framework.php";i:1;s:31:"wp-migrate-db/wp-migrate-db.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'bss_theme', 'yes'),
(41, 'stylesheet', 'bss_theme', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '45805', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'admin_email_lifespan', '1592897883', 'yes'),
(94, 'initial_db_version', '45805', 'yes'),
(95, 'wp_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:61:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(96, 'fresh_site', '0', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes') ;
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:5:{s:19:"wp_inactive_widgets";a:0:{}s:8:"footer_1";a:0:{}s:8:"footer_2";a:0:{}s:7:"sidebar";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:13:"array_version";i:3;}', 'yes'),
(103, 'cron', 'a:6:{i:1581478689;a:1:{s:34:"wp_privacy_delete_old_export_files";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"hourly";s:4:"args";a:0:{}s:8:"interval";i:3600;}}}i:1581493088;a:1:{s:32:"recovery_mode_clean_expired_keys";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1581493089;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1581493102;a:2:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}s:25:"delete_expired_transients";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1581493104;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(104, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(105, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(106, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(107, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(108, 'widget_media_gallery', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(109, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(110, 'nonce_key', 'N_6N0*[xAq@a(d@aFi4?FU.BDP[EhHjF0xM-x8w#QI`h!II$yq$m)gZT}VTyUls!', 'no'),
(111, 'nonce_salt', 'n~|R =eWjiA|aC+;4z%1ApH-);8;bV&cb-uS^QN`%Y%z&a)#`A-W]*NCEd#!/D%Y', 'no'),
(112, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(113, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(114, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(116, 'recovery_keys', 'a:0:{}', 'yes'),
(118, 'theme_mods_twentytwenty', 'a:2:{s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1577345918;s:4:"data";a:3:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:3:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";}s:9:"sidebar-2";a:3:{i:0;s:10:"archives-2";i:1;s:12:"categories-2";i:2;s:6:"meta-2";}}}}', 'yes'),
(124, 'logged_in_key', 'g.P<|QLO.KRT~:I~J<&ktb&e3P)5uPGNX`7kdEt tOyfk.(]E`enYcmD+j4h9&S>', 'no'),
(125, 'logged_in_salt', '4suPr;>#o@1(6K47O^0b@pDU@nqDl$/|7AOClj[zJe[LkF3Zto.{AN0&1v.DmQ_1', 'no'),
(126, 'auth_key', '0F*x~2&W|5^,z$^DK98x/KQPo.;41lOT2=F[v}qn%QU_V%(7Y7B%A}:1Qo9[[6I8', 'no'),
(127, 'auth_salt', 'Oo@p=NdU^>J}I{d2hfu+[|L#,6gP@Dd:Hhc/G&C6&sl][ZNi3|A^VlEQ?(3{:+kM', 'no'),
(135, 'can_compress_scripts', '1', 'no'),
(146, 'current_theme', 'Bss Theme', 'yes'),
(147, 'theme_mods_bss_theme', 'a:3:{i:0;b:0;s:18:"nav_menu_locations";a:0:{}s:18:"custom_css_post_id";i:-1;}', 'yes'),
(148, 'theme_switched', '', 'yes'),
(149, 'bss_theme_installed', '1', 'yes'),
(152, 'redux_version_upgraded_from', '3.6.16', 'yes'),
(154, 'bss_options', 'a:17:{s:8:"last_tab";s:1:"0";s:7:"logo-on";s:0:"";s:10:"logo-image";a:9:{s:3:"url";s:0:"";s:2:"id";s:0:"";s:6:"height";s:0:"";s:5:"width";s:0:"";s:9:"thumbnail";s:0:"";s:5:"title";s:0:"";s:7:"caption";s:0:"";s:3:"alt";s:0:"";s:11:"description";s:0:"";}s:14:"logo-on-mobile";s:0:"";s:17:"logo-image-mobile";a:9:{s:3:"url";s:0:"";s:2:"id";s:0:"";s:6:"height";s:0:"";s:5:"width";s:0:"";s:9:"thumbnail";s:0:"";s:5:"title";s:0:"";s:7:"caption";s:0:"";s:3:"alt";s:0:"";s:11:"description";s:0:"";}s:14:"logo-on-footer";s:0:"";s:17:"logo-image-footer";a:9:{s:3:"url";s:0:"";s:2:"id";s:0:"";s:6:"height";s:0:"";s:5:"width";s:0:"";s:9:"thumbnail";s:0:"";s:5:"title";s:0:"";s:7:"caption";s:0:"";s:3:"alt";s:0:"";s:11:"description";s:0:"";}s:21:"logo-on-footer-mobile";s:0:"";s:9:"copyright";s:0:"";s:11:"API_CXI_URL";s:38:"http://cxi.web.beesightsoft.com/api/v1";s:7:"CXI_URL";s:31:"http://cxi.web.beesightsoft.com";s:17:"API_CXI_USER_NAME";s:19:"ba@beesightsoft.com";s:11:"API_CXI_PWD";s:8:"admin123";s:13:"API_CXI_TOKEN";s:1079:"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImViNzBhYzFmZTllNGQzM2MyYzI1ZjA4YmQ1NTZlY2UyMTVjNGNkNjgxZDNjMzVmMWI2NjIwZTAzMDQ3MWNkMDEzYmM3NDQzYWM2MzViZjYwIn0.eyJhdWQiOiIxIiwianRpIjoiZWI3MGFjMWZlOWU0ZDMzYzJjMjVmMDhiZDU1NmVjZTIxNWM0Y2Q2ODFkM2MzNWYxYjY2MjBlMDMwNDcxY2QwMTNiYzc0NDNhYzYzNWJmNjAiLCJpYXQiOjE1ODAzNTUwMjUsIm5iZiI6MTU4MDM1NTAyNSwiZXhwIjoxNTgxNjUxMDI1LCJzdWIiOiIyNSIsInNjb3BlcyI6W119.jM4n1OSYaFYFK7RH3NarkuB0oCmXZmC71jp1sU847lSPpI72_tbqQ0ongUFnt7GREG78HoxOn7rE-Lxn5u9F7HweF_EiR-LXskss_GtAcyEPEzJrZE1m96yhwGFWBZLMUjHwW_kKTsb8AZ57mkOJJ3xHDahGT-f9WA0yW4FX7eEi0_b-hU0t5xa2qxVy2xqBwnyPQkfOMUN5UjK2vou4gCwxs4M3k1GTMDl9og7e1HKlkP7GKrgesMIFgM5MV1KQ_5OYdVqyeGFuRKPl94IZ2F7717_rbcheO-oi0VjMlJy9A3ye5umwqkyUPxvLv8DH_ZzDIaJ7UVZ9MVuBy40S6IAcrcy2p6zEWtTHPCHUSELRZJFJK3swyhnpi35iWj1ZrWLFjOrcFy4manAIDKZ3R-CRL-lOiYgVXCqGVfeP6BQDxcQoSyPsu_HU1HFkGBaUHUdmcm2jRSgGVIx8ULNf9cdb7x9fJAfmU7kgZRv0IzMKFTG4usnhOsx04d-t1YZ4TkcBeD19khlBk9zogPSQR1nXwqfp4RWsr8O1IEypI3s2gYS9QSaoYYb_LzEkwVQzWFNbiru6nrkVE_CpLLKsHO1XvaxC0L3w7ezW35_Aw2NjJVmw2SBL6ZBFUN4SzUCADYciVvgZ0Amz7AlA7w3dBMrwwkOLloJ5OhC-Xdwn9h4";s:16:"API_CXI_X_APP_ID";s:32:"c70ccbe93b3b4f8fadbc5d57f008a32f";s:8:"BRAND_ID";s:2:"44";s:15:"ORGANIZATION_ID";s:1:"1";}', 'yes'),
(155, 'bss_options-transients', 'a:2:{s:14:"changed_values";a:0:{}s:9:"last_save";i:1581304117;}', 'yes'),
(165, 'recently_activated', 'a:0:{}', 'yes'),
(174, 'recovery_mode_email_last_sent', '1581304566', 'yes'),
(331, 'WPLANG', '', 'yes'),
(332, 'new_admin_email', 'phuong.luong@beesightsoft.com', 'yes'),
(549, 'wpmdb_usage', 'a:2:{s:6:"action";s:8:"savefile";s:4:"time";i:1581476067;}', 'no') ;

#
# End of data contents of table `wp_options`
# --------------------------------------------------------



#
# Delete any existing table `wp_postmeta`
#

DROP TABLE IF EXISTS `wp_postmeta`;


#
# Table structure of table `wp_postmeta`
#

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_postmeta`
#
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_wp_page_template', 'list_product.php'),
(4, 6, '_wp_page_template', 'single-product.php'),
(5, 7, '_wp_attached_file', '2019/12/logo-1.jpg'),
(6, 7, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:800;s:6:"height";i:799;s:4:"file";s:18:"2019/12/logo-1.jpg";s:5:"sizes";a:3:{s:6:"medium";a:4:{s:4:"file";s:18:"logo-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:18:"logo-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:18:"logo-1-768x767.jpg";s:5:"width";i:768;s:6:"height";i:767;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(7, 8, '_wp_attached_file', '2019/12/2_logo-1.jpg'),
(8, 8, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:800;s:6:"height";i:799;s:4:"file";s:20:"2019/12/2_logo-1.jpg";s:5:"sizes";a:3:{s:6:"medium";a:4:{s:4:"file";s:20:"2_logo-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:20:"2_logo-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"2_logo-1-768x767.jpg";s:5:"width";i:768;s:6:"height";i:767;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(9, 9, '_wp_attached_file', '2019/12/3_logo-1.jpg'),
(10, 9, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:800;s:6:"height";i:799;s:4:"file";s:20:"2019/12/3_logo-1.jpg";s:5:"sizes";a:3:{s:6:"medium";a:4:{s:4:"file";s:20:"3_logo-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:20:"3_logo-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"3_logo-1-768x767.jpg";s:5:"width";i:768;s:6:"height";i:767;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(11, 10, '_wp_attached_file', '2019/12/4_logo-1.jpg'),
(12, 10, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:800;s:6:"height";i:799;s:4:"file";s:20:"2019/12/4_logo-1.jpg";s:5:"sizes";a:3:{s:6:"medium";a:4:{s:4:"file";s:20:"4_logo-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:20:"4_logo-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"4_logo-1-768x767.jpg";s:5:"width";i:768;s:6:"height";i:767;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(13, 11, '_wp_attached_file', '2019/12/5_logo-1.jpg'),
(14, 11, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:800;s:6:"height";i:799;s:4:"file";s:20:"2019/12/5_logo-1.jpg";s:5:"sizes";a:3:{s:6:"medium";a:4:{s:4:"file";s:20:"5_logo-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:20:"5_logo-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"5_logo-1-768x767.jpg";s:5:"width";i:768;s:6:"height";i:767;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(15, 12, '_wp_attached_file', '2019/12/6_logo-1.jpg'),
(16, 12, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:800;s:6:"height";i:799;s:4:"file";s:20:"2019/12/6_logo-1.jpg";s:5:"sizes";a:3:{s:6:"medium";a:4:{s:4:"file";s:20:"6_logo-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:20:"6_logo-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"6_logo-1-768x767.jpg";s:5:"width";i:768;s:6:"height";i:767;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(17, 13, '_wp_attached_file', '2019/12/7_logo-1.jpg'),
(18, 13, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:800;s:6:"height";i:799;s:4:"file";s:20:"2019/12/7_logo-1.jpg";s:5:"sizes";a:3:{s:6:"medium";a:4:{s:4:"file";s:20:"7_logo-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:20:"7_logo-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"7_logo-1-768x767.jpg";s:5:"width";i:768;s:6:"height";i:767;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(19, 14, '_wp_attached_file', '2019/12/elementor-logo.png'),
(20, 14, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:64;s:6:"height";i:64;s:4:"file";s:26:"2019/12/elementor-logo.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(21, 15, '_wp_attached_file', '2019/12/8_logo-1.jpg'),
(22, 15, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:800;s:6:"height";i:799;s:4:"file";s:20:"2019/12/8_logo-1.jpg";s:5:"sizes";a:3:{s:6:"medium";a:4:{s:4:"file";s:20:"8_logo-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:20:"8_logo-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"8_logo-1-768x767.jpg";s:5:"width";i:768;s:6:"height";i:767;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(23, 16, '_edit_lock', '1577350176:1'),
(26, 18, '_edit_lock', '1577350369:1'),
(29, 20, '_wp_attached_file', '2019/12/Image_2.jpg'),
(30, 20, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1200;s:6:"height";i:800;s:4:"file";s:19:"2019/12/Image_2.jpg";s:5:"sizes";a:4:{s:6:"medium";a:4:{s:4:"file";s:19:"Image_2-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"Image_2-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:19:"Image_2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"Image_2-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(31, 21, '_wp_attached_file', '2019/12/Image_1.jpg'),
(32, 21, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1200;s:6:"height";i:800;s:4:"file";s:19:"2019/12/Image_1.jpg";s:5:"sizes";a:4:{s:6:"medium";a:4:{s:4:"file";s:19:"Image_1-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"Image_1-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:19:"Image_1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"Image_1-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(33, 23, '_wp_attached_file', '2020/01/appointment-reminders-feature02.png'),
(34, 23, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:752;s:6:"height";i:696;s:4:"file";s:43:"2020/01/appointment-reminders-feature02.png";s:5:"sizes";a:2:{s:6:"medium";a:4:{s:4:"file";s:43:"appointment-reminders-feature02-300x278.png";s:5:"width";i:300;s:6:"height";i:278;s:9:"mime-type";s:9:"image/png";}s:9:"thumbnail";a:4:{s:4:"file";s:43:"appointment-reminders-feature02-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}') ;

#
# End of data contents of table `wp_postmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_posts`
#

DROP TABLE IF EXISTS `wp_posts`;


#
# Table structure of table `wp_posts`
#

CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT 0,
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_posts`
#
INSERT INTO `wp_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-12-26 07:38:07', '2019-12-26 07:38:07', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2019-12-26 07:38:07', '2019-12-26 07:38:07', '', 0, 'http://localhost/demoaddtheme/?p=1', 0, 'post', '', 1),
(2, 1, '2019-12-26 07:38:07', '2019-12-26 07:38:07', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href="http://localhost/demoaddtheme/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2019-12-26 07:38:07', '2019-12-26 07:38:07', '', 0, 'http://localhost/demoaddtheme/?page_id=2', 0, 'page', '', 0),
(3, 1, '2019-12-26 07:38:07', '2019-12-26 07:38:07', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://localhost/demoaddtheme.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {"level":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {"level":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {"level":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {"level":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {"level":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {"level":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {"level":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {"level":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {"level":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {"level":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {"level":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2019-12-26 07:38:07', '2019-12-26 07:38:07', '', 0, 'http://localhost/demoaddtheme/?page_id=3', 0, 'page', '', 0),
(5, 0, '2019-12-26 07:38:39', '2019-12-26 07:38:39', '', 'List Product', '', 'publish', 'closed', 'closed', '', 'list-product', '', '', '2019-12-26 07:38:39', '2019-12-26 07:38:39', '', 0, 'http://localhost/demoaddtheme/list-product/', 0, 'page', '', 0),
(6, 0, '2019-12-26 07:38:39', '2019-12-26 07:38:39', '', 'Single Product', '', 'publish', 'closed', 'closed', '', 'single-product', '', '', '2019-12-26 07:38:39', '2019-12-26 07:38:39', '', 0, 'http://localhost/demoaddtheme/single-product/', 0, 'page', '', 0),
(7, 1, '2019-12-26 08:03:01', '2019-12-26 08:03:01', '', 'logo-1', '', 'inherit', 'open', 'closed', '', 'logo-1', '', '', '2019-12-26 08:03:01', '2019-12-26 08:03:01', '', 0, 'http://localhost/demoaddtheme/logo-1/', 0, 'attachment', 'image/jpeg', 0),
(8, 1, '2019-12-26 08:08:21', '2019-12-26 08:08:21', '', 'logo-1', '', 'inherit', 'open', 'closed', '', 'logo-1-2', '', '', '2019-12-26 08:08:21', '2019-12-26 08:08:21', '', 0, 'http://localhost/demoaddtheme/logo-1-2/', 0, 'attachment', 'image/jpeg', 0),
(9, 1, '2019-12-26 08:08:39', '2019-12-26 08:08:39', '', 'logo-1', '', 'inherit', 'open', 'closed', '', 'logo-1-3', '', '', '2019-12-26 08:08:39', '2019-12-26 08:08:39', '', 0, 'http://localhost/demoaddtheme/logo-1-3/', 0, 'attachment', 'image/jpeg', 0),
(10, 1, '2019-12-26 08:09:05', '2019-12-26 08:09:05', '', 'logo-1', '', 'inherit', 'open', 'closed', '', 'logo-1-4', '', '', '2019-12-26 08:09:05', '2019-12-26 08:09:05', '', 0, 'http://localhost/demoaddtheme/logo-1-4/', 0, 'attachment', 'image/jpeg', 0),
(11, 1, '2019-12-26 08:11:19', '2019-12-26 08:11:19', '', 'logo-1', '', 'inherit', 'open', 'closed', '', 'logo-1-5', '', '', '2019-12-26 08:11:19', '2019-12-26 08:11:19', '', 0, 'http://localhost/demoaddtheme/logo-1-5/', 0, 'attachment', 'image/jpeg', 0),
(12, 1, '2019-12-26 08:11:27', '2019-12-26 08:11:27', '', 'logo-1', '', 'inherit', 'open', 'closed', '', 'logo-1-6', '', '', '2019-12-26 08:11:27', '2019-12-26 08:11:27', '', 0, 'http://localhost/demoaddtheme/logo-1-6/', 0, 'attachment', 'image/jpeg', 0),
(13, 1, '2019-12-26 08:42:54', '2019-12-26 08:42:54', '', 'logo-1', '', 'inherit', 'open', 'closed', '', 'logo-1-7', '', '', '2019-12-26 08:42:54', '2019-12-26 08:42:54', '', 0, 'http://localhost/demoaddtheme/logo-1-7/', 0, 'attachment', 'image/jpeg', 0),
(14, 1, '2019-12-26 08:43:38', '2019-12-26 08:43:38', '', 'elementor-logo', '', 'inherit', 'open', 'closed', '', 'elementor-logo', '', '', '2019-12-26 08:43:38', '2019-12-26 08:43:38', '', 0, 'http://localhost/demoaddtheme/elementor-logo/', 0, 'attachment', 'image/png', 0),
(15, 1, '2019-12-26 08:43:56', '2019-12-26 08:43:56', '', 'logo-1', '', 'inherit', 'open', 'closed', '', 'logo-1-8', '', '', '2019-12-26 08:43:56', '2019-12-26 08:43:56', '', 0, 'http://localhost/demoaddtheme/logo-1-8/', 0, 'attachment', 'image/jpeg', 0),
(16, 1, '2019-12-26 08:51:56', '2019-12-26 08:51:56', '<!-- wp:paragraph -->\n<p>Demo Elementor Demo Elementor Demo ElementorDemo Elementor Demo Elementor Demo Elementor Demo Elementor Demo Elementor</p>\n<!-- /wp:paragraph -->', 'Demo Elementor', '', 'publish', 'open', 'open', '', 'demo-elementor', '', '', '2019-12-26 08:51:56', '2019-12-26 08:51:56', '', 0, 'http://localhost/demoaddtheme/?p=16', 0, 'post', '', 0),
(17, 1, '2019-12-26 08:51:56', '2019-12-26 08:51:56', '<!-- wp:paragraph -->\n<p>Demo Elementor Demo Elementor Demo ElementorDemo Elementor Demo Elementor Demo Elementor Demo Elementor Demo Elementor</p>\n<!-- /wp:paragraph -->', 'Demo Elementor', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2019-12-26 08:51:56', '2019-12-26 08:51:56', '', 16, 'http://localhost/demoaddtheme/2019/12/26/16-revision-v1/', 0, 'revision', '', 0),
(18, 1, '2019-12-26 08:52:19', '2019-12-26 08:52:19', '<!-- wp:paragraph -->\n<p>DEMO ELEMENTOR 1 DEMO ELEMENTOR 1 DEMO ELEMENTOR 1 DEMO ELEMENTOR 1 DEMO ELEMENTOR 1 DEMO ELEMENTOR 1 DEMO ELEMENTOR 1 DEMO ELEMENTOR 1</p>\n<!-- /wp:paragraph -->', 'DEMO ELEMENTOR 1', '', 'publish', 'open', 'open', '', 'demo-elementor-1', '', '', '2019-12-26 08:52:19', '2019-12-26 08:52:19', '', 0, 'http://localhost/demoaddtheme/?p=18', 0, 'post', '', 0),
(19, 1, '2019-12-26 08:52:19', '2019-12-26 08:52:19', '<!-- wp:paragraph -->\n<p>DEMO ELEMENTOR 1 DEMO ELEMENTOR 1 DEMO ELEMENTOR 1 DEMO ELEMENTOR 1 DEMO ELEMENTOR 1 DEMO ELEMENTOR 1 DEMO ELEMENTOR 1 DEMO ELEMENTOR 1</p>\n<!-- /wp:paragraph -->', 'DEMO ELEMENTOR 1', '', 'inherit', 'closed', 'closed', '', '18-revision-v1', '', '', '2019-12-26 08:52:19', '2019-12-26 08:52:19', '', 18, 'http://localhost/demoaddtheme/2019/12/26/18-revision-v1/', 0, 'revision', '', 0),
(20, 1, '2019-12-27 04:21:45', '2019-12-27 04:21:45', '', 'Image_2', '', 'inherit', 'open', 'closed', '', 'image_2', '', '', '2019-12-27 04:21:45', '2019-12-27 04:21:45', '', 0, 'http://localhost/demoaddtheme/image_2/', 0, 'attachment', 'image/jpeg', 0),
(21, 1, '2019-12-27 04:42:33', '2019-12-27 04:42:33', '', 'Image_1', '', 'inherit', 'open', 'closed', '', 'image_1', '', '', '2019-12-27 04:42:33', '2019-12-27 04:42:33', '', 0, 'http://localhost/demoaddtheme/image_1/', 0, 'attachment', 'image/jpeg', 0),
(23, 1, '2020-01-30 02:08:14', '2020-01-30 02:08:14', '', 'appointment-reminders-feature02', '', 'inherit', 'open', 'closed', '', 'appointment-reminders-feature02', '', '', '2020-01-30 02:08:14', '2020-01-30 02:08:14', '', 0, 'http://localhost/demoaddtheme/appointment-reminders-feature02/', 0, 'attachment', 'image/png', 0),
(24, 1, '2020-02-06 10:12:37', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2020-02-06 10:12:37', '0000-00-00 00:00:00', '', 0, 'http://192.168.1.145/demoaddtheme/?p=24', 0, 'post', '', 0) ;

#
# End of data contents of table `wp_posts`
# --------------------------------------------------------



#
# Delete any existing table `wp_term_relationships`
#

DROP TABLE IF EXISTS `wp_term_relationships`;


#
# Table structure of table `wp_term_relationships`
#

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_term_relationships`
#
INSERT INTO `wp_term_relationships` ( `object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(16, 1, 0),
(18, 1, 0) ;

#
# End of data contents of table `wp_term_relationships`
# --------------------------------------------------------



#
# Delete any existing table `wp_term_taxonomy`
#

DROP TABLE IF EXISTS `wp_term_taxonomy`;


#
# Table structure of table `wp_term_taxonomy`
#

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_term_taxonomy`
#
INSERT INTO `wp_term_taxonomy` ( `term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 3) ;

#
# End of data contents of table `wp_term_taxonomy`
# --------------------------------------------------------



#
# Delete any existing table `wp_termmeta`
#

DROP TABLE IF EXISTS `wp_termmeta`;


#
# Table structure of table `wp_termmeta`
#

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_termmeta`
#

#
# End of data contents of table `wp_termmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_terms`
#

DROP TABLE IF EXISTS `wp_terms`;


#
# Table structure of table `wp_terms`
#

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_terms`
#
INSERT INTO `wp_terms` ( `term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0) ;

#
# End of data contents of table `wp_terms`
# --------------------------------------------------------



#
# Delete any existing table `wp_usermeta`
#

DROP TABLE IF EXISTS `wp_usermeta`;


#
# Table structure of table `wp_usermeta`
#

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_usermeta`
#
INSERT INTO `wp_usermeta` ( `umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'bssadmin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:2:{s:64:"efd59d30e4f0895e6e79fbaa540199b8fbd8b768c6e4e6f79b5684f9f48f91a4";a:4:{s:10:"expiration";i:1581559740;s:2:"ip";s:13:"192.168.1.145";s:2:"ua";s:114:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36";s:5:"login";i:1581386940;}s:64:"a1ba2e185a25b6bdce941328fad55d567828f74aa7273cdc2136db3d4f42aef2";a:4:{s:10:"expiration";i:1581648617;s:2:"ip";s:13:"192.168.1.145";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.100 Safari/537.36";s:5:"login";i:1581475817;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '24'),
(18, 1, 'wp_r_tru_u_x', 'a:2:{s:2:"id";i:0;s:7:"expires";i:1577434018;}'),
(19, 1, 'community-events-location', 'a:1:{s:2:"ip";s:11:"192.168.1.0";}'),
(20, 1, 'wp_user-settings', 'mfold=o'),
(21, 1, 'wp_user-settings-time', '1581071934') ;

#
# End of data contents of table `wp_usermeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_users`
#

DROP TABLE IF EXISTS `wp_users`;


#
# Table structure of table `wp_users`
#

CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


#
# Data contents of table `wp_users`
#
INSERT INTO `wp_users` ( `ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'bssadmin', '$P$B26sHTzmg5hX/.kPLEXThzKO3O6vAs.', 'bssadmin', 'phuong.luong@beesightsoft.com', '', '2019-12-26 07:38:07', '', 0, 'bssadmin') ;

#
# End of data contents of table `wp_users`
# --------------------------------------------------------

#
# Add constraints back in and apply any alter data queries.
#

